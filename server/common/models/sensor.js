'use strict';

const HasMany = require('../mixins/HasManyIntegrityCheck');
const BelongsTo = require('../mixins/BelongsToIntegrityCheck');
module.exports = function(Sensor, options) {
  Sensor.validatesUniquenessOf('label', {
    message: 'This sensor already exist',
  });
  HasMany(Sensor, options);
  BelongsTo(Sensor, options);
};
