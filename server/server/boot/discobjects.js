'use strict';

// var host    = 'http://alfademo.ddns.net:3001';
var host = 'http://localhost:3001';
var loopback = require('loopback');
var ds = loopback.createDataSource({
  connector: require('loopback-connector-rest'),
  debug: false,
  operations: [
    {
      functions: {findAll: []},
      template: {
        method: 'GET',
        url: `${host}/discobjects`,
        headers: {
          accepts: 'application/json',
          'content-type': 'application/json',
        },
      },
    },
    {
      functions: {find: ['latitude', 'longitude']},
      template: {
        method: 'GET',
        url: `${host}/discobjects/location/{latitude}/{longitude}/`,
        headers: {
          accepts: 'application/json',
          'content-type': 'application/json',
        },
      },
    },
    {
      functions: {create: ['data']},
      template: {
        method: 'POST',
        url: `${host}/discobjects`,
        body: '{data:object}',
      },
    },
    {
      functions: {remove: ['id']},
      template: {
        method: 'DELETE',
        url: `${host}/discobjects/{id}`,
        headers: {
          accepts: 'application/json',
          'content-type': 'application/json',
        },
      },
    },
  ],
});

module.exports = (server) => {
  var router = server.loopback.Router();

  router.get('/api/discobjects', (req, res)=>{
    ds.findAll().then(result=>res.send(result))
    .catch(err=>res.status(500).send(err));
  });

  router.get('/api/discobjects/:latitude/:longitude/', (req, res)=>{
    ds.find(req.params.latitude, req.params.longitude).then(result=>res.send(result))
    .catch(err=>res.status(500).send(err));
  });

  router.post('/api/discobjects', (req, res)=>{
    let value = req.body;
    ds.create(value).then(result=>{
      ds.find(value.location.latitude, value.location.longitude).then(disc=>{
        let index = disc.findIndex(item =>
          item.name === value.name &&
          item.eqType.id === value.eqType.id &&
          item.location.latitude === value.location.latitude &&
          item.location.longitude === value.location.longitude
        );
        res.send(disc[index]);
      }).catch(err=>res.status(500).send(err));
    }).catch(err=>res.status(500).send(err));
  });

  router.delete('/api/discobjects/:id', (req, res)=>{
    ds.remove(req.params.id).then(result=>res.send(result))
    .catch(err=>res.status(500).send(err));
  });

  server.use(router);
};


//  insert into disc_object values (null, -6.883435, 107.609897, 'Gembok', 1, null);
