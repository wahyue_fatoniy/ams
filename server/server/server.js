'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

var kafka = require('kafka-node');
var Consumer = kafka.Consumer;
var client = new kafka.Client('localhost:2181/');
var topics = [{topic: 'alarm.data'}, {topic: 'perfmeasurement.data'}];
var options = {fromOffset: 'latest', fromBeginning: false};
var consumer = new Consumer(client, topics, options);
var offset = new kafka.Offset(client);
const winston = require('winston');

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;
  // start the server if `$ node server.js`
  if (require.main === module) {
    app.io = require('socket.io')(app.start());
  }

  client.on('error', err => winston.error('Tidak bisa connect ke kafka', err));
  consumer.on('error', err => winston.error('Tidak bisa konek consumer message kafka', err));
  consumer.on('offsetOutOfRange', topic => {
    winston.info('offset out of range', topic);
    topic.maxNum = 2;
    offset.fetch([topic], (err, offsets) => {
        var min = Math.min.apply(null, offsets[topic.topic][topic.partition]);
        consumer.setOffset(topic.topic, topic.partition, min);
    });
  });

  consumer.on('message', kafka => (
    app.io.emit(kafka.topic, kafka.value),
    winston.info('Berhasil menerima message', kafka)
  ));

});


