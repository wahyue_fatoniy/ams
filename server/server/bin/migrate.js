'use strict';

var path = require('path');

var server = require(path.resolve(__dirname, '../server'));
var my = server.dataSources.mysql;

var lbTables = ['worker', 'work_order', 'work_order_history', 'access_key', 'access_key_history', 'request_key'];

my.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' + lbTables + '] created in ' + my.adapter.name);
  my.disconnect();
  process.exit(0);
});
