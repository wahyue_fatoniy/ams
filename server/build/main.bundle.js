(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	// install source-map support so we get mapped stack traces.
	__webpack_require__(1).install();
	
	var loopback = __webpack_require__(2);
	
	var app = module.exports = loopback();
	
	app.start = function() {
	  // start the web server
	  return app.listen(function() {
	    app.emit('started');
	    var baseUrl = app.get('url').replace(/\/$/, '');
	    console.log('Web server listening at: %s', baseUrl);
	    if (app.get('loopback-component-explorer')) {
	      var explorerPath = app.get('loopback-component-explorer').mountPath;
	      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
	    }
	  });
	};
	
	// Bootstrap the application, configure models, datasources and middleware.
	// Sub-apps like REST API are mounted via boot scripts.
	console.log('Executing boot instructions...');
	// instructions are provided by an explicit webpack resolve
	// alias (see gulpfile.js).
	var ins = __webpack_require__(3);
	// install the external dynamic configuration.
	ins.config = __webpack_require__(4);
	ins.dataSources = __webpack_require__(5);
	var execute = __webpack_require__(6);
	execute(app, ins, (err)=>{
	  if (err) {
	    console.error(`Boot error: ${err}`);
	    throw err;
	  }
	  console.log('Starting server...');
	    // NOTE/TODO: the require.main === module check fails here under webpack
	    // so we're not doing it.
	  var server = app.io = __webpack_require__(64)(app.start());
	  var kafka = __webpack_require__(62);
	  var Consumer = kafka.Consumer;
	  var client = new kafka.Client('localhost:2181/');
	  var topics = [{topic: 'alarm.data'}];
	  var consumer = new Consumer(client, topics);
	  consumer.on('message', function(message) {
	    console.log(message);
	    app.io.emit('kafka', message.value);
	  });
	});
	console.log('Done.');


/***/ }),
/* 1 */
/***/ (function(module, exports) {

	module.exports = require("source-map-support");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	module.exports = require("loopback");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	module.exports = {"env":"development","models":[{"name":"AccessToken","config":{"dataSource":"mysql","public":false},"definition":{"name":"AccessToken","properties":{"id":{"type":"string","id":true},"ttl":{"type":"number","ttl":true,"default":1209600,"description":"time to live in seconds (2 weeks by default)"},"scopes":{"type":["string"],"description":"Array of scopes granted to this access token."},"created":{"type":"Date","defaultFn":"now"}},"relations":{"user":{"type":"belongsTo","model":"User","foreignKey":"userId"}},"acls":[{"principalType":"ROLE","principalId":"$everyone","permission":"DENY"},{"principalType":"ROLE","principalId":"$everyone","property":"create","permission":"ALLOW"}]},"sourceFile":"./node_modules/loopback/common/models/access-token.js"},{"name":"province","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"province","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"label":{"type":"string","required":true},"location":{"type":"geopoint","required":true},"zoom":{"type":"number","required":true}},"validations":[],"relations":{"cities":{"type":"hasMany","model":"city","foreignKey":"province_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/province.js"},{"name":"city","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"city","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"label":{"type":"string","required":true},"location":{"type":"geopoint","required":true},"province_id":{"type":"number","required":true},"zoom":{"type":"number","required":true}},"validations":[],"relations":{"province":{"type":"belongsTo","model":"province","foreignKey":"province_id"},"districts":{"type":"hasMany","model":"district","foreignKey":"city_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/city.js"},{"name":"district","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"district","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"city_id":{"type":"number","required":true},"label":{"type":"string","required":true},"location":{"type":"geopoint","required":true},"zoom":{"type":"number","required":true}},"validations":[],"relations":{"city":{"type":"belongsTo","model":"city","foreignKey":"city_id"},"sites":{"type":"hasMany","model":"site","foreignKey":"district_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/district.js"},{"name":"site","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"site","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"address":{"type":"string","required":true},"district_id":{"type":"number","required":true},"location":{"type":"geopoint","required":true},"label":{"type":"string","required":true},"zoom":{"type":"number","required":true},"discobj_id":{"type":"number","required":true}},"validations":[],"relations":{"district":{"type":"belongsTo","model":"district","foreignKey":"district_id"},"managedObjects":{"type":"hasMany","model":"managed_object","foreignKey":"site_id"},"securitySites":{"type":"hasMany","model":"security_site","foreignKey":"site_id"},"sensors":{"type":"hasMany","model":"sensor","foreignKey":"site_id"},"items":{"type":"hasMany","model":"item","foreignKey":"site_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/site.js"},{"name":"managed_object","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"managed_object","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"site_id":{"type":"number","required":true},"discobj_id":{"type":"number","required":true},"top":{"type":"number","required":true},"left":{"type":"number","required":true}},"validations":[],"relations":{"site":{"type":"belongsTo","model":"site","foreignKey":"site_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/managed-object.js"},{"name":"container","config":{"dataSource":"file","public":true},"definition":{"name":"container","base":"Model","idInjection":true,"options":{"validateUpsert":true},"properties":{},"validations":[],"relations":{},"acls":[],"methods":{}},"sourceFile":"./common/models/container.js"},{"name":"User","definition":{"name":"User","properties":{"realm":{"type":"string"},"username":{"type":"string"},"password":{"type":"string","required":true},"email":{"type":"string","required":true},"emailVerified":"boolean","verificationToken":"string"},"options":{"caseSensitiveEmail":true},"hidden":["password","verificationToken"],"acls":[{"principalType":"ROLE","principalId":"$everyone","permission":"DENY"},{"principalType":"ROLE","principalId":"$everyone","permission":"ALLOW","property":"create"},{"principalType":"ROLE","principalId":"$owner","permission":"ALLOW","property":"deleteById"},{"principalType":"ROLE","principalId":"$everyone","permission":"ALLOW","property":"login"},{"principalType":"ROLE","principalId":"$everyone","permission":"ALLOW","property":"logout"},{"principalType":"ROLE","principalId":"$owner","permission":"ALLOW","property":"findById"},{"principalType":"ROLE","principalId":"$owner","permission":"ALLOW","property":"patchAttributes"},{"principalType":"ROLE","principalId":"$owner","permission":"ALLOW","property":"replaceById"},{"principalType":"ROLE","principalId":"$everyone","permission":"ALLOW","property":"verify","accessType":"EXECUTE"},{"principalType":"ROLE","principalId":"$everyone","permission":"ALLOW","property":"confirm"},{"principalType":"ROLE","principalId":"$everyone","permission":"ALLOW","property":"resetPassword","accessType":"EXECUTE"},{"principalType":"ROLE","principalId":"$authenticated","permission":"ALLOW","property":"changePassword","accessType":"EXECUTE"},{"principalType":"ROLE","principalId":"$authenticated","permission":"ALLOW","property":"setPassword","accessType":"EXECUTE"}],"relations":{"accessTokens":{"type":"hasMany","model":"AccessToken","foreignKey":"userId","options":{"disableInclude":true}}}},"sourceFile":"./node_modules/loopback/common/models/user.js"},{"name":"security","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"security","base":"User","idInjection":true,"options":{"validateUpsert":true},"properties":{"email":{"type":"string","required":true},"password":{"type":"string","required":true},"phone":{"type":"string","required":true},"username":{"type":"string","required":true},"name":{"type":"string","required":true}},"validations":[],"relations":{"securitySites":{"type":"hasMany","model":"security_site","foreignKey":"security_id"}},"acls":[{"accessType":"*","principalType":"ROLE","principalId":"$everyone","permission":"ALLOW"}],"methods":{}},"sourceFile":"./common/models/security.js"},{"name":"security_site","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"security_site","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"security_id":{"type":"number","required":true},"site_id":{"type":"number","required":true}},"validations":[],"relations":{"security":{"type":"belongsTo","model":"security","foreignKey":"security_id"},"site":{"type":"belongsTo","model":"site","foreignKey":"site_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/security-site.js"},{"name":"channel_type","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"channel_type","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"label":{"type":"string","required":true}},"validations":[],"relations":{"channels":{"type":"hasMany","model":"channel","foreignKey":"channel_type_id"},"sensors":{"type":"hasMany","model":"sensor","foreignKey":"channel_type_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/channel-type.js"},{"name":"channel","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"channel","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"label":{"type":"string","required":true},"channel_type_id":{"type":"number","required":true}},"validations":[],"relations":{"channelType":{"type":"belongsTo","model":"channel_type","foreignKey":"channel_type_id"},"mappingSensors":{"type":"hasMany","model":"mapping_sensor","foreignKey":"channel_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/channel.js"},{"name":"sensor","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"sensor","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"label":{"type":"string","required":true},"site_id":{"type":"number","required":true},"channel_type_id":{"type":"number","required":true}},"validations":[],"relations":{"mappingSensors":{"type":"hasMany","model":"mapping_sensor","foreignKey":"sensor_id"},"site":{"type":"belongsTo","model":"site","foreignKey":"site_id"},"channelType":{"type":"belongsTo","model":"channel_type","foreignKey":"channel_type_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/sensor.js"},{"name":"mapping_sensor","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"mapping_sensor","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"sensor_id":{"type":"number","required":true},"channel_id":{"type":"number","required":true},"discobj_id":{"type":"number","required":true}},"validations":[],"relations":{"sensor":{"type":"belongsTo","model":"sensor","foreignKey":"sensor_id"},"channel":{"type":"belongsTo","model":"channel","foreignKey":"channel_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/mapping-sensor.js"},{"name":"item","config":{"dataSource":"mysql","public":true,"$promise":{},"$resolved":true},"definition":{"name":"item","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"top":{"type":"number","required":true},"left":{"type":"number","required":true},"type":{"type":"string","required":true},"site_id":{"type":"number","required":true}},"validations":[],"relations":{"site":{"type":"belongsTo","model":"site","foreignKey":"site_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/item.js"},{"name":"worker","config":{"dataSource":"mysql","public":true},"definition":{"name":"worker","base":"User","idInjection":true,"options":{"validateUpsert":true},"properties":{"name":{"type":"string","required":true},"profile":{"type":"string","required":true},"email":{"type":"string","required":true},"password":{"type":"string","required":true},"phone":{"type":"string","required":true},"company":{"type":"string","required":true},"username":{"type":"string","required":true}},"validations":[],"relations":{"workOrders":{"type":"hasMany","model":"work_order","foreignKey":"worker_id"}},"acls":[{"accessType":"*","principalType":"ROLE","principalId":"$everyone","permission":"ALLOW"}],"methods":{}},"sourceFile":"./common/models/worker.js"},{"name":"work_order","config":{"dataSource":"mysql","public":true},"definition":{"name":"work_order","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"worker_id":{"type":"number","required":true},"site_id":{"type":"number","required":true},"start":{"type":"date","required":true},"end":{"type":"date","required":true},"project":{"type":"string","required":true},"activity":{"type":"string","required":true},"status":{"type":"string","required":true}},"validations":[],"relations":{"accessKeys":{"type":"hasMany","model":"access_key","foreignKey":"work_order_id"},"site":{"type":"belongsTo","model":"site","foreignKey":"site_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/work-order.js"},{"name":"access_key","config":{"dataSource":"mysql","public":true},"definition":{"name":"access_key","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"work_order_id":{"type":"number","required":true},"discobj_id":{"type":"number","required":true},"key":{"type":"string","required":true}},"validations":[],"relations":{"workOrder":{"type":"belongsTo","model":"work_order","foreignKey":"work_order_id"}},"acls":[],"methods":{}},"sourceFile":"./common/models/access-key.js"},{"name":"work_order_history","config":{"dataSource":"mysql","public":true},"definition":{"name":"work_order_history","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"work_order_id":{"type":"number","required":true},"status":{"type":"string","required":true},"picture":{"type":"string"},"note":{"type":"string"},"date":{"type":"date","required":true,"default":"$now"}},"validations":[],"relations":{},"acls":[],"methods":{}},"sourceFile":"./common/models/work-order-history.js"},{"name":"access_key_history","config":{"dataSource":"mysql","public":true},"definition":{"name":"access_key_history","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"access_key_id":{"type":"number","required":true},"date":{"type":"date","required":true,"default":"$now"}},"validations":[],"relations":{},"acls":[],"methods":{}},"sourceFile":"./common/models/access-key-history.js"},{"name":"request_key","config":{"dataSource":"mysql","public":true},"definition":{"name":"request_key","base":"PersistedModel","idInjection":true,"options":{"validateUpsert":true},"properties":{"work_order_id":{"type":"number","required":true},"picture":{"type":"string","required":true}},"validations":[],"relations":{},"acls":[],"methods":{}},"sourceFile":"./common/models/request-key.js"}],"middleware":{"phases":["initial","session","auth","parse","routes","files","final"],"middleware":[{"sourceFile":"./node_modules/loopback","config":{"phase":"initial:before"},"fragment":"favicon"},{"sourceFile":"./node_modules/compression","config":{"phase":"initial"}},{"sourceFile":"./node_modules/cors","config":{"params":{"origin":true,"credentials":true,"maxAge":86400},"phase":"initial"}},{"sourceFile":"./node_modules/helmet","config":{"phase":"initial"},"fragment":"xssFilter"},{"sourceFile":"./node_modules/helmet","config":{"params":["deny"],"phase":"initial"},"fragment":"frameguard"},{"sourceFile":"./node_modules/helmet","config":{"params":{"maxAge":0,"includeSubdomains":true},"phase":"initial"},"fragment":"hsts"},{"sourceFile":"./node_modules/helmet","config":{"phase":"initial"},"fragment":"hidePoweredBy"},{"sourceFile":"./node_modules/helmet","config":{"phase":"initial"},"fragment":"ieNoOpen"},{"sourceFile":"./node_modules/helmet","config":{"phase":"initial"},"fragment":"noSniff"},{"sourceFile":"./node_modules/helmet","config":{"enabled":false,"phase":"initial"},"fragment":"noCache"},{"sourceFile":"./node_modules/body-parser","config":{"phase":"parse"},"fragment":"json"},{"sourceFile":"./node_modules/body-parser","config":{"params":{"extended":true},"phase":"parse"},"fragment":"urlencoded"},{"sourceFile":"./node_modules/loopback","config":{"paths":["${restApiRoot}"],"phase":"routes"},"fragment":"rest"},{"sourceFile":"./node_modules/loopback","config":{"params":"/home/toni/elock/ams/server/client","phase":"files"},"fragment":"static"},{"sourceFile":"./node_modules/loopback","config":{"phase":"final"},"fragment":"urlNotFound"},{"sourceFile":"./node_modules/strong-error-handler","config":{"phase":"final:after"}}]},"components":[{"sourceFile":"./node_modules/loopback-component-explorer/index.js","config":{"mountPath":"/explorer"}}],"mixins":[],"files":{"boot":["./server/boot/alarms.js","./server/boot/discobjects.js","./server/boot/equipments.js","./server/boot/padlocks.js","./server/boot/perf-counters.js","./server/boot/perf-measurements.js","./server/boot/perf-objects.js"]}}

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	module.exports = require("../server/config.json");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	module.exports = require("../server/datasources.json");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	// Copyright IBM Corp. 2014,2016. All Rights Reserved.
	// Node module: loopback-boot
	// This file is licensed under the MIT License.
	// License text available at https://opensource.org/licenses/MIT
	
	var assert = __webpack_require__(7);
	var semver = __webpack_require__(8);
	var debug = __webpack_require__(9)('loopback:boot:executor');
	var async = __webpack_require__(10);
	var path = __webpack_require__(11);
	var format = __webpack_require__(12).format;
	var g = __webpack_require__(13)();
	var requireNodeOrEsModule = __webpack_require__(14);
	
	/**
	 * Execute bootstrap instructions gathered by `boot.compile`.
	 *
	 * @param {Object} app The loopback app to boot.
	 * @options {Object} instructions Boot instructions.
	 * @param {Function} [callback] Callback function.
	 *
	 * @header boot.execute(instructions)
	 */
	
	module.exports = function execute(app, instructions, callback) {
	  callback = callback || function() {};
	
	  app.booting = true;
	
	  patchAppLoopback(app);
	  assertLoopBackVersion(app);
	
	  setEnv(app, instructions);
	  setHost(app, instructions);
	  setPort(app, instructions);
	  setApiRoot(app, instructions);
	  applyAppConfig(app, instructions);
	
	  setupDataSources(app, instructions);
	  setupModels(app, instructions);
	  setupMiddleware(app, instructions);
	  setupComponents(app, instructions);
	
	  // Run the boot scripts in series synchronously or asynchronously
	  // Please note async supports both styles
	  async.series([
	    function(done) {
	      runBootScripts(app, instructions, done);
	    },
	    function(done) {
	      enableAnonymousSwagger(app, instructions);
	      done();
	    },
	    // Ensure both the "booted" event and the callback are always called
	    // in the next tick of the even loop.
	    // See http://blog.izs.me/post/59142742143/designing-apis-for-asynchrony
	    process.nextTick,
	  ], function(err) {
	    app.booting = false;
	
	    if (err) return callback(err);
	
	    app.emit('booted');
	
	    callback();
	  });
	};
	
	function patchAppLoopback(app) {
	  if (app.loopback) return;
	  // app.loopback was introduced in 1.9.0
	  // patch the app object to make loopback-boot work with older versions too
	  try {
	    app.loopback = __webpack_require__(2);
	  } catch (err) {
	    if (err.code === 'MODULE_NOT_FOUND') {
	      g.error(
	          'When using {{loopback-boot}} with {{loopback}} <1.9, ' +
	          'the {{loopback}} module must be available ' +
	          'for `{{require(\'loopback\')}}`.');
	    }
	    throw err;
	  }
	}
	
	function assertLoopBackVersion(app) {
	  var RANGE = '1.x || 2.x || ^3.0.0-alpha';
	
	  var loopback = app.loopback;
	  // remove any pre-release tag from the version string,
	  // because semver has special treatment of pre-release versions,
	  // while loopback-boot treats pre-releases the same way as regular versions
	  var version = (loopback.version || '1.0.0').replace(/-.*$/, '');
	  if (!semver.satisfies(version, RANGE)) {
	    var msg = g.f(
	      'The `{{app}}` is powered by an incompatible {{loopback}} version %s. ' +
	      'Supported versions: %s',
	      loopback.version || '(unknown)',
	      RANGE);
	    throw new Error(msg);
	  }
	}
	
	function setEnv(app, instructions) {
	  var env = instructions.env;
	  if (env !== undefined)
	    app.set('env', env);
	}
	
	function setHost(app, instructions) {
	  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	  var host =
	    process.env.npm_config_host ||
	    process.env.OPENSHIFT_SLS_IP ||
	    process.env.OPENSHIFT_NODEJS_IP ||
	    process.env.HOST ||
	    process.env.VCAP_APP_HOST ||
	    instructions.config.host ||
	    process.env.npm_package_config_host ||
	    app.get('host');
	
	  if (host !== undefined) {
	    assert(typeof host === 'string', g.f('{{app.host}} must be a {{string}}'));
	    app.set('host', host);
	  }
	}
	
	function setPort(app, instructions) {
	  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	  var port = find([
	    process.env.npm_config_port,
	    process.env.OPENSHIFT_SLS_PORT,
	    process.env.OPENSHIFT_NODEJS_PORT,
	    process.env.PORT,
	    process.env.VCAP_APP_PORT,
	    instructions.config.port,
	    process.env.npm_package_config_port,
	    app.get('port'),
	    3000,
	  ], function(p) {
	    return p != null;
	  });
	
	  if (port !== undefined) {
	    var portType = typeof port;
	    assert(portType === 'string' || portType === 'number',
	      g.f('{{app.port}} must be a {{string}} or {{number}}'));
	    app.set('port', port);
	  }
	}
	
	function find(array, predicate) {
	  return array.filter(predicate)[0];
	}
	
	function setApiRoot(app, instructions) {
	  var restApiRoot =
	    instructions.config.restApiRoot ||
	    app.get('restApiRoot') ||
	    '/api';
	
	  assert(restApiRoot !== undefined, g.f('{{app.restBasePath}} is required'));
	  assert(typeof restApiRoot === 'string',
	    g.f('{{app.restApiRoot}} must be a {{string}}'));
	  assert(/^\//.test(restApiRoot),
	    g.f('{{app.restApiRoot}} must start with "/"'));
	  app.set('restApiRoot', restApiRoot);
	}
	
	function applyAppConfig(app, instructions) {
	  var appConfig = instructions.config;
	  for (var configKey in appConfig) {
	    var cur = app.get(configKey);
	    if (cur === undefined || cur === null) {
	      app.set(configKey, appConfig[configKey]);
	    }
	  }
	}
	
	function setupDataSources(app, instructions) {
	  forEachKeyedObject(instructions.dataSources, function(key, obj) {
	    var opts = {
	      useEnvVars: true,
	    };
	    obj = getUpdatedConfigObject(app, obj, opts);
	    var lazyConnect = process.env.LB_LAZYCONNECT_DATASOURCES;
	    if (lazyConnect) {
	      obj.lazyConnect =
	        lazyConnect === 'false' || lazyConnect === '0' ? false : true;
	    }
	    app.dataSource(key, obj);
	  });
	}
	
	function setupModels(app, instructions) {
	  defineMixins(app, instructions);
	  defineModels(app, instructions);
	
	  instructions.models.forEach(function(data) {
	    // Skip base models that are not exported to the app
	    if (!data.config) return;
	
	    app.model(data._model, data.config);
	  });
	}
	
	function defineMixins(app, instructions) {
	  var modelBuilder = (app.registry || app.loopback).modelBuilder;
	  var BaseClass = app.loopback.Model;
	  var mixins = instructions.mixins || [];
	
	  if (!modelBuilder.mixins || !mixins.length) return;
	
	  mixins.forEach(function(obj) {
	    var mixin = requireNodeOrEsModule(obj.sourceFile);
	
	    if (typeof mixin === 'function' || mixin.prototype instanceof BaseClass) {
	      debug('Defining mixin %s', obj.name);
	      modelBuilder.mixins.define(obj.name, mixin); // TODO (name, mixin, meta)
	    } else {
	      debug('Skipping mixin file %s - `module.exports` is not a function' +
	        ' or Loopback model', obj);
	    }
	  });
	}
	
	function defineModels(app, instructions) {
	  var registry = app.registry || app.loopback;
	  instructions.models.forEach(function(data) {
	    var name = data.name;
	    var model;
	
	    if (!data.definition) {
	      model = registry.getModel(name);
	      if (!model) {
	        throw new Error(g.f('Cannot configure unknown model %s', name));
	      }
	      debug('Configuring existing model %s', name);
	    } else if (isBuiltinLoopBackModel(app, data)) {
	      model = registry.getModel(name);
	      assert(model, g.f('Built-in model %s should have been defined', name));
	      debug('Configuring built-in LoopBack model %s', name);
	    } else {
	      debug('Creating new model %s %j', name, data.definition);
	      model = registry.createModel(data.definition);
	      if (data.sourceFile) {
	        debug('Loading customization script %s', data.sourceFile);
	        var code = requireNodeOrEsModule(data.sourceFile);
	        if (typeof code === 'function') {
	          debug('Customizing model %s', name);
	          code(model);
	        } else {
	          debug('Skipping model file %s - `module.exports` is not a function',
	            data.sourceFile);
	        }
	      }
	    }
	
	    data._model = model;
	  });
	}
	
	// Regular expression to match built-in loopback models
	var LOOPBACK_MODEL_REGEXP = new RegExp(
	  ['', 'node_modules', 'loopback', '[^\\/\\\\]+', 'models', '[^\\/\\\\]+\\.js$']
	    .join('\\' + path.sep));
	
	function isBuiltinLoopBackModel(app, data) {
	  // 1. Built-in models are exposed on the loopback object
	  if (!app.loopback[data.name]) return false;
	
	  // 2. Built-in models have a script file `loopback/{facet}/models/{name}.js`
	  var srcFile = data.sourceFile;
	  return srcFile &&
	    LOOPBACK_MODEL_REGEXP.test(srcFile);
	}
	
	function forEachKeyedObject(obj, fn) {
	  if (typeof obj !== 'object') return;
	
	  Object.keys(obj).forEach(function(key) {
	    fn(key, obj[key]);
	  });
	}
	
	function runScripts(app, list, callback) {
	  list = list || [];
	  var functions = [];
	  list.forEach(function(filepath) {
	    debug('Requiring script %s', filepath);
	    try {
	      var bootFn = requireNodeOrEsModule(filepath);
	      if (typeof bootFn === 'function') {
	        debug('Exported function detected %s', filepath);
	        functions.push({
	          path: filepath,
	          func: bootFn,
	        });
	      }
	    } catch (err) {
	      g.error('Failed loading boot script: %s\n%s', filepath, err.stack);
	      throw err;
	    }
	  });
	
	  async.eachSeries(functions, function(f, done) {
	    debug('Running script %s', f.path);
	    var cb = function(err) {
	      debug('Async function %s %s', err ? 'failed' : 'finished', f.path);
	      done(err);
	      // Make sure done() isn't called twice, e.g. if a script returns a
	      // thenable object and also calls the passed callback.
	      cb = function() {};
	    };
	    try {
	      var result = f.func(app, cb);
	      if (result && typeof result.then === 'function') {
	        result.then(function() { cb(); }, cb);
	      } else if (f.func.length < 2) {
	        debug('Sync function finished %s', f.path);
	        done();
	      }
	    } catch (err) {
	      debug('Sync function failed %s', f.path, err);
	      done(err);
	    }
	  }, callback);
	}
	
	function setupMiddleware(app, instructions) {
	  if (!instructions.middleware) {
	    // the browserified client does not support middleware
	    return;
	  }
	
	  // Phases can be empty
	  var phases = instructions.middleware.phases || [];
	  assert(Array.isArray(phases),
	    g.f('{{instructions.middleware.phases}} must be an {{array}}'));
	
	  var middleware = instructions.middleware.middleware;
	  assert(Array.isArray(middleware),
	    'instructions.middleware.middleware must be an object');
	
	  debug('Defining middleware phases %j', phases);
	  app.defineMiddlewarePhases(phases);
	
	  middleware.forEach(function(data) {
	    debug('Configuring middleware %j%s', data.sourceFile,
	        data.fragment ? ('#' + data.fragment) : '');
	    var factory = requireNodeOrEsModule(data.sourceFile);
	    if (data.fragment) {
	      factory = factory[data.fragment].bind(factory);
	    }
	    assert(typeof factory === 'function',
	      'Middleware factory must be a function');
	    var opts = {
	      useEnvVars: true,
	    };
	    data.config = getUpdatedConfigObject(app, data.config, opts);
	    app.middlewareFromConfig(factory, data.config);
	  });
	}
	
	function getUpdatedConfigObject(app, config, opts) {
	  var DYNAMIC_CONFIG_PARAM = /\$\{(\w+)\}$/;
	  var useEnvVars = opts && opts.useEnvVars;
	
	  function getConfigVariable(param) {
	    var configVariable = param;
	    var match = configVariable.match(DYNAMIC_CONFIG_PARAM);
	    if (match) {
	      var varName = match[1];
	      if (useEnvVars && process.env[varName] !== undefined) {
	        debug('Dynamic Configuration: Resolved via process.env: %s as %s',
	          process.env[varName], param);
	        configVariable = process.env[varName];
	      } else if (app.get(varName) !== undefined) {
	        debug('Dynamic Configuration: Resolved via app.get(): %s as %s',
	          app.get(varName), param);
	        var appValue = app.get(varName);
	        configVariable = appValue;
	      } else {
	        // previously it returns the original string such as "${restApiRoot}"
	        // it will now return `undefined`, for the use case of
	        // dynamic datasources url:`undefined` to fallback to other parameters
	        configVariable = undefined;
	        g.warn('%s does not resolve to a valid value, returned as %s. ' +
	        '"%s" must be resolvable in Environment variable or by app.get().',
	          param, configVariable, varName);
	        debug('Dynamic Configuration: Cannot resolve variable for `%s`, ' +
	          'returned as %s', varName, configVariable);
	      }
	    }
	    return configVariable;
	  }
	
	  function interpolateVariables(config) {
	    // config is a string and contains a config variable ('${var}')
	    if (typeof config === 'string')
	      return getConfigVariable(config);
	
	    // anything but an array or object
	    if (typeof config !== 'object' || config == null)
	      return config;
	
	    // recurse into array elements
	    if (Array.isArray(config))
	      return config.map(interpolateVariables);
	
	    // Not a plain object. Examples: RegExp, Date,
	    if (!config.constructor || config.constructor !== Object)
	      return config;
	
	    // recurse into object props
	    var interpolated = {};
	    Object.keys(config).forEach(function(configKey) {
	      var value = config[configKey];
	      if (Array.isArray(value)) {
	        interpolated[configKey] = value.map(interpolateVariables);
	      } else if (typeof value === 'string') {
	        interpolated[configKey] = getConfigVariable(value);
	      } else if (value === null) {
	        interpolated[configKey] = value;
	      } else if (typeof value === 'object' && Object.keys(value).length) {
	        interpolated[configKey] = interpolateVariables(value);
	      } else {
	        interpolated[configKey] = value;
	      }
	    });
	    return interpolated;
	  }
	
	  return interpolateVariables(config);
	}
	
	function setupComponents(app, instructions) {
	  instructions.components.forEach(function(data) {
	    debug('Configuring component %j', data.sourceFile);
	    var configFn = requireNodeOrEsModule(data.sourceFile);
	    var opts = {
	      useEnvVars: true,
	    };
	    data.config = getUpdatedConfigObject(app, data.config, opts);
	    configFn(app, data.config);
	  });
	}
	
	function runBootScripts(app, instructions, callback) {
	  runScripts(app, instructions.files.boot, callback);
	}
	
	function enableAnonymousSwagger(app, instructions) {
	  // disable token requirement for swagger, if available
	  var swagger = app.remotes().exports.swagger;
	  if (!swagger) return;
	
	  var appConfig = instructions.config;
	  var requireTokenForSwagger = appConfig.swagger &&
	    appConfig.swagger.requireToken;
	  swagger.requireToken = requireTokenForSwagger || false;
	}


/***/ }),
/* 7 */
/***/ (function(module, exports) {

	module.exports = require("assert");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	module.exports = require("semver");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

	module.exports = require("debug");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

	module.exports = require("async");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	module.exports = require("path");

/***/ }),
/* 12 */
/***/ (function(module, exports) {

	module.exports = require("util");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	module.exports = require("strong-globalize");

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	// Copyright IBM Corp. 2015,2017. All Rights Reserved.
	// Node module: loopback-boot
	// This file is licensed under the MIT License.
	// License text available at https://opensource.org/licenses/MIT
	
	module.exports = function requireNodeOrEsModule(sourceFile) {
	  var exports = __webpack_require__(15)(sourceFile);
	  return exports && exports.__esModule ? exports.default : exports;
	};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

	var map = {
		"./common/models/access-key-history.js": 16,
		"./common/models/access-key.js": 17,
		"./common/models/channel-type.js": 18,
		"./common/models/channel.js": 21,
		"./common/models/city.js": 23,
		"./common/models/container.js": 24,
		"./common/models/district.js": 25,
		"./common/models/item.js": 26,
		"./common/models/managed-object.js": 27,
		"./common/models/mapping-sensor.js": 28,
		"./common/models/province.js": 29,
		"./common/models/request-key.js": 30,
		"./common/models/security-site.js": 31,
		"./common/models/security.js": 32,
		"./common/models/sensor.js": 33,
		"./common/models/site.js": 34,
		"./common/models/work-order-history.js": 35,
		"./common/models/work-order.js": 36,
		"./common/models/worker.js": 37,
		"./node_modules/body-parser": 38,
		"./node_modules/compression": 39,
		"./node_modules/cors": 40,
		"./node_modules/helmet": 41,
		"./node_modules/loopback": 2,
		"./node_modules/loopback-component-explorer/index.js": 42,
		"./node_modules/loopback/common/models/access-token.js": 43,
		"./node_modules/loopback/common/models/user.js": 44,
		"./node_modules/strong-error-handler": 45,
		"./server/boot/alarms.js": 46,
		"./server/boot/discobjects.js": 48,
		"./server/boot/equipments.js": 49,
		"./server/boot/padlocks.js": 50,
		"./server/boot/perf-counters.js": 65,
		"./server/boot/perf-measurements.js": 66,
		"./server/boot/perf-objects.js": 67
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 15;


/***/ }),
/* 16 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function(Accesskeyhistory) {
	
	};


/***/ }),
/* 17 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function(Accesskey) {
	
	};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	const HasMany = __webpack_require__(19);
	module.exports = function(Channeltype, options) {
	  Channeltype.validatesUniquenessOf('label', {
	    message: 'This channel type already exist',
	  });
	  HasMany(Channeltype, options);
	};


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

	/* Ce mixin permet de vérifier les contraintes d'intégrité (si l'objet parent possède des enfants, on ne peut pas le supprimer)
	en faisant une requête sur les enfants qui sont rattachés à ce parent */
	
	'use strict';
	
	let _ = __webpack_require__(20);
	
	let checkHasManyIntegrity = function(ctx, next) {
	  if (ctx.where) {
	    let relations = ctx.Model.definition.settings.relations;
	    let relationsArray = _.map(relations, rel => {
	      return {modelName: rel.model, fk: rel.foreignKey, type: rel.type};
	    });
	
	    /* On utilise Lodash pour transformer l'objet des relations en Tableau de la forme
	      [
	        { modelName: 'achat', fk: 'achat_id', type: 'belongsTo' },
	        { modelName: 'FED_AGENT', fk: 'agent_rfagent', type: 'belongsTo' }
	      ]
	    */
	
	    let thisModel = ctx.Model;
	    // Le message qui sera renvoyé en cas d'échec de vérification des contraintes d'intégrité
	    let message = '';
	    // Le tableau des promises correspondant aux requêtes vérifiants les contraintes
	    let promiseArray = [];
	
	    relationsArray.forEach(function(relation) {
	      if (relation.type == 'hasMany') {
	        let childrenModelName = relation.modelName;
	        let childrenModel = thisModel.app.models[childrenModelName];
	        let parentId = ctx.where.id;
	        let whereObject = {};
	        whereObject[relation.fk] = ctx.where.id;
	        // On cherche les enfants éventuels
	        promiseArray.push(childrenModel.find({
	          where: whereObject,
	        }).then(function(data) {
	          if (data.length > 0) { // Si il y a des enfants, on renvoit une erreur ';
	            message += `This ${thisModel.modelName} has many related with ${childrenModelName} and can't be deleted`;
	          }
	        }));
	      }
	    }
	    );
	
	    /* Une fois que toutes les promesses ont été déterminées et conduisent vers un message en cas de non respect de la contrainte d'intégrité,
	    on les regroupe dans une promesse commune résolue quand toutes sont résolues et qui renvoit le message en cas de non respect de contrainte */
	    Promise.all(promiseArray)
	      .then(
	      function() {
	        console.log(message);
	        next(message);
	      }
	      , console.error)
	      .catch(function(err) {
	        next(err);
	      });
	  }
	};
	
	module.exports = function(Model, options) {
	  Model.observe('before delete', checkHasManyIntegrity);
	};


/***/ }),
/* 20 */
/***/ (function(module, exports) {

	module.exports = require("lodash");

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	const BelongTo = __webpack_require__(22);
	const HasMany = __webpack_require__(19);
	module.exports = function(Channel, options) {
	  Channel.validatesUniquenessOf('label', {
	    message: 'This channel already exist',
	  });
	  BelongTo(Channel, options);
	  HasMany(Channel, options);
	};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

	/* Ce mixin permet de vérifier les contraintes d'intégrité (l'objet enfant possède bien un parent)
	en vérifiant que les clés étrangères des objets parents existes bien lors de la création d'un objet enfant */
	
	'use strict';
	
	let _ = __webpack_require__(20);
	
	let checkBelongsToIntegrity = function(ctx, next) {
	  if (ctx.instance) {
	    let relations = ctx.Model.definition.settings.relations;
	    let relationsArray = _.map(relations, rel => {
	      return {modelName: rel.model, fk: rel.foreignKey, type: rel.type};
	    });
	
	    /* On utilise Lodash pour transformer l'objet des relations en Tableau de la forme
	      [
	        { modelName: 'achat', fk: 'achat_id', type: 'belongsTo' },
	        { modelName: 'FED_AGENT', fk: 'agent_rfagent', type: 'belongsTo' }
	      ]
	    */
	
	    let thisModel = ctx.Model;
	    // Le message qui sera renvoyé en cas d'échec de vérification des contraintes d'intégrité
	    let message = '';
	    // Le tableau des promises correspondant aux requêtes vérifiants les contraintes
	    let promiseArray = [];
	
	    relationsArray.forEach(function(relation) {
	      if (relation.type == 'belongsTo') {
	        let parentModelName = relation.modelName;
	        let parentModel = thisModel.app.models[parentModelName];
	        let parentId = ctx.instance[relation.fk];
	
	        // On cherche le modèle parent qui correspond à l'id demandé pour le modèle enfant...
	        promiseArray.push(parentModel.findById(parentId).then(function(parentInstance) {
	          if (parentInstance === null) {
	            message += `No ${parentModelName} with id ${parentId}`;
	          }
	        }));
	      }
	    }
	    );
	
	    /* Une fois que toutes les promesses ont été déterminées et conduisent vers un message en cas de non respect de la contrainte d'intégrité,
	    on les regroupe dans une promesse commune résolue quand toutes sont résolues et qui renvoit le message en cas de non respect de contrainte */
	    Promise.all(promiseArray)
	      .then(
	      function() {
	        next(message);
	      }
	      , console.error)
	      .catch(function(err) {
	        next(err);
	      });
	  }
	};
	
	module.exports = function(Model, options) {
	  Model.observe('before save', checkBelongsToIntegrity);
	};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	const BelongTo = __webpack_require__(22);
	const HasMany = __webpack_require__(19);
	
	module.exports = function(City, options) {
	  City.validatesUniquenessOf('label', {
	    message: 'This city already exists'
	  });
	  City.validatesUniquenessOf('location', {
	    message: 'This location already exists'
	  });
	  BelongTo(City, options);
	  HasMany(City, options);
	};


/***/ }),
/* 24 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function(Container) {
	
	};


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	const BelongTo = __webpack_require__(22);
	const HasMany = __webpack_require__(19);
	
	module.exports = function(District, options) {
	  District.validatesUniquenessOf('label', {
	    message: 'This district already exists'
	  });
	  District.validatesUniquenessOf('location', {
	    message: 'This location already exists'
	  });
	  BelongTo(District, options);
	  HasMany(District, options);
	};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var BelongsTo = __webpack_require__(22);
	module.exports = function(Item, options) {
	  Item.validatesInclusionOf('type', {in: [
	    'atm', 'cashier', 'rack2x2', 'rack5x2', 'rack5x1',
	    'door-left1', 'door-top1', 'door-right1', 'door-bottom1',
	    'door-left2', 'door-top2', 'door-right2', 'door-bottom2',
	  ]});
	  BelongsTo(Item, options);
	};


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	const BelongTo = __webpack_require__(22);
	
	module.exports = function(Managedobject, options) {
	  BelongTo(Managedobject, options);
	};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	var BelongsTo = __webpack_require__(22);
	module.exports = function(Mappingsensor, options) {
	  BelongsTo(Mappingsensor, options);
	};


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	const HasMany = __webpack_require__(19);
	
	module.exports = function(Province, options) {
	  Province.validatesUniquenessOf('label', {
	    message: 'This province already exists'
	  });
	  Province.validatesUniquenessOf('location', {
	    message: 'This location already exists'
	  });
	  HasMany(Province, options);
	};


/***/ }),
/* 30 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function(Requestkey) {
	
	};


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var BelongTo = __webpack_require__(22);
	module.exports = function(Securitysite, options) {
	  BelongTo(Securitysite, options);
	};


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	const HasMany = __webpack_require__(19);
	
	module.exports = function(Security, options) {
	  
	  Security.validatesUniquenessOf('username', {
	    message: 'Username already exist',
	  });
	
	  Security.validatesUniquenessOf('email', {
	    message: 'Email already exist',
	  });
	
	  Security.validatesUniquenessOf('phone', {
	    message: 'Phone number already exist',
	  });
	  
	  HasMany(Security, options);
	};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	const HasMany = __webpack_require__(19);
	const BelongsTo = __webpack_require__(22);
	module.exports = function(Sensor, options) {
	  Sensor.validatesUniquenessOf('label', {
	    message: 'This sensor already exist',
	  });
	  HasMany(Sensor, options);
	  BelongsTo(Sensor, options);
	};


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	const BelongTo = __webpack_require__(22);
	const HasMany = __webpack_require__(19);
	
	module.exports = function(Site, options) {
	  Site.validatesUniquenessOf('label', {
	    message: 'This site already exists'
	  });
	  Site.validatesUniquenessOf('location', {
	    message: 'This location already exists'
	  });
	  HasMany(Site, options);
	  BelongTo(Site, options);
	};


/***/ }),
/* 35 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function(Workorderhistory) {
	
	};


/***/ }),
/* 36 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function(Workorder) {
	
	};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	const HasMany = __webpack_require__(19);
	module.exports = function(app, options) {
	  app.validatesUniquenessOf('email', { message: 'Email already exist'});
	  HasMany(app, options);
	};


/***/ }),
/* 38 */
/***/ (function(module, exports) {

	module.exports = require("body-parser");

/***/ }),
/* 39 */
/***/ (function(module, exports) {

	module.exports = require("compression");

/***/ }),
/* 40 */
/***/ (function(module, exports) {

	module.exports = require("cors");

/***/ }),
/* 41 */
/***/ (function(module, exports) {

	module.exports = require("helmet");

/***/ }),
/* 42 */
/***/ (function(module, exports) {

	module.exports = require("loopback-component-explorer/index.js");

/***/ }),
/* 43 */
/***/ (function(module, exports) {

	module.exports = require("loopback/common/models/access-token.js");

/***/ }),
/* 44 */
/***/ (function(module, exports) {

	module.exports = require("loopback/common/models/user.js");

/***/ }),
/* 45 */
/***/ (function(module, exports) {

	module.exports = require("strong-error-handler");

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var host    = 'http://alfademo.ddns.net:3001';
	var loopback = __webpack_require__(2);
	var ds = loopback.createDataSource({
	  connector: __webpack_require__(47),
	  debug: false,
	  operations: [
	    {
	      functions: {alarms: []},
	      template: {
	        method: 'GET',
	        url: `${host}/alarm/`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	    {
	      functions: {alarm: ['latitude', 'longitude']},
	      template: {
	        method: 'GET',
	        url: `${host}/alarm/{latitude}/{longitude}/`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	    {
	      functions: {history: ['startTime', 'endTime', 'latitude', 'longitude']},
	      template: {
	        method: 'GET',
	        url: `${host}/alarm/log/{startTime}/{endTime}/{latitude}/{longitude}/`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	    {
	      functions: {acknowledge: ['id', 'username']},
	      template: {
	        method: 'POST',
	        url: `${host}/alarm/acknowledge/{id}/{username}/`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	  ],
	});
	
	module.exports = (server) => {
	  let router = server.loopback.Router();
	
	  router.get('/api/alarms', (req, res)=>{
	    ds.alarms().then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  router.get('/api/alarms/:latitude/:longitude/', (req, res)=>{
	    ds.alarm(req.params.latitude, req.params.longitude)
	    .then(result=>res.send(result)).catch(err=>res.status(500).send(err));
	  });
	
	  router.get('/api/alarms/log/:startTime/:endTime/:latitude/:longitude/',
	    (req, res)=>{
	      ds.history(
	            req.params.startTime,
	            req.params.endTime,
	            req.params.latitude,
	            req.params.longitude
	        ).then(result=>res.send(result))
	        .catch(err=>res.status(500).send(err));
	    });
	
	  router.post('/api/alarms/acknowledge/:id/:username', (req, res)=>{
	    ds.acknowledge(req.params.id, req.params.username)
	    .then(result=>res.send(result)).catch(err=>res.status(500).send(err));
	  });
	
	  server.use(router);
	};


/***/ }),
/* 47 */
/***/ (function(module, exports) {

	module.exports = require("loopback-connector-rest");

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var host    = 'http://alfademo.ddns.net:3001';
	var loopback = __webpack_require__(2);
	var ds = loopback.createDataSource({
	  connector: __webpack_require__(47),
	  debug: false,
	  operations: [
	    {
	      functions: {findAll: []},
	      template: {
	        method: 'GET',
	        url: `${host}/discobjects`,
	        headers: {
	          accepts: 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	    {
	      functions: {find: ['latitude', 'longitude']},
	      template: {
	        method: 'GET',
	        url: `${host}/discobjects/location/{latitude}/{longitude}/`,
	        headers: {
	          accepts: 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	    {
	      functions: {create: ['data']},
	      template: {
	        method: 'POST',
	        url: `${host}/discobjects`,
	        body: '{data:object}',
	      },
	    },
	    {
	      functions: {remove: ['id']},
	      template: {
	        method: 'DELETE',
	        url: `${host}/discobjects/{id}`,
	        headers: {
	          accepts: 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	  ],
	});
	
	module.exports = (server) => {
	  var router = server.loopback.Router();
	
	  router.get('/api/discobjects', (req, res)=>{
	    ds.findAll().then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  router.get('/api/discobjects/:latitude/:longitude/', (req, res)=>{
	    ds.find(req.params.latitude, req.params.longitude).then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  router.post('/api/discobjects', (req, res)=>{
	    let value = req.body;
	    ds.create(value).then(result=>{
	      ds.find(value.location.latitude, value.location.longitude).then(disc=>{
	        let index = disc.findIndex(item =>
	          item.name === value.name &&
	          item.eqType.id === value.eqType.id &&
	          item.location.latitude === value.location.latitude &&
	          item.location.longitude === value.location.longitude
	        );
	        res.send(disc[index]);
	      }).catch(err=>res.status(500).send(err));
	    }).catch(err=>res.status(500).send(err));
	  });
	
	  router.delete('/api/discobjects/:id', (req, res)=>{
	    ds.remove(req.params.id).then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  server.use(router);
	};


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var host    = 'http://alfademo.ddns.net:3001';
	var loopback = __webpack_require__(2);
	var ds = loopback.createDataSource({
	  connector: __webpack_require__(47),
	  debug: false,
	  operations: [
	    {
	      functions: {find: []},
	      template: {
	        method: 'GET',
	        url: `${host}/discobjects/equipmenttype/`,
	        headers: {
	          accepts: 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	    {
	      functions: {create: ['data']},
	      template: {
	        method: 'POST',
	        url: `${host}/discobjects/equipmenttype/`,
	        body: '{data:object}',
	      },
	    },
	    {
	      functions: {remove: ['id']},
	      template: {
	        method: 'DELETE',
	        url: `${host}/discobjects/equipmenttype/{id}`,
	        headers: {
	          accepts: 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	  ],
	});
	
	module.exports = (server) => {
	  let router = server.loopback.Router();
	
	  router.get('/api/equipmenttype', (req, res)=>{
	    ds.find().then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  router.post('/api/equipmenttype', (req, res)=>{
	    ds.create(req.body).then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  router.delete('/api/equipmenttype/:id', (req, res)=>{
	    ds.remove(req.params.id).then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  server.use(router);
	};


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	var loopback = __webpack_require__(2);
	
	const app = __webpack_require__(51);
	module.exports = (server) => {
	  let router = server.loopback.Router();
	  router.post('/api/workers/:id/change-password', (req, res) => {
	    server.models.worker
	      .changePassword(req.params.id, req.body.oldPassword, req.body.newPassword)
	      .then(success => res.send(success))
	      .catch(error => res.status(500).send(error));
	  });
	
	  router.get('/api/workers/:id/workOrders', (req, res) => {
	    server.models.work_order
	      .find({
	        where: {
	          worker_id: req.params.id,
	          status: {
	            inq: ['idle', 'request', 'reject', 'approve']
	          },
	          end : { gte : Date.now()}
	        },
	        include: ['accessKeys', 'site']
	      })
	      .then(success => res.send(success))
	      .catch(error => res.status(500).send(error));
	  });
	
	  router.post('/api/accessKeys/:id/open', (req, res) => {
	    server.models.access_key.findOne({
	        where: {
	          id: req.params.id
	        },
	        include: ['workOrder']
	      })
	      .then(key => {
	        if (key) {
	          key = JSON.parse(JSON.stringify(key));
	          if (key.workOrder.status === 'approve') {
	            server.models.access_key_history.create({
	                access_key_id: req.params.id
	              })
	              .then(success => res.send(success))
	              .catch(error => res.send(error));
	          }
	        } else {
	          res.status(500).send('Access key not found');
	        }
	      })
	      .catch(error => res.status(500).send(error));
	  });
	
	
	  router.post('/api/workOrders/:id/finish', (req, res) => {
	    server.models.work_order.findById(req.params.id)
	      .then(order => {
	        if (order) {
	          Promise.all([
	            server.models.work_order_history.create({
	              work_order_id: req.params.id,
	              picture: req.body.picture,
	              status: 'finish'
	            }),
	            server.models.work_order.replaceById(req.params.id, {
	              status: 'finish',
	              worker_id: order.worker_id,
	              site_id: order.site_id,
	              start: order.start,
	              end: order.end,
	              project: order.project,
	              activity: order.activity
	            })
	          ]).then(success => res.send()).catch(error  => res.status(500).send(error));
	        } else {
	          res.status(500).send('Work order not found');
	        }
	      })
	  });
	
	  router.post('/api/workOrders/:id/request', (req, res) => {
	    server.models.work_order.findById(req.params.id)
	      .then(order => {
	        if (order) {
	          if (order.status === 'request') {
	            res.status(500).send('Work order already request');
	          } else if (order.status === 'expired' || order.status === 'finish' || order.status === 'approve') {
	            res.status(500).send('status work order ' + order.status);
	          } else {
	            Promise.all([
	                server.models.request_key.create({
	                  work_order_id: req.params.id,
	                  picture: req.body.picture
	                }),
	                server.models.work_order_history.create({
	                  work_order_id: req.params.id,
	                  picture: req.body.picture,
	                  status: 'request'
	                })
	              ]).then(success => {
	                server.models.work_order.replaceById(req.params.id, {
	                  status: 'request',
	                  worker_id: order.worker_id,
	                  site_id: order.site_id,
	                  start: order.start,
	                  end: order.end,
	                  project: order.project,
	                  activity: order.activity
	                });
	                app.io.emit('request.key', JSON.stringify(success[0]));
	                res.send();
	              })
	              .catch(error => res.send(error))
	          }
	        } else {
	          res.status(500).send('Work order not available')
	        }
	      });
	  });
	
	
	  server.use(router);
	};


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(module) {'use strict';
	
	var loopback = __webpack_require__(2);
	var boot = __webpack_require__(53);
	
	var app = module.exports = loopback();
	
	app.start = function() {
	  // start the web server
	  return app.listen(function() {
	    app.emit('started');
	    var baseUrl = app.get('url').replace(/\/$/, '');
	    console.log('Web server listening at: %s', baseUrl);
	    if (app.get('loopback-component-explorer')) {
	      var explorerPath = app.get('loopback-component-explorer').mountPath;
	      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
	    }
	  });
	};
	
	var kafka = __webpack_require__(62);
	var Consumer = kafka.Consumer;
	var client = new kafka.Client('localhost:2181/');
	var topics = [{topic: 'alarm.data'}, {topic: 'perfmeasurement.data'}];
	var options = {fromOffset: 'latest', fromBeginning: false};
	var consumer = new Consumer(client, topics, options);
	var offset = new kafka.Offset(client);
	const winston = __webpack_require__(63);
	
	// Bootstrap the application, configure models, datasources and middleware.
	// Sub-apps like REST API are mounted via boot scripts.
	boot(app, __dirname, function(err) {
	  if (err) throw err;
	  // start the server if `$ node server.js`
	  if (__webpack_require__.c[0] === module) {
	    app.io = __webpack_require__(64)(app.start());
	  }
	
	  client.on('error', err => winston.error('Tidak bisa connect ke kafka', err));
	  consumer.on('error', err => winston.error('Tidak bisa konek consumer message kafka', err));
	  consumer.on('offsetOutOfRange', topic => {
	    winston.info('offset out of range', topic);
	    topic.maxNum = 2;
	    offset.fetch([topic], (err, offsets) => {
	        var min = Math.min.apply(null, offsets[topic.topic][topic.partition]);
	        consumer.setOffset(topic.topic, topic.partition, min);
	    });
	  });
	
	  consumer.on('message', kafka => (
	    app.io.emit(kafka.topic, kafka.value),
	    winston.info('Berhasil menerima message', kafka)
	  ));
	
	});
	
	
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(52)(module)))

/***/ }),
/* 52 */
/***/ (function(module, exports) {

	module.exports = require("webpack/buildin/module.js");

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

	// Copyright IBM Corp. 2014,2016. All Rights Reserved.
	// Node module: loopback-boot
	// This file is licensed under the MIT License.
	// License text available at https://opensource.org/licenses/MIT
	
	// Strong globalize
	var SG = __webpack_require__(13);
	SG.SetRootDir(__dirname);
	
	var ConfigLoader = __webpack_require__(54);
	var compile = __webpack_require__(57);
	var execute = __webpack_require__(6);
	var addInstructionsToBrowserify = __webpack_require__(60);
	var utils = __webpack_require__(55);
	
	/**
	 * Initialize an application from an options object or
	 * a set of JSON and JavaScript files.
	 *
	 * > **NOTE**: This module is primarily intended for use with LoopBack 2.0.
	 * It _does_ work with LoopBack 1.x applications, but
	 * none of the LoopBack 1.x examples or generated code (scaffolding) use it.
	 *
	 * This function takes an optional argument that is either a string
	 * or an object.
	 *
	 * If the argument is a string, then it sets the application root directory
	 * based on the string value. Then it:
	 *
	 *  1. Creates DataSources from the `datasources.json` file in the application
	 *   root directory.
	 *
	 *  2. Configures Models from the `model-config.json` file in the application
	 *    root directory.
	 *
	 *  3. Configures the LoopBack Application object from the `config.json` file
	 *     in the application root directory. These properties can be accessed
	 *     using `app.get('propname')`.
	 *
	 * If the argument is an object, then it looks for `models`, `dataSources`,
	 * 'config', `modelsRootDir`, `dsRootDir`, `appConfigRootDir` and `appRootDir`
	 * properties of the object.
	 *
	 * If the object has no `appRootDir` property then it sets the current working
	 * directory as the application root directory.
	 *
	 * The execution environment, {env}, is established from, in order,
	 *  - `options.env`
	 *  - `process.env.NODE_ENV`,
	 *  - the literal `development`.
	 *
	 * Then it:
	 *
	 *  1. Creates DataSources from the `options.dataSources` object, if provided;
	 *    otherwise, it searches for the files
	 *     - `datasources.json`,
	 *     - `datasources.local.js` or `datasources.local.json` (only one),
	 *     - `datasources.{env}.js` or `datasources.{env}.json` (only one)
	 *
	 *    in the directory designated by 'options.dsRootDir', if present, or the
	 *    application root directory. It merges the data source definitions from
	 *    the files found.
	 *
	 *  2. Creates Models from the `options.models` object, if provided;
	 *    otherwise, it searches for the files
	 *     - `model-config.json`,
	 *     - `model-config.local.js` or `model-config.local.json` (only one),
	 *     - `model-config.{env}.js` or `model-config.{env}.json` (only one)
	 *
	 *    in the directory designated by 'options.modelsRootDir', if present, or
	 *    the application root directory. It merges the model definitions from the
	 *    files found.
	 *
	 *  3. Configures the Application object from the `options.config` object,
	 *    if provided;
	 *    otherwise, it searches for the files
	 *     - `config.json`,
	 *     - `config.local.js` or `config.local.json` (only one),
	 *     - `config.{env}.js` or `config.{env}.json` (only one)
	 *
	 *    in the directory designated by 'options.appConfigRootDir', if present, or
	 *    the application root directory. It merges the properties from the files
	 *    found.
	 *
	 * In both cases, the function loads JavaScript files in the
	 * `/boot` subdirectory of the application root directory with `require()`.
	 *
	 *  **NOTE:** The version 2.0 of loopback-boot changed the way how models
	 *  are created. The `model-config.json` file contains only configuration
	 *  options like dataSource and extra relations. To define a model,
	 *  create a per-model JSON file in `models/` directory.
	 *
	 *  **NOTE:** Mixing `bootLoopBackApp(app, bootConfig)` and
	 *  `app.model(name, modelConfig)` in multiple
	 *  files may result in models being undefined due to race conditions.
	 *  To avoid this when using `bootLoopBackApp()` make sure all models are passed
	 *  as part of the `models` definition.
	 *
	 * Throws an error if the config object is not valid or if boot fails.
	 *
	 * @param app LoopBack application created by `loopback()`.
	 * @options {String|Object} options Boot options; If String, this is
	 * the application root directory; if object, has below properties.
	 * @property {String} [appRootDir] Directory to use when loading JSON and
	 * JavaScript files.
	 * Defaults to the current directory (`process.cwd()`).
	 * @property {String} [appConfigRootDir] Directory to use when loading
	 * `config.json`. Defaults to `appRootDir`.
	 * @property {Object} [models] Object containing `Model` configurations.
	 * @property {Array} [modelDefinitions] List of model definitions to use.
	 *   When `options.modelDefinitions` is provided, loopback-boot does not
	 *   search filesystem and use only the models provided in this argument.
	 * @property {Object} [dataSources] Object containing `DataSource` definitions.
	 * @property {String} [modelsRootDir] Directory to use when loading
	 * `model-config.json`. Defaults to `appRootDir`.
	 * @property {String} [dsRootDir] Directory to use when loading
	 * `datasources.json`. Defaults to `appRootDir`.
	 * @property {String} [middlewareRootDir] Directory to use when loading
	 * `middleware.json`. Defaults to `appRootDir`.
	 * @property {String} [componentRootDir] Directory to use when loading
	 * `component-config.json`. Defaults to `appRootDir`.
	 * @property {String} [env] Environment type, defaults to `process.env.NODE_ENV`
	 * or `development`. Common values are `development`, `staging` and
	 * `production`; however the applications are free to use any names.
	 * @property {Array.<String>} [modelSources] List of directories where to look
	 * for files containing model definitions.
	 * @property {Object} [middleware] Middleware configuration to use instead
	 * of `{appRootDir}/middleware.json`
	 * @property {Object} [components] Component configuration to use instead
	 * of `{appRootDir}/component-config.json`
	 * @property {Array.<String>} [mixinDirs] List of directories where to look
	 * for files containing model mixin definitions. All files (mixins) found
	 * in these directory are loaded.
	 * @property {Array.<String>} [mixinSources] List of directories where to look
	 * for files containing model mixin definitions. Only mixins used by
	 * application models are loaded from these directories.
	 * @property {Array.<String>} [bootDirs] List of directories where to look
	 * for boot scripts.
	 * @property {Array.<String>} [bootScripts] List of script files to execute
	 * on boot.
	 * @property {String|Function|Boolean} [normalization] Mixin normalization
	 * format: false, 'none', 'classify', 'dasherize' - defaults to 'classify'.
	 * @end
	 * @param {Function} [callback] Callback function.
	 *
	 * @header boot(app, [options], [callback])
	 */
	
	exports = module.exports = function bootLoopBackApp(app, options, callback) {
	  // backwards compatibility with loopback's app.boot
	  options.env = options.env || app.get('env');
	
	  var instructions = compile(options);
	  execute(app, instructions, callback);
	};
	
	/**
	 * Compile boot instructions and add them to a browserify bundler.
	 * @param {Object|String} options as described in `bootLoopBackApp` above.
	 * @property {String} [appId] Application identifier used to load the correct
	 * boot configuration when building multiple applications using browserify.
	 * @end
	 * @param {Object} bundler A browserify bundler created by `browserify()`.
	 *
	 * @header boot.compileToBrowserify(options, bundler)
	 */
	exports.compileToBrowserify = function(options, bundler) {
	  addInstructionsToBrowserify(compile(options), bundler);
	};
	
	/* -- undocumented low-level API -- */
	
	exports.ConfigLoader = ConfigLoader;
	exports.compile = compile;
	exports.execute = execute;
	exports.utils = utils;
	exports.addInstructionsToBrowserify = addInstructionsToBrowserify;


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

	// Copyright IBM Corp. 2014,2016. All Rights Reserved.
	// Node module: loopback-boot
	// This file is licensed under the MIT License.
	// License text available at https://opensource.org/licenses/MIT
	
	var cloneDeep = __webpack_require__(20).cloneDeep;
	var path = __webpack_require__(11);
	var utils = __webpack_require__(55);
	var debug = __webpack_require__(9)('loopback:boot:config-loader');
	var assert = __webpack_require__(7);
	var g = __webpack_require__(13)();
	
	var ConfigLoader = exports;
	
	/**
	 * Load application config from `config.json` and friends.
	 * @param {String} rootDir Directory where to look for files.
	 * @param {String} env Environment, usually `process.env.NODE_ENV`
	 * @returns {Object}
	 */
	ConfigLoader.loadAppConfig = function(rootDir, env) {
	  return loadNamed(rootDir, env, 'config', mergeAppConfig);
	};
	
	/**
	 * Load data-sources config from `datasources.json` and friends.
	 * @param {String} rootDir Directory where to look for files.
	 * @param {String} env Environment, usually `process.env.NODE_ENV`
	 * @returns {Object}
	 */
	ConfigLoader.loadDataSources = function(rootDir, env) {
	  return loadNamed(rootDir, env, 'datasources', mergeDataSourceConfig);
	};
	
	/**
	 * Load model config from `model-config.json` and friends.
	 * @param {String} rootDir Directory where to look for files.
	 * @param {String} env Environment, usually `process.env.NODE_ENV`
	 * @returns {Object}
	 */
	ConfigLoader.loadModels = function(rootDir, env) {
	  return loadNamed(rootDir, env, 'model-config', mergeModelConfig);
	};
	
	/**
	 * Load middleware config from `middleware.json` and friends.
	 * @param {String} rootDir Directory where to look for files.
	 * @param {String} env Environment, usually `process.env.NODE_ENV`
	 * @returns {Object}
	 */
	ConfigLoader.loadMiddleware = function(rootDir, env) {
	  return loadNamed(rootDir, env, 'middleware', mergeMiddlewareConfig);
	};
	
	/**
	 * Load component config from `component-config.json` and friends.
	 * @param {String} rootDir Directory where to look for files.
	 * @param {String} env Environment, usually `process.env.NODE_ENV`
	 * @returns {Object}
	 */
	ConfigLoader.loadComponents = function(rootDir, env) {
	  return loadNamed(rootDir, env, 'component-config', mergeComponentConfig);
	};
	
	/*-- Implementation --*/
	
	/**
	 * Load named configuration.
	 * @param {String} rootDir Directory where to look for files.
	 * @param {String} env Environment, usually `process.env.NODE_ENV`
	 * @param {String} name
	 * @param {function(target:Object, config:Object, filename:String)} mergeFn
	 * @returns {Object}
	 */
	function loadNamed(rootDir, env, name, mergeFn) {
	  var files = findConfigFiles(rootDir, env, name);
	  if (files.length) {
	    debug('found %s %s files', env, name);
	    files.forEach(function(f) { debug('  %s', f); });
	  }
	  var configs = loadConfigFiles(files);
	  var merged = mergeConfigurations(configs, mergeFn);
	
	  debug('merged %s %s configuration %j', env, name, merged);
	
	  return merged;
	}
	
	/**
	 * Search `appRootDir` for all files containing configuration for `name`.
	 * @param {String} appRootDir
	 * @param {String} env Environment, usually `process.env.NODE_ENV`
	 * @param {String} name
	 * @returns {Array.<String>} Array of absolute file paths.
	 */
	function findConfigFiles(appRootDir, env, name) {
	  var master = ifExists(name + '.json');
	  if (!master && (ifExistsWithAnyExt(name + '.local') ||
	    ifExistsWithAnyExt(name + '.' + env))) {
	    g.warn('WARNING: Main {{config}} file "%s.json" is missing', name);
	  }
	  if (!master) return [];
	
	  var candidates = [
	    master,
	    ifExistsWithAnyExt(name + '.local'),
	    ifExistsWithAnyExt(name + '.' + env),
	  ];
	
	  return candidates.filter(function(c) { return c !== undefined; });
	
	  function ifExists(fileName) {
	    var filepath = path.resolve(appRootDir, fileName);
	    return utils.fileExistsSync(filepath) ? filepath : undefined;
	  }
	
	  function ifExistsWithAnyExt(fileName) {
	    return ifExists(fileName + '.js') || ifExists(fileName + '.json');
	  }
	}
	
	/**
	 * Load configuration files into an array of objects.
	 * Attach non-enumerable `_filename` property to each object.
	 * @param {Array.<String>} files
	 * @returns {Array.<Object>}
	 */
	function loadConfigFiles(files) {
	  return files.map(function(f) {
	    var config = cloneDeep(__webpack_require__(15)(f));
	    Object.defineProperty(config, '_filename', {
	      enumerable: false,
	      value: f,
	    });
	    debug('loaded config file %s: %j', f, config);
	    return config;
	  });
	}
	
	/**
	 * Merge multiple configuration objects into a single one.
	 * @param {Array.<Object>} configObjects
	 * @param {function(target:Object, config:Object, filename:String)} mergeFn
	 */
	function mergeConfigurations(configObjects, mergeFn) {
	  var result = configObjects.shift() || {};
	  while (configObjects.length) {
	    var next = configObjects.shift();
	    mergeFn(result, next, next._filename);
	  }
	  return result;
	}
	
	function mergeDataSourceConfig(target, config, fileName) {
	  var err = mergeObjects(target, config);
	  if (err) {
	    throw new Error(g.f('Cannot apply %s: %s', fileName, err));
	  }
	}
	
	function mergeModelConfig(target, config, fileName) {
	  var err = mergeObjects(target, config);
	  if (err) {
	    throw new Error(g.f('Cannot apply %s: %s', fileName, err));
	  }
	}
	
	function mergeAppConfig(target, config, fileName) {
	  var err = mergeObjects(target, config);
	  if (err) {
	    throw new Error(g.f('Cannot apply %s: %s', fileName, err));
	  }
	}
	
	function mergeMiddlewareConfig(target, config, fileName) {
	  var err = undefined; // see https://github.com/eslint/eslint/issues/5744
	  for (var phase in config) {
	    if (phase in target) {
	      err = mergePhaseConfig(target[phase], config[phase], phase);
	    } else {
	      err = g.f('The {{phase}} "%s" is not defined in the main config.', phase);
	    }
	    if (err)
	      throw new Error(g.f('Cannot apply %s: %s', fileName, err));
	  }
	}
	
	function mergeNamedItems(arr1, arr2, key) {
	  assert(Array.isArray(arr1), g.f('invalid array: %s', arr1));
	  assert(Array.isArray(arr2), g.f('invalid array: %s', arr2));
	  key = key || 'name';
	  var result = [].concat(arr1);
	  for (var i = 0, n = arr2.length; i < n; i++) {
	    var item = arr2[i];
	    var found = false;
	    if (item[key]) {
	      for (var j = 0, k = result.length; j < k; j++) {
	        if (result[j][key] === item[key]) {
	          mergeObjects(result[j], item);
	          found = true;
	          break;
	        }
	      }
	    }
	    if (!found) {
	      result.push(item);
	    }
	  }
	  return result;
	}
	
	function mergePhaseConfig(target, config, phase) {
	  var err = undefined; // see https://github.com/eslint/eslint/issues/5744
	  for (var mw in config) {
	    if (mw in target) {
	      var targetMiddleware = target[mw];
	      var configMiddleware = config[mw];
	      if (Array.isArray(targetMiddleware) && Array.isArray(configMiddleware)) {
	        // Both are arrays, combine them
	        target[mw] = mergeNamedItems(targetMiddleware, configMiddleware);
	      } else if (Array.isArray(targetMiddleware)) {
	        if (typeof configMiddleware === 'object' &&
	          Object.keys(configMiddleware).length) {
	          // Config side is an non-empty object
	          target[mw] = mergeNamedItems(targetMiddleware, [configMiddleware]);
	        }
	      } else if (Array.isArray(configMiddleware)) {
	        if (typeof targetMiddleware === 'object' &&
	          Object.keys(targetMiddleware).length) {
	          // Target side is an non-empty object
	          target[mw] = mergeNamedItems([targetMiddleware], configMiddleware);
	        } else {
	          // Target side is empty
	          target[mw] = configMiddleware;
	        }
	      } else {
	        err = mergeObjects(targetMiddleware, configMiddleware);
	      }
	    } else {
	      err = g.f('The {{middleware}} "%s" in phase "%s"' +
	        'is not defined in the main config.', mw, phase);
	    }
	    if (err) return err;
	  }
	}
	
	function mergeComponentConfig(target, config, fileName) {
	  var err = mergeObjects(target, config);
	  if (err) {
	    throw new Error(g.f('Cannot apply %s: %s', fileName, err));
	  }
	}
	
	function mergeObjects(target, config, keyPrefix) {
	  for (var key in config) {
	    var fullKey = keyPrefix ? keyPrefix + '.' + key : key;
	    var err = mergeSingleItemOrProperty(target, config, key, fullKey);
	    if (err) return err;
	  }
	  return null; // no error
	}
	
	function mergeSingleItemOrProperty(target, config, key, fullKey) {
	  var origValue = target[key];
	  var newValue = config[key];
	
	  if (!hasCompatibleType(origValue, newValue)) {
	    return 'Cannot merge values of incompatible types for the option `' +
	      fullKey + '`.';
	  }
	
	  if (Array.isArray(origValue)) {
	    return mergeArrays(origValue, newValue, fullKey);
	  }
	
	  if (newValue !== null && typeof origValue === 'object') {
	    return mergeObjects(origValue, newValue, fullKey);
	  }
	
	  target[key] = newValue;
	  return null; // no error
	}
	
	function mergeArrays(target, config, keyPrefix) {
	  if (target.length !== config.length) {
	    return 'Cannot merge array values of different length' +
	      ' for the option `' + keyPrefix + '`.';
	  }
	
	  // Use for(;;) to iterate over undefined items, for(in) would skip them.
	  for (var ix = 0; ix < target.length; ix++) {
	    var fullKey = keyPrefix + '[' + ix + ']';
	    var err = mergeSingleItemOrProperty(target, config, ix, fullKey);
	    if (err) return err;
	  }
	
	  return null; // no error
	}
	
	function hasCompatibleType(origValue, newValue) {
	  if (origValue === null || origValue === undefined)
	    return true;
	
	  if (Array.isArray(origValue))
	    return Array.isArray(newValue);
	
	  if (typeof origValue === 'object')
	    return typeof newValue === 'object';
	
	  // Note: typeof Array() is 'object' too,
	  // we don't need to explicitly check array types
	  return typeof newValue !== 'object';
	}


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

	// Copyright IBM Corp. 2014,2016. All Rights Reserved.
	// Node module: loopback-boot
	// This file is licensed under the MIT License.
	// License text available at https://opensource.org/licenses/MIT
	
	var fs = __webpack_require__(56);
	
	exports.fileExistsSync = fileExistsSync;
	
	/**
	 * Check synchronously if a filepath points to an existing file.
	 * Replaces calls to fs.existsSync, which is deprecated (see:
	 * https://github.com/nodejs/node/pull/166).
	 *
	 * @param   {String} filepath The absolute path to check
	 * @returns {Boolean}  True if the file exists
	 */
	function fileExistsSync(filepath) {
	  try {
	    fs.statSync(filepath);
	    return true;
	  } catch (e) {
	    return false;
	  }
	}


/***/ }),
/* 56 */
/***/ (function(module, exports) {

	module.exports = require("fs");

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

	// Copyright IBM Corp. 2014,2016. All Rights Reserved.
	// Node module: loopback-boot
	// This file is licensed under the MIT License.
	// License text available at https://opensource.org/licenses/MIT
	
	var assert = __webpack_require__(7);
	var cloneDeep = __webpack_require__(20).cloneDeep;
	var fs = __webpack_require__(56);
	var path = __webpack_require__(11);
	var toposort = __webpack_require__(58);
	var ConfigLoader = __webpack_require__(54);
	var utils = __webpack_require__(55);
	var debug = __webpack_require__(9)('loopback:boot:compiler');
	var Module = __webpack_require__(59);
	var _ = __webpack_require__(20);
	var g = __webpack_require__(13)();
	var requireNodeOrEsModule = __webpack_require__(14);
	
	var FILE_EXTENSION_JSON = '.json';
	
	function arrayToObject(array) {
	  return array.reduce(function(obj, val) {
	    obj[val] = val;
	    return obj;
	  }, {});
	}
	
	/**
	 * Gather all bootstrap-related configuration data and compile it into
	 * a single object containing instruction for `boot.execute`.
	 *
	 * @options {String|Object} options Boot options; If String, this is
	 * the application root directory; if object, has the properties
	 * described in `bootLoopBackApp` options above.
	 * @return {Object}
	 *
	 * @header boot.compile(options)
	 */
	
	module.exports = function compile(options) {
	  options = options || {};
	
	  if (typeof options === 'string') {
	    options = { appRootDir: options };
	  }
	
	  // For setting properties without modifying the original object
	  options = Object.create(options);
	
	  var appRootDir = options.appRootDir = options.appRootDir || process.cwd();
	  var env = options.env || process.env.NODE_ENV || 'development';
	  var scriptExtensions = options.scriptExtensions ?
	    arrayToObject(options.scriptExtensions) :
	    (void 0);
	
	  var appConfigRootDir = options.appConfigRootDir || appRootDir;
	  var appConfig = options.config ||
	    ConfigLoader.loadAppConfig(appConfigRootDir, env);
	  assertIsValidConfig('app', appConfig);
	
	  var modelsRootDir = options.modelsRootDir || appRootDir;
	  var modelsConfig = options.models ||
	    ConfigLoader.loadModels(modelsRootDir, env);
	  assertIsValidModelConfig(modelsConfig);
	
	  var dsRootDir = options.dsRootDir || appRootDir;
	  var dataSourcesConfig = options.dataSources ||
	    ConfigLoader.loadDataSources(dsRootDir, env);
	  assertIsValidConfig('data source', dataSourcesConfig);
	
	  var middlewareRootDir = options.middlewareRootDir || appRootDir;
	
	  var middlewareConfig = options.middleware ||
	    ConfigLoader.loadMiddleware(middlewareRootDir, env);
	  var middlewareInstructions =
	    buildMiddlewareInstructions(middlewareRootDir, middlewareConfig);
	
	  var componentRootDir = options.componentRootDir || appRootDir;
	  var componentConfig = options.components ||
	      ConfigLoader.loadComponents(componentRootDir, env);
	  var componentInstructions =
	    buildComponentInstructions(componentRootDir, componentConfig);
	
	  // require directories
	  var bootDirs = options.bootDirs || []; // precedence
	  bootDirs = bootDirs.concat(path.join(appRootDir, 'boot'));
	  resolveRelativePaths(bootDirs, appRootDir);
	
	  var bootScripts = options.bootScripts || [];
	  resolveRelativePaths(bootScripts, appRootDir);
	
	  bootDirs.forEach(function(dir) {
	    bootScripts = bootScripts.concat(findScripts(dir, scriptExtensions));
	    var envdir = dir + '/' + env;
	    bootScripts = bootScripts.concat(findScripts(envdir, scriptExtensions));
	  });
	
	  // de-dedup boot scripts -ERS
	  // https://github.com/strongloop/loopback-boot/issues/64
	  bootScripts = _.uniq(bootScripts);
	
	  var modelsMeta = modelsConfig._meta || {};
	  delete modelsConfig._meta;
	
	  var modelSources = options.modelSources || modelsMeta.sources || ['./models'];
	  var modelInstructions = buildAllModelInstructions(
	    modelsRootDir, modelsConfig, modelSources, options.modelDefinitions,
	    scriptExtensions);
	
	  var mixinSources = options.mixinSources || modelsMeta.mixins || ['./mixins'];
	  var mixinInstructions = buildAllMixinInstructions(
	    appRootDir, options, mixinSources, scriptExtensions, modelInstructions);
	
	  // When executor passes the instruction to loopback methods,
	  // loopback modifies the data. Since we are loading the data using `require`,
	  // such change affects also code that calls `require` for the same file.
	  var instructions = {
	    env: env,
	    config: appConfig,
	    dataSources: dataSourcesConfig,
	    models: modelInstructions,
	    middleware: middlewareInstructions,
	    components: componentInstructions,
	    mixins: mixinInstructions,
	    files: {
	      boot: bootScripts,
	    },
	  };
	
	  if (options.appId)
	    instructions.appId = options.appId;
	
	  return cloneDeep(instructions);
	};
	
	function assertIsValidConfig(name, config) {
	  if (config) {
	    assert(typeof config === 'object',
	      g.f('%s config must be a valid JSON object', name));
	  }
	}
	
	function assertIsValidModelConfig(config) {
	  assertIsValidConfig('model', config);
	  for (var name in config) {
	    var entry = config[name];
	    var options = entry.options || {};
	    var unsupported = entry.properties ||
	      entry.base || options.base ||
	      entry.plural || options.plural;
	
	    if (unsupported) {
	      throw new Error(
	        g.f('The data in {{model-config.json}}' +
	          ' is in the unsupported 1.x format.'));
	    }
	  }
	}
	
	/**
	 * Find all javascript files (except for those prefixed with _)
	 * and all directories.
	 * @param {String} dir Full path of the directory to enumerate.
	 * @return {Array.<String>} A list of absolute paths to pass to `require()`.
	 * @private
	 */
	
	function findScripts(dir, scriptExtensions) {
	  assert(dir, g.f('cannot require directory contents without directory name'));
	
	  var files = tryReadDir(dir);
	  scriptExtensions = scriptExtensions || (void 0);
	
	  // sort files in lowercase alpha for linux
	  files.sort(function(a, b) {
	    a = a.toLowerCase();
	    b = b.toLowerCase();
	
	    if (a < b) {
	      return -1;
	    } else if (b < a) {
	      return 1;
	    } else {
	      return 0;
	    }
	  });
	
	  var results = [];
	  files.forEach(function(filename) {
	    // ignore index.js and files prefixed with underscore
	    if (filename === 'index.js' || filename[0] === '_') {
	      return;
	    }
	
	    var filepath = path.resolve(path.join(dir, filename));
	    var stats = fs.statSync(filepath);
	
	    // only require files supported by specified extensions
	    if (stats.isFile()) {
	      if (scriptExtensions && isPreferredExtension(filename, scriptExtensions))
	        results.push(filepath);
	      else
	        debug('Skipping file %s - unknown extension', filepath);
	    } else {
	      debug('Skipping directory %s', filepath);
	    }
	  });
	
	  return results;
	}
	
	function tryReadDir() {
	  try {
	    return fs.readdirSync.apply(fs, arguments);
	  } catch (e) {
	    return [];
	  }
	}
	
	function buildAllModelInstructions(rootDir, modelsConfig, sources,
	                                   modelDefinitions, scriptExtensions) {
	  var registry = verifyModelDefinitions(rootDir, modelDefinitions,
	                                        scriptExtensions);
	  if (!registry) {
	    registry = findModelDefinitions(rootDir, sources, scriptExtensions);
	  }
	
	  var modelNamesToBuild = addAllBaseModels(registry, Object.keys(modelsConfig));
	
	  var instructions = modelNamesToBuild
	    .map(function createModelInstructions(name) {
	      var config = modelsConfig[name];
	      var definition = registry[name] || {};
	
	      debug('Using model "%s"\nConfiguration: %j\nDefinition %j',
	        name, config, definition.definition);
	
	      return {
	        name: name,
	        config: config,
	        definition: definition.definition,
	        sourceFile: definition.sourceFile,
	      };
	    });
	
	  return sortByInheritance(instructions);
	}
	
	function addAllBaseModels(registry, modelNames) {
	  var result = [];
	  var visited = {};
	
	  while (modelNames.length) {
	    var name = modelNames.shift();
	
	    if (visited[name]) continue;
	    visited[name] = true;
	    result.push(name);
	
	    var definition = registry[name] && registry[name].definition;
	    if (!definition) continue;
	
	    var base = getBaseModelName(definition);
	
	    // ignore built-in models like User
	    if (!registry[base]) continue;
	
	    modelNames.push(base);
	  }
	
	  return result;
	}
	
	function getBaseModelName(modelDefinition) {
	  if (!modelDefinition)
	    return undefined;
	
	  return modelDefinition.base ||
	    modelDefinition.options && modelDefinition.options.base;
	}
	
	function sortByInheritance(instructions) {
	  // create edges Base name -> Model name
	  var edges = instructions
	    .map(function(inst) {
	      return [getBaseModelName(inst.definition), inst.name];
	    });
	
	  var sortedNames = toposort(edges);
	
	  var instructionsByModelName = {};
	  instructions.forEach(function(inst) {
	    instructionsByModelName[inst.name] = inst;
	  });
	
	  return sortedNames
	    // convert to instructions
	    .map(function(name) {
	      return instructionsByModelName[name];
	    })
	    // remove built-in models
	    .filter(function(inst) {
	      return !!inst;
	    });
	}
	
	function verifyModelDefinitions(rootDir, modelDefinitions, scriptExtensions) {
	  if (!modelDefinitions || modelDefinitions.length < 1) {
	    return undefined;
	  }
	
	  var registry = {};
	  modelDefinitions.forEach(function(definition, idx) {
	    if (definition.sourceFile) {
	      var fullPath = path.resolve(rootDir, definition.sourceFile);
	      definition.sourceFile = fixFileExtension(
	        fullPath,
	        tryReadDir(path.dirname(fullPath)),
	        scriptExtensions);
	      if (!definition.sourceFile) {
	        debug('Model source code not found: %s - %s', definition.sourceFile);
	      }
	    }
	
	    debug('Found model "%s" - %s %s',
	      definition.definition.name,
	      'from options',
	      definition.sourceFile ?
	        path.relative(rootDir, definition.sourceFile) :
	        '(no source file)');
	
	    var modelName = definition.definition.name;
	    if (!modelName) {
	      debug('Skipping model definition without Model name ' +
	        '(from options.modelDefinitions @ index %s)',
	         idx);
	      return;
	    }
	    registry[modelName] = definition;
	  });
	
	  return registry;
	}
	
	function findModelDefinitions(rootDir, sources, scriptExtensions) {
	  var registry = {};
	
	  sources.forEach(function(src) {
	    var srcDir = tryResolveAppPath(rootDir, src, { strict: false });
	    if (!srcDir) {
	      debug('Skipping unknown module source dir %j', src);
	      return;
	    }
	
	    var files = tryReadDir(srcDir);
	
	    files
	      .filter(function(f) {
	        return f[0] !== '_' && path.extname(f) === '.json';
	      })
	      .forEach(function(f) {
	        var fullPath = path.resolve(srcDir, f);
	        var entry = loadModelDefinition(rootDir, fullPath, files,
	                                        scriptExtensions);
	        var modelName = entry.definition.name;
	        if (!modelName) {
	          debug('Skipping model definition without Model name: %s',
	            path.relative(srcDir, fullPath));
	          return;
	        }
	        registry[modelName] = entry;
	      });
	  });
	
	  return registry;
	}
	
	function resolveAppPath(rootDir, relativePath, resolveOptions) {
	  var resolvedPath = tryResolveAppPath(rootDir, relativePath, resolveOptions);
	  if (resolvedPath === undefined && !resolveOptions.optional) {
	    var err = new Error(g.f('Cannot resolve path "%s"', relativePath));
	    err.code = 'PATH_NOT_FOUND';
	    throw err;
	  }
	  return resolvedPath;
	}
	
	function tryResolveAppPath(rootDir, relativePath, resolveOptions) {
	  var fullPath;
	  var start = relativePath.substring(0, 2);
	
	  /* In order to retain backward compatibility, we need to support
	   * two ways how to treat values that are not relative nor absolute
	   * path (e.g. `relativePath = 'foobar'`)
	   *  - `resolveOptions.strict = true` searches in `node_modules` only
	   *  - `resolveOptions.strict = false` attempts to resolve the value
	   *     as a relative path first before searching `node_modules`
	   */
	  resolveOptions = resolveOptions || { strict: true };
	
	  var isModuleRelative = false;
	  // would love to use `path.isAbsolute(relativePath)` from node's core module `path`
	  // but unfortunately that is not available in node v0.10.x
	  // https://nodejs.org/dist/latest-v6.x/docs/api/path.html#path_path_isabsolute_path
	  if (relativePath[0] === '/' || /^[a-zA-Z]:[\\]{1,2}/.test(relativePath)) {
	    fullPath = relativePath;
	  } else if (start === './' || start === '..') {
	    fullPath = path.resolve(rootDir, relativePath);
	  } else if (!resolveOptions.strict) {
	    isModuleRelative = true;
	    fullPath = path.resolve(rootDir, relativePath);
	  }
	
	  if (fullPath) {
	    // This check is needed to support paths pointing to a directory
	    if (utils.fileExistsSync(fullPath)) {
	      return fullPath;
	    }
	
	    try {
	      fullPath = /*require.resolve*/(__webpack_require__(15).resolve(fullPath));
	      return fullPath;
	    } catch (err) {
	      if (!isModuleRelative) {
	        debug ('Skipping %s - %s', fullPath, err);
	        return undefined;
	      }
	    }
	  }
	
	  // Handle module-relative path, e.g. `loopback/common/models`
	
	  // Module.globalPaths is a list of globally configured paths like
	  //   [ env.NODE_PATH values, $HOME/.node_modules, etc. ]
	  // Module._nodeModulePaths(rootDir) returns a list of paths like
	  //   [ rootDir/node_modules, rootDir/../node_modules, etc. ]
	  var modulePaths = Module.globalPaths
	    .concat(Module._nodeModulePaths(rootDir));
	
	  fullPath = modulePaths
	    .map(function(candidateDir) {
	      var absPath = path.join(candidateDir, relativePath);
	      try {
	        // NOTE(bajtos) We need to create a proper String object here,
	        // otherwise we can't attach additional properties to it
	        var filePath = new String(/*require.resolve*/(__webpack_require__(15).resolve(absPath)));
	        filePath.unresolvedPath = absPath;
	        return filePath;
	      } catch (err) {
	        return absPath;
	      }
	    })
	    .filter(function(candidate) {
	      return utils.fileExistsSync(candidate.toString());
	    })
	    [0];
	
	  if (fullPath) {
	    if (fullPath.unresolvedPath && resolveOptions.fullResolve === false)
	      return fullPath.unresolvedPath;
	    // Convert String object back to plain string primitive
	    return fullPath.toString();
	  }
	
	  debug ('Skipping %s - module not found', fullPath);
	  return undefined;
	}
	
	function loadModelDefinition(rootDir, jsonFile, allFiles, scriptExtensions) {
	  var definition = __webpack_require__(15)(jsonFile);
	  var basename = path.basename(jsonFile, path.extname(jsonFile));
	  definition.name = definition.name || _.upperFirst(_.camelCase(basename));
	
	  // find a matching file with a supported extension like `.js` or `.coffee`
	  var sourceFile = fixFileExtension(jsonFile, allFiles, scriptExtensions);
	
	  if (sourceFile === undefined) {
	    debug('Model source code not found: %s', sourceFile);
	  }
	
	  debug('Found model "%s" - %s %s', definition.name,
	    path.relative(rootDir, jsonFile),
	    sourceFile ? path.relative(rootDir, sourceFile) : '(no source file)');
	
	  return {
	    definition: definition,
	    sourceFile: sourceFile,
	  };
	}
	
	function buildMiddlewareInstructions(rootDir, config) {
	  var phasesNames = Object.keys(config);
	  var middlewareList = [];
	  phasesNames.forEach(function(phase) {
	    var phaseConfig = config[phase];
	    Object.keys(phaseConfig).forEach(function(middleware) {
	      var allConfigs = phaseConfig[middleware];
	      if (!Array.isArray(allConfigs))
	        allConfigs = [allConfigs];
	
	      allConfigs.forEach(function(config) {
	        var resolved = resolveMiddlewarePath(rootDir, middleware, config);
	
	        // resolved.sourceFile will be false-y if an optional middleware
	        // is not resolvable.
	        // if a non-optional middleware is not resolvable, it will throw
	        // at resolveAppPath() and not reach here
	        if (!resolved.sourceFile) {
	          return g.log('{{Middleware}} "%s" not found: %s',
	            middleware,
	            resolved.optional
	          );
	        }
	
	        var middlewareConfig = cloneDeep(config);
	        middlewareConfig.phase = phase;
	
	        if (middlewareConfig.params) {
	          middlewareConfig.params = resolveMiddlewareParams(
	            rootDir, middlewareConfig.params);
	        }
	
	        var item = {
	          sourceFile: resolved.sourceFile,
	          config: middlewareConfig,
	        };
	        if (resolved.fragment) {
	          item.fragment = resolved.fragment;
	        }
	        middlewareList.push(item);
	      });
	    });
	  });
	
	  var flattenedPhaseNames = phasesNames
	    .map(function getBaseName(name) {
	      return name.replace(/:[^:]+$/, '');
	    })
	    .filter(function differsFromPreviousItem(value, ix, source) {
	      // Skip duplicate entries. That happens when
	      // `name:before` and `name:after` are both translated to `name`
	      return ix === 0 || value !== source[ix - 1];
	    });
	
	  return {
	    phases: flattenedPhaseNames,
	    middleware: middlewareList,
	  };
	}
	
	function resolveMiddlewarePath(rootDir, middleware, config) {
	  var resolved = {
	    optional: !!config.optional,
	  };
	
	  var segments = middleware.split('#');
	  var pathName = segments[0];
	  var fragment = segments[1];
	  var middlewarePath = pathName;
	  var opts = {
	    strict: true,
	    optional: !!config.optional,
	  };
	
	  if (fragment) {
	    resolved.fragment = fragment;
	  }
	
	  if (pathName.indexOf('./') === 0 || pathName.indexOf('../') === 0) {
	    // Relative path
	    pathName = path.resolve(rootDir, pathName);
	  }
	
	  var resolveOpts = _.extend(opts, {
	    // Workaround for strong-agent to allow probes to detect that
	    // strong-express-middleware was loaded: exclude the path to the
	    // module main file from the source file path.
	    // For example, return
	    //   node_modules/strong-express-metrics
	    // instead of
	    //   node_modules/strong-express-metrics/index.js
	    fullResolve: false,
	  });
	  var sourceFile = resolveAppScriptPath(rootDir, middlewarePath, resolveOpts);
	
	  if (!fragment) {
	    resolved.sourceFile = sourceFile;
	    return resolved;
	  }
	
	  // Try to require the module and check if <module>.<fragment> is a valid
	  // function
	  var m = requireNodeOrEsModule(sourceFile);
	  if (typeof m[fragment] === 'function') {
	    resolved.sourceFile = sourceFile;
	    return resolved;
	  }
	
	  /*
	   * module/server/middleware/fragment
	   * module/middleware/fragment
	   */
	  var candidates = [
	    pathName + '/server/middleware/' + fragment,
	    pathName + '/middleware/' + fragment,
	    // TODO: [rfeng] Should we support the following flavors?
	    // pathName + '/lib/' + fragment,
	    // pathName + '/' + fragment
	  ];
	
	  var err = undefined; // see https://github.com/eslint/eslint/issues/5744
	  for (var ix in candidates) {
	    try {
	      resolved.sourceFile = resolveAppScriptPath(rootDir, candidates[ix], opts);
	      delete resolved.fragment;
	      return resolved;
	    } catch (e) {
	      // Report the error for the first candidate when no candidate matches
	      if (!err) err = e;
	    }
	  }
	  throw err;
	}
	
	// Match values starting with `$!./` or `$!../`
	var MIDDLEWARE_PATH_PARAM_REGEX = /^\$!(\.\/|\.\.\/)/;
	
	function resolveMiddlewareParams(rootDir, params) {
	  return _.cloneDeepWith(params, function resolvePathParam(value) {
	    if (typeof value === 'string' && MIDDLEWARE_PATH_PARAM_REGEX.test(value)) {
	      return path.resolve(rootDir, value.slice(2));
	    } else {
	      return undefined; // no change
	    }
	  });
	}
	
	function buildComponentInstructions(rootDir, componentConfig) {
	  return Object.keys(componentConfig)
	    .filter(function(name) { return !!componentConfig[name]; })
	    .map(function(name) {
	      return {
	        sourceFile: resolveAppScriptPath(rootDir, name, { strict: true }),
	        config: componentConfig[name],
	      };
	    });
	}
	
	function resolveRelativePaths(relativePaths, appRootDir) {
	  var resolveOpts = { strict: false };
	  relativePaths.forEach(function(relativePath, k) {
	    var resolvedPath = tryResolveAppPath(appRootDir, relativePath, resolveOpts);
	    if (resolvedPath !== undefined) {
	      relativePaths[k] = resolvedPath;
	    } else {
	      debug ('skipping boot script %s - unknown file', relativePath);
	    }
	  });
	}
	
	function getExcludedExtensions() {
	  return {
	    '.json': '.json',
	    /**
	     * This is a temporary workaround for #246
	     * See discussion here for full description of the underlying issue
	     * https://github.com/strongloop/loopback-boot/pull/245#issuecomment-311052798
	     */
	    '.map': '.map',
	    '.node': 'node',
	  };
	}
	
	function isPreferredExtension(filename, includeExtensions) {
	  assert(!!includeExtensions, '"includeExtensions" argument is required');
	
	  var ext = path.extname(filename);
	  return (ext in includeExtensions) && !(ext in getExcludedExtensions());
	}
	
	function fixFileExtension(filepath, files, scriptExtensions) {
	  var results = [];
	  var otherFile;
	
	  /* Prefer coffee scripts over json */
	  if (scriptExtensions && isPreferredExtension(filepath, scriptExtensions)) {
	    return filepath;
	  }
	
	  var basename = path.basename(filepath, FILE_EXTENSION_JSON);
	  var sourceDir = path.dirname(filepath);
	
	  files.forEach(function(f) {
	    otherFile = path.resolve(sourceDir, f);
	
	    var stats = fs.statSync(otherFile);
	    if (stats.isFile()) {
	      var otherFileExtension = path.extname(f);
	
	      if (!(otherFileExtension in getExcludedExtensions()) &&
	        path.basename(f, otherFileExtension) == basename) {
	        if (!scriptExtensions || otherFileExtension in scriptExtensions) {
	          results.push(otherFile);
	        }
	      }
	    }
	  });
	  return (results.length > 0 ? results[0] : undefined);
	}
	
	function resolveAppScriptPath(rootDir, relativePath, resolveOptions) {
	  var resolvedPath = resolveAppPath(rootDir, relativePath, resolveOptions);
	  if (!resolvedPath) {
	    return false;
	  }
	  var sourceDir = path.dirname(resolvedPath);
	  var files = tryReadDir(sourceDir);
	  var fixedFile = fixFileExtension(resolvedPath, files);
	  return (fixedFile === undefined ? resolvedPath : fixedFile);
	}
	
	function buildAllMixinInstructions(appRootDir, options, mixinSources,
	                                   scriptExtensions, modelInstructions) {
	  // load mixins from `options.mixins`
	  var sourceFiles = options.mixins || [];
	  var mixinDirs = options.mixinDirs || [];
	  var instructionsFromMixins = loadMixins(sourceFiles, options.normalization);
	
	  // load mixins from `options.mixinDirs`
	  sourceFiles = findMixinDefinitions(appRootDir, mixinDirs, scriptExtensions);
	  if (sourceFiles === undefined) return;
	  var instructionsFromMixinDirs = loadMixins(sourceFiles,
	                                             options.normalization);
	
	  /* If `mixinDirs` and `mixinSources` have any directories in common,
	   * then remove the common directories from `mixinSources` */
	  mixinSources = _.difference(mixinSources, mixinDirs);
	
	  // load mixins from `options.mixinSources`
	  sourceFiles = findMixinDefinitions(appRootDir, mixinSources,
	                                     scriptExtensions);
	  if (sourceFiles === undefined) return;
	  var instructionsFromMixinSources = loadMixins(sourceFiles,
	                                                options.normalization);
	
	  // Fetch unique list of mixin names, used in models
	  var modelMixins = fetchMixinNamesUsedInModelInstructions(modelInstructions);
	  modelMixins = _.uniq(modelMixins);
	
	  // Filter-in only mixins, that are used in models
	  instructionsFromMixinSources = filterMixinInstructionsUsingWhitelist(
	    instructionsFromMixinSources, modelMixins);
	
	  var mixins = _.assign(
	    instructionsFromMixins,
	    instructionsFromMixinDirs,
	    instructionsFromMixinSources);
	
	  return _.values(mixins);
	}
	
	function findMixinDefinitions(appRootDir, sourceDirs, scriptExtensions) {
	  var files = [];
	  sourceDirs.forEach(function(dir) {
	    var path = tryResolveAppPath(appRootDir, dir);
	    if (!path) {
	      debug('Skipping unknown module source dir %j', dir);
	      return;
	    }
	    files = files.concat(findScripts(path, scriptExtensions));
	  });
	  return files;
	}
	
	function loadMixins(sourceFiles, normalization) {
	  var mixinInstructions = {};
	  sourceFiles.forEach(function(filepath) {
	    var dir = path.dirname(filepath);
	    var ext = path.extname(filepath);
	    var name = path.basename(filepath, ext);
	    var metafile = path.join(dir, name + FILE_EXTENSION_JSON);
	
	    name = normalizeMixinName(name, normalization);
	    var meta = {};
	    meta.name = name;
	    if (utils.fileExistsSync(metafile)) {
	      // May overwrite name, not sourceFile
	      _.extend(meta, requireNodeOrEsModule(metafile));
	    }
	    meta.sourceFile = filepath;
	    mixinInstructions[meta.name] = meta;
	  });
	
	  return mixinInstructions;
	}
	
	function fetchMixinNamesUsedInModelInstructions(modelInstructions) {
	  return _.flatten(modelInstructions
	  .map(function(model) {
	    return model.definition && model.definition.mixins ?
	      Object.keys(model.definition.mixins) : [];
	  }));
	}
	
	function filterMixinInstructionsUsingWhitelist(instructions, includeMixins) {
	  var instructionKeys = Object.keys(instructions);
	  includeMixins = _.intersection(instructionKeys, includeMixins);
	
	  var filteredInstructions = {};
	  instructionKeys.forEach(function(mixinName) {
	    if (includeMixins.indexOf(mixinName) !== -1) {
	      filteredInstructions[mixinName] = instructions[mixinName];
	    }
	  });
	  return filteredInstructions;
	}
	
	function normalizeMixinName(str, normalization) {
	  switch (normalization) {
	    case false:
	    case 'none': return str;
	
	    case undefined:
	    case 'classify':
	      str = String(str).replace(/([A-Z]+)/g, ' $1').trim();
	      str = String(str).replace(/[\W_]/g, ' ').toLowerCase();
	      str = str.replace(/(?:^|\s|-)\S/g, function(c) {
	        return c.toUpperCase();
	      });
	      str = str.replace(/\s+/g, '');
	      return str;
	
	    case 'dasherize':
	      str = String(str).replace(/([A-Z]+)/g, ' $1').trim();
	      str = String(str).replace(/[\W_]/g, ' ').toLowerCase();
	      str = str.replace(/\s+/g, '-');
	      return str;
	
	    default:
	      if (typeof normalization === 'function') {
	        return normalization(str);
	      }
	
	      var err = new Error(g.f('Invalid normalization format - "%s"',
	        normalization));
	      err.code = 'INVALID_NORMALIZATION_FORMAT';
	      throw err;
	  }
	}


/***/ }),
/* 58 */
/***/ (function(module, exports) {

	module.exports = require("toposort");

/***/ }),
/* 59 */
/***/ (function(module, exports) {

	module.exports = require("module");

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

	// Copyright IBM Corp. 2014,2016. All Rights Reserved.
	// Node module: loopback-boot
	// This file is licensed under the MIT License.
	// License text available at https://opensource.org/licenses/MIT
	
	var fs = __webpack_require__(56);
	var path = __webpack_require__(11);
	var commondir = __webpack_require__(61);
	var cloneDeep = __webpack_require__(20).cloneDeep;
	var g = __webpack_require__(13)();
	
	/**
	 * Add boot instructions to a browserify bundler.
	 * @param {Object} instructions Boot instructions.
	 * @param {Object} bundler A browserify object created by `browserify()`.
	 */
	
	module.exports = function addInstructionsToBrowserify(instructions, bundler) {
	  bundleModelScripts(instructions, bundler);
	  bundleMixinScripts(instructions, bundler);
	  bundleComponentScripts(instructions, bundler);
	  bundleOtherScripts(instructions, bundler);
	  bundleInstructions(instructions, bundler);
	};
	
	function bundleOtherScripts(instructions, bundler) {
	  for (var key in instructions.files) {
	    addScriptsToBundle(key, instructions.files[key], bundler);
	  }
	}
	
	function bundleModelScripts(instructions, bundler) {
	  bundleSourceFiles(instructions, 'models', bundler);
	}
	
	function bundleMixinScripts(instructions, bundler) {
	  bundleSourceFiles(instructions, 'mixins', bundler);
	}
	
	function bundleComponentScripts(instructions, bundler) {
	  bundleSourceFiles(instructions, 'components', bundler);
	}
	
	function bundleSourceFiles(instructions, type, bundler) {
	  var files = instructions[type]
	    .map(function(m) { return m.sourceFile; })
	    .filter(function(f) { return !!f; });
	
	  var instructionToFileMapping = instructions[type]
	    .map(function(m) { return files.indexOf(m.sourceFile); });
	
	  addScriptsToBundle(type, files, bundler);
	
	  // Update `sourceFile` properties with the new paths
	  instructionToFileMapping.forEach(function(fileIx, sourceIx) {
	    if (fileIx === -1) return;
	    instructions[type][sourceIx].sourceFile = files[fileIx];
	  });
	}
	
	function addScriptsToBundle(name, list, bundler) {
	  if (!list.length) return;
	
	  var root = commondir(list.map(path.dirname));
	
	  for (var ix in list) {
	    var filepath = list[ix];
	
	    // Build a short unique id that does not expose too much
	    // information about the file system, but still preserves
	    // useful information about where is the file coming from.
	    var fileid = 'loopback-boot#' + name + '#' + path.relative(root, filepath);
	
	    // Add the file to the bundle.
	    bundler.require(filepath, { expose: fileid });
	
	    // Rewrite the instructions entry with the new id that will be
	    // used to load the file via `require(fileid)`.
	    list[ix] = fileid;
	  }
	}
	
	function bundleInstructions(instructions, bundler) {
	  instructions = cloneDeep(instructions);
	
	  var hasMiddleware = instructions.middleware.phases.length ||
	    instructions.middleware.middleware.length;
	  if (hasMiddleware) {
	    g.warn(
	      'Discarding {{middleware}} instructions,' +
	      ' {{loopback}} client does not support {{middleware}}.');
	  }
	  delete instructions.middleware;
	
	  var instructionsString = JSON.stringify(instructions, null, 2);
	
	  /* The following code does not work due to a bug in browserify
	   * https://github.com/substack/node-browserify/issues/771
	   var instructionsStream = require('resumer')()
	     .queue(instructionsString);
	   instructionsStream.path = 'boot-instructions';
	   b.require(instructionsStream, { expose: 'loopback-boot#instructions' });
	   */
	
	  var instructionId = 'instructions';
	  // Create an unique instruction identifier using the application ID.
	  // This is only useful when multiple loopback applications are being bundled
	  // together.
	  if (instructions.appId)
	    instructionId += '-' + instructions.appId;
	
	  // Write the instructions to a file in our node_modules folder.
	  // The location should not really matter as long as it is .gitignore-ed
	  var instructionsFile = path.resolve(__dirname,
	    '..', 'generated-' + instructionId + '.json');
	  fs.writeFileSync(instructionsFile, instructionsString, 'utf-8');
	
	  var moduleName = 'loopback-boot#' + instructionId;
	  bundler.require(instructionsFile, { expose: moduleName });
	}


/***/ }),
/* 61 */
/***/ (function(module, exports) {

	module.exports = require("commondir");

/***/ }),
/* 62 */
/***/ (function(module, exports) {

	module.exports = require("kafka-node");

/***/ }),
/* 63 */
/***/ (function(module, exports) {

	module.exports = require("winston");

/***/ }),
/* 64 */
/***/ (function(module, exports) {

	module.exports = require("socket.io");

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var host    = 'http://alfademo.ddns.net:3001';
	var loopback = __webpack_require__(2);
	var ds = loopback.createDataSource({
	  connector: __webpack_require__(47),
	  debug: false,
	  operations: [
	    {
	      functions: {find: ['latitude', 'longitude']},
	      template: {
	        method: 'GET',
	        url: `${host}/perfcounters/{latitude}/{longitude}/`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	    {
	      functions: {create: ['data']},
	      template: {
	        method: 'POST',
	        url: `${host}/perfcounters`,
	        body: '{data:object}',
	      },
	    },
	    {
	      functions: {remove: ['id']},
	      template: {
	        method: 'DELETE',
	        url: `${host}/perfcounters/{id}`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	    {
	      functions: {measurement: ['id', 'start', 'end']},
	      template: {
	        method: 'GET',
	        url: `${host}/perfmeasurements/{id}/{start}/{end}/`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	  ],
	});
	
	module.exports  = (server) => {
	  let router = server.loopback.Router();
	  router.get('/api/perfcounters/:latitude/:longitude/', (req, res)=>{
	    ds.find(req.params.latitude, req.params.longitude)
	     .then(result=>res.send(result))
	     .catch(err=>res.status(500).send(err));
	  });
	
	  router.post('/api/perfcounters', (req, res)=>{
	    ds.create(req.body).then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  router.delete('/api/perfcounters/:id', (req, res)=>{
	    ds.remove(req.params.id).then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));;
	  });
	
	  function format(now, format) {
	    let months = {Jan: '01', Feb: '02', Mar: '03', Apr: '04', May: '05', Jun: '06',
	      Jul: '07', Aug: '08', Sep: '09', Oct: '10', Nov: '11', Dec: '12'};
	    now = new Date(now).toString();
	    let config = {
	      YYYY: now.substr(11, 4),
	      MM: months[now.substr(4, 3)],
	      DD: now.substr(8, 2),
	      hh: now.substr(16, 2),
	      mm: now.substr(19, 2),
	      ss: now.substr(22, 2),
	    };
	      // format
	    let myDate = '';
	    myDate = format;
	    for (var i in config) {
	      myDate = myDate.split(i).join(config[i]);
	    }
	    return myDate;
	  }
	
	  function getDate(timeType) {
	    var start = new Date();
	    var end = new Date();
	    var value = {start: 'T00:00:00Z', end: 'T23:59:59Z'};
	    if (timeType !== 'MIN5') {
	      if (timeType === 'HOUR1') {
	        start.setDate(start.getDate() - 6);
	      } else {
	        start.setMonth(start.getMonth() - 2);
	        start.setDate(1);
	      }
	    }
	    start = format(start, 'YYYY-MM-DD');
	    end   = format(end, 'YYYY-MM-DD');
	    value.start = start + value.start;
	    value.end   = end   + value.end;
	    return value;
	  }
	
	  router.get('/api/perfmeasurements/:latitude/:longitude', (req, res)=>{
	    const latitude  = req.params.latitude;
	    const longitude = req.params.longitude;
	    ds.find(latitude, longitude).then(counters=>{
	      counters = JSON.parse(JSON.stringify(counters));
	      if (counters.length === 0) {
	        res.send([]);
	      } else {
	        var services = [];
	        counters.forEach(element => {
	          const date = getDate(element.perfObj.period);
	          services.push(ds.measurement(element.id, date.start, date.end));
	        });
	
	        Promise.all(services).then(measurements=>{
	          let values = [];
	          measurements = JSON.parse(JSON.stringify(measurements));
	          measurements.forEach(data=>{
	            values = values.concat(data);
	          });
	          res.send(values);
	        }).catch(err=>res.status(500).send(err));
	        // end if
	      }
	      // end ds find
	    }).catch(err=>res.status(500).send(err));
	  });
	
	  server.use(router);
	};


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	var host    = 'http://alfademo.ddns.net:3001';
	var loopback = __webpack_require__(2);
	var ds = loopback.createDataSource({
	  connector: __webpack_require__(47),
	  debug: false,
	  operations: [
	    {
	      functions: {find: ['id', 'start', 'end']},
	      template: {
	        method: 'GET',
	        url: `${host}/perfmeasurements/{id}/{start}/{end}/`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    }
	  ]
	});
	
	module.exports = (server) => {
	  let router = server.loopback.Router();
	
	  router.get('/api/perfmeasurements/:id/:start/:end/', (req, res) =>{
	      const start = req.params.start + 'Z';
	      const end = req.params.end + 'Z';
	      ds.find(req.params.id, start, end)
	      .then(result => res.send(result))
	      .catch(err=>res.status(500).send(err));
	  });
	
	  server.use(router);
	};


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var host    = 'http://alfademo.ddns.net:3001';
	var loopback = __webpack_require__(2);
	var ds = loopback.createDataSource({
	  connector: __webpack_require__(47),
	  debug: false,
	  operations: [
	    {
	      functions: {find: []},
	      template: {
	        method: 'GET',
	        url: `${host}/perfobjects`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	    {
	      functions: {create: ['data']},
	      template: {
	        method: 'POST',
	        url: `${host}/perfobjects`,
	        body: '{data:object}',
	      },
	    },
	    {
	      functions: {delete: ['id']},
	      template: {
	        method: 'DELETE',
	        url: `${host}/perfobjects/{id}`,
	        headers: {
	          'accepts': 'application/json',
	          'content-type': 'application/json',
	        },
	      },
	    },
	  ],
	});
	
	module.exports = (server) => {
	  let router = server.loopback.Router();
	
	  router.get('/api/perfobjects', (req, res)=>{
	    ds.find().then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  router.post('/api/perfobjects', (req, res)=>{
	    ds.create(req.body).then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  router.delete('/api/perfobjects/:id', (req, res)=>{
	    ds.delete(req.params.id).then(result=>res.send(result))
	    .catch(err=>res.status(500).send(err));
	  });
	
	  server.use(router);
	};


/***/ })
/******/ ])));
//# sourceMappingURL=main.bundle.js.map