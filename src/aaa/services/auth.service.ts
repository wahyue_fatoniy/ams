import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import * as auth0 from 'auth0-js';
import { HOST, PORT_AAA } from '../../app/global.variable';

@Injectable()
export class AuthService {

  auth0 = new auth0.WebAuth({
    clientID: 'TPbF5PKI20F1yTLMle0eqy4ck2UzFGej',
    domain: 'ngx-ems.auth0.com',
    responseType: 'token id_token',
    audience: `https://ngx-ems.auth0.com/userinfo`,
    redirectUri: `${HOST}${PORT_AAA}/#/callback`,
    scope: 'openid profile'
  });

  constructor(public router: Router) {}

  public loginView(): void {
    this.auth0.authorize();
  }

  public checkAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        if (localStorage.getItem('role') === 'work-order-protelindo') {
          this.router.navigate(['/padlock']);
        } else {
          this.router.navigate(['/public']);
        }

      } else if (err) {
        this.router.navigate(['/login']);
        console.log('error', err);
        alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  getRedirect(): string {
    const value = localStorage.getItem('role') === 'work-order-protelindo' ? '/padlock/dashboard-padlock' : '/public';
    return value;
  }

  getProfile() {
    return JSON.parse(localStorage.getItem('profile'));
  }

  private setSession(authResult): void {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    // If there is a value on the `scope` param from the authResult,
    // use it to set scopes in the session for the user. Otherwise
    // use the scopes as requested. If no scopes were requested,
    // set it to nothing
    const scopes = authResult.scope || '';
    localStorage.setItem('profile', JSON.stringify(authResult.idTokenPayload));
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    localStorage.setItem('scopes', JSON.stringify(scopes));
    localStorage.setItem('role', authResult.idTokenPayload['https://example.com/roles']);
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('scopes');
    localStorage.removeItem('role');
    localStorage.removeItem('profile');
    // Go back to the home route
    this.router.navigate(['/login']);
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  public userHasScopes(scopes: Array<string>): boolean {
    let authScope = JSON.parse(localStorage.getItem('scopes'));
    let valid = false;
    if (authScope !== null) {
      authScope = authScope.split(' ');
      for (const i of Object.keys(scopes)) {
         const index = authScope.findIndex(res => scopes[i] === res);
         if (index !== -1) {
           valid = true;
         }
      }
    }
    return valid;
  }

}

