import { TestBed, async, inject } from '@angular/core/testing';

import { AuthScopeGuard } from './auth-scope.guard';

describe('AuthScopeGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthScopeGuard]
    });
  });

  it('should ...', inject([AuthScopeGuard], (guard: AuthScopeGuard) => {
    expect(guard).toBeTruthy();
  }));
});
