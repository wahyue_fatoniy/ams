import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthScopeGuard implements CanActivate {

  constructor(public auth: AuthService, public router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (!this.auth.isAuthenticated() || !this.auth.userHasScopes(route.data.scopes)) {
      this.router.navigate(['/login']);
      console.log('Auth Guard Scope protective');
      return true;
    }
    return true;
  }

}
