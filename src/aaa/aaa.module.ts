import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

// modules from authentication authorization acounting
import { AuthGuard } from './guards/auth.guard';
import { AuthScopeGuard } from './guards/auth-scope.guard';
import { AuthService } from './services/auth.service';


@NgModule({
  imports: [
    CommonModule
  ],
  exports : [],
  providers : [AuthService]
})

export class AaaModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AaaModule,
      providers: [ AuthGuard, AuthScopeGuard, AuthService ]
    };
  }

}
