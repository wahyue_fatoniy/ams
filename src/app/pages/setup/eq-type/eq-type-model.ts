
export interface EqType {
  id?: number;
  type: string;
  brand: string;
  series: string;
}

export class EqTypeModel {
}
