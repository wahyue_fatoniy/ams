import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EqTypeComponent } from './eq-type.component';

describe('EqTypeComponent', () => {
  let component: EqTypeComponent;
  let fixture: ComponentFixture<EqTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EqTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EqTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
