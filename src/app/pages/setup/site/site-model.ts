import { Location } from '../../../app.component';
import { HttpErrorResponse } from '@angular/common/http';


export interface Site {
  id?: number;
  label: string;
  zoom: number;
  address: string;
  discobj_id: number;
  district_id: number;
  location: Location;
}


const DEFAULT: Site = {
  id: null,
  label: null,
  zoom: null,
  address: null,
  discobj_id: null,
  district_id: null,
  location: {lat: null, lng: null}
};

export class SiteModel {

  constructor() {}
  list: Site[];
  loading: boolean;
  statusError: HttpErrorResponse;

  setAll(list: Site[]) {
    console.info('[site] ambil data site');
    console.debug(list);
    this.loading = false;
    this.list = list;
  }

  getById(id: number): Site {
    const index = this.list.findIndex(w => w.id === id);
    return index === -1 ? DEFAULT : this.list[index];
  }

  getError(): {name: string, message: string} {
    let name: string, message: string;
    name = this.statusError.name;
    message = this.statusError.message;
    if (this.statusError.error) {
        if (this.statusError.error.error) {
          name = this.statusError.error.error.name ? this.statusError.error.error.name : name;
          message = this.statusError.error.error.message ? this.statusError.error.error.message : message;
        }
    }
    return {name: name, message: message};
  }

  setError(error: HttpErrorResponse) {
    this.statusError = error;
    this.loading = false;
    console.error('[site] terdeteksi error', this.getError());
  }

  add(value: Site) {
    console.info('[site] tambah data site');
    console.debug(value);
    this.loading = false;
    this.list = [value, ...this.list];
  }

  edit(id: number, value: Site) {
    console.info('[site] ubah data site, ID = ' + id);
    console.debug(value);
    this.loading = false;
    this.list = this.list.map(site => site.id === id ? value : site);
  }

  remove(id: number) {
    console.info('[site] hapus data site, ID = ' + id);
    this.loading = false;
    this.list = this.list.filter(site => site.id !== id);
  }

}

