import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { pipe } from 'rxjs/util/pipe';
import 'rxjs/add/observable/throw';
import { Site } from './site-model';
import { catchError } from 'rxjs/operators';
const URL  = '/api/sites';

@Injectable()
export class SiteService {

  constructor(private http: HttpClient) { }

  getAllSites(): Observable<Site[]> {
    const filter  = {order: 'label'};
    const encode = encodeURIComponent(JSON.stringify(filter));
    return this.http.get<Site[]>(`${URL}?filter=${encode}`)
    .pipe(
      catchError(error => Observable.throw(error))
    );
  }

}
