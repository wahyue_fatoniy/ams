import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
  City,
  UrlEncodePipe,
  Table,
  ErrorHandlerComponent,
  Province,
  FormMapDialogComponent,
  AuthService
} from '../../../../shared';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

  constructor(public http: HttpClient,public auth:AuthService) { }
  @ViewChild('error') error: ErrorHandlerComponent;
  @ViewChild('form') form: FormMapDialogComponent;
  encode: UrlEncodePipe = new UrlEncodePipe();
  provinces: Province[];
  table: Table = {
     title : 'Table City',
     columns : ['id', 'Label', 'Latitude', 'Longitude', 'Zoom', 'Province'],
     buttons : this.auth.userHasScopes(['write:city']) ? ['create', 'edit', 'remove'] : [],
     datasets : [],
     loading : false,
     width : '1300px'
  };

  ngOnInit() {
   this.getData();
  }

  getData() {
    this.table.loading = true;
    this.http.get<Province[]>(`/api/provinces?filter=${this.encode.transform({order: 'label'})}`)
    .subscribe(provinces => this.provinces = provinces);
    this.http.get<City[]>(`/api/cities?filter=${this.encode.transform({
      order: 'label', include : ['province']
    })}`).subscribe(data => {
        this.table.loading = false;
        this.table.datasets = data.map(city => {
            return {
              id : city.id,
              Label : city.label,
              Latitude : city.location.lat,
              Longitude : city.location.lng,
              Zoom : city.zoom,
              Province : city.province.label
            };
        });
    }, error => {
        this.table.loading = false;
        this.error.open(error);
    });
  }

  action(event) {
    this.form.openDialog('Form City', event.actionType, event.value, {Province: this.provinces});
  }

  save(event) {
    let value: City;
    if (event.actionType !== 'remove') {
      value = {
        label : event.value.Label,
        location : {
          lat : event.value.Latitude,
          lng : event.value.Longitude
        },
        zoom : event.value.Zoom,
        province_id : event.value.list.Province
      };
    }
    switch (event.actionType) {
      case 'create':
        this.http.post<City>('/api/cities', value)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;
      case 'edit':
        this.http.put<City>(`/api/cities/${event.value.id}`, value)
        .subscribe(res => this.getData(), err => {
          this.getData();
          this.error.open(err);
        });
      break;
      case 'remove':
        this.http.delete(`/api/cities/${event.value.id}`)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;
    }
  }


}
