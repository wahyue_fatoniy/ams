import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
  District,
  UrlEncodePipe,
  Table,
  ErrorHandlerComponent,
  FormMapDialogComponent,
  Province,
  City,
  AuthService
} from '../../../../shared';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'app-district',
  templateUrl: './district.component.html',
  styleUrls: ['./district.component.css']
})
export class DistrictComponent implements OnInit {

  constructor(public http: HttpClient,public auth:AuthService) { }
  @ViewChild('error') error: ErrorHandlerComponent;
  @ViewChild('form') form: FormMapDialogComponent;
  encode: UrlEncodePipe = new UrlEncodePipe();
  table: Table = {
     title : 'Table District',
     columns : ['id', 'Label', 'Latitude', 'Longitude', 'Zoom', 'City'],
     buttons : this.auth.userHasScopes(['write:district']) ? ['create', 'edit', 'remove'] : [],
     datasets : [],
     loading : false,
     width : '1300px'
  };
  provinces: Province[];
  cities: City[];
  ngOnInit() {
   this.getData();
  }

  getData() {
      this.table.loading = true;
      Observable.forkJoin([
        this.http.get<Province[]>(`/api/provinces?filter=${this.encode.transform({order: 'label'})}`),
        this.http.get<City[]>(`/api/cities?filter=${this.encode.transform({order: 'label'})}`),
        this.http.get<District[]>(`/api/districts?filter=${this.encode.transform({
          order: 'label', include : ['city']
        })}`)
      ]).subscribe(data => {
        this.table.loading = false;
        this.provinces = data[0];
        this.cities = data[1].map(res => Object.assign({}, res, {id_parent: res.province_id}));
        this.table.datasets = data[2].map(city => {
            return {
              id : city.id,
              Label : city.label,
              Latitude : city.location.lat,
              Longitude : city.location.lng,
              Zoom : city.zoom,
              City : city.city.label
            };
        });
      }, err => {
          this.table.loading = false;
          this.error.open(err);
      });
  }

  action(event) {
    this.form.openDialog('Form District', event.actionType, event.value, {Province: this.provinces, City: this.cities});
  }

  save(event) {
    let value: District;
    if (event.actionType !== 'remove') {
      value = {
          label : event.value.Label,
          location : {
             lat : event.value.Latitude,
             lng : event.value.Longitude
          },
          zoom : event.value.Zoom,
          city_id : event.value.list.City
      };
    }
    switch (event.actionType) {
      case 'create':
        this.http.post<District>('/api/districts', value)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;
      case 'edit':
        this.http.put<District>(`/api/districts/${event.value.id}`, value)
        .subscribe(res => this.getData(), err => {
          this.getData();
          this.error.open(err);
        });
      break;
      case 'remove':
        this.http.delete(`/api/districts/${event.value.id}`)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;
    }
  }

}
