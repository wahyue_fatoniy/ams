import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormV2Component,
  FormV2,
  ErrorHandlerComponent,
  Table,
  DialogRemoveComponent,
  EquipmentType,
  SortDataPipe,
  AuthService
 } from '../../../../shared';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-equipment-type',
  templateUrl: './equipment-type.component.html',
  styleUrls: ['./equipment-type.component.css']
})
export class EquipmentTypeComponent implements OnInit {

  constructor(public http: HttpClient,public auth:AuthService) {}
  @ViewChild('form') form: FormV2Component;
  @ViewChild('remove') remove: DialogRemoveComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  sort: SortDataPipe = new SortDataPipe();
  table: Table = {
    title : 'Table Equipment Type',
    columns : ['id', 'Type', 'Brand', 'Series'],
    buttons : this.auth.userHasScopes(['write:equipmentType']) ? ['create', 'remove'] : [],
    loading : false,
    datasets : [],
    width  : '1300px'
  };
  formConfig: FormV2 = {
    title : 'Form Equipment Type',
    actionType : '',
    value : {},
    models : [
      {type: 'text', model : 'Type'}, {type: 'text', model: 'Brand'}, {type: 'text', model: 'Series'}
    ]
  };

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.table.loading = true;
    this.http.get<EquipmentType[]>('/api/equipmenttype').subscribe(result => {
        this.table.loading = false;
        result = this.sort.transform(result, 'type', 'asc');
        this.table.datasets = result.map(res =>
          Object.assign({}, {
            id : res.id,
            Type : res.type,
            Brand : res.brand,
            Series : res.series
        }));
    }, err => {
      this.table.loading = false;
      this.error.open(err);
    });
  }

  action(event) {
    if (event.actionType !== 'remove') {
        this.formConfig.actionType = event.actionType;
        this.form.open(this.formConfig);
    } else {
        this.remove.open({id: event.value.id, message: `Are you sure to remove ${event.value.Type}`});
    }
  }

  save(event) {
      const value: EquipmentType  = {
        type  : event.value.Type,
        brand : event.value.Brand,
        series : event.value.Series
      };
      this.http.post<EquipmentType>('/api/equipmenttype', value)
      .subscribe(res => this.getData());
  }

  saveRemove(event) {
    this.http.delete(`/api/equipmenttype/${event}`)
    .subscribe(res => this.getData());
  }

}
