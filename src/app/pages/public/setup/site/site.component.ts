import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
  Site,
  UrlEncodePipe,
  Table,
  ErrorHandlerComponent,
  Province,
  City,
  District,
  FormMapDialogComponent,
  EquipmentType,
  Discobject,
  AuthService
} from '../../../../shared';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.css']
})
export class SiteComponent implements OnInit {


  constructor(public http: HttpClient,public auth:AuthService) { }
  @ViewChild('error') error: ErrorHandlerComponent;
  @ViewChild('form') form: FormMapDialogComponent;
  encode: UrlEncodePipe = new UrlEncodePipe();
  table: Table = {
     title : 'Table Site',
     columns : ['id', 'Label', 'Address', 'Latitude', 'Longitude', 'Zoom', 'District'],
     buttons : this.auth.userHasScopes(['write:site']) ? ['create', 'remove'] : [],
     datasets : [],
     loading : false,
     width : '1300px'
  };
  provinces: Province[];
  cities: City[];
  districts: District[];
  equipmentTypes: EquipmentType[];
  ngOnInit() {
   this.getData();
  }

  getData() {
    this.table.loading = true;
    Observable.forkJoin([
      this.http.get<Province[]>(`/api/provinces?filter=${this.encode.transform({order: 'label'})}`),
      this.http.get<City[]>(`/api/cities?filter=${this.encode.transform({order: 'label'})}`),
      this.http.get<District[]>(`/api/districts?filter=${this.encode.transform({order: 'label'})}`),
      this.http.get<Site[]>(`/api/sites?filter=${this.encode.transform({
        order: 'label', include : ['district']
      })}`),
      this.http.get<EquipmentType[]>(`/api/equipmenttype`)
    ]).subscribe(data => {
        this.table.loading = false;
        this.provinces = data[0];
        this.cities = data[1].map(city => Object.assign({}, city, {id_parent: city.province_id}));
        this.districts = data[2].map(district => Object.assign({}, district, {id_parent: district.city_id}));
        this.table.datasets = data[3].map(site => {
            return {
              id : site.id,
              Label : site.label,
              Address : site.address,
              Latitude : site.location.lat,
              Longitude : site.location.lng,
              Zoom : site.zoom,
              District : site.district.label,
              discobj_id  : site.discobj_id
            };
        });
        this.equipmentTypes = data[4];
    }, error => {
        this.table.loading = false;
        this.error.open(error);
    });
  }

  action(event) {
    this.form.openDialog('Form Site', event.actionType, event.value, {
      Province: this.provinces, City: this.cities, District: this.districts
    });
  }

  save(event) {
    switch (event.actionType) {
      case 'create':
         const indexEqType = this.equipmentTypes.findIndex(res => res.type === 'Site');
         const discobject: Discobject = {
            name : event.value.Label,
            location : {
               latitude : event.value.Latitude,
               longitude : event.value.Longitude
            },
            eqType : { id : this.equipmentTypes[indexEqType].id }
         };
         this.http.post<Discobject>('/api/discobjects', discobject).subscribe(disc => {
             const site: Site = {
                label : event.value.Label,
                address : event.value.Address,
                zoom : event.value.Zoom,
                district_id : event.value.list.District,
                discobj_id : disc.id,
                location : { lat : event.value.Latitude, lng : event.value.Longitude }
             };
             this.http.post<Site>('/api/sites', site).subscribe(res => this.getData(), err => this.error.open(err));
         }, err => this.error.open(err));
      break;

      case 'remove':
          const index = this.table.datasets.findIndex(res => res.id === event.value.id);
          const value = this.table.datasets[index];
          this.http.delete(`/api/sites/${value.id}`).subscribe(site => {
              this.http.delete(`/api/discobjects/${value.discobj_id}`)
              .subscribe(disc => this.getData(), err => this.error.open(err));
          }, err => this.error.open(err));
      break;
    }
  }

}
