import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Province,
  UrlEncodePipe,
  Table,
  ErrorHandlerComponent,
  FormMapDialogComponent,
  AuthService
} from '../../../../shared';


@Component({
  selector: 'app-province',
  templateUrl: './province.component.html',
  styleUrls: ['./province.component.css']
})
export class ProvinceComponent implements OnInit {

  constructor(public http: HttpClient,public auth:AuthService) {}
  @ViewChild('error') error: ErrorHandlerComponent;
  @ViewChild('form') form: FormMapDialogComponent;
  encode: UrlEncodePipe = new UrlEncodePipe();
  table: Table = {
     title : 'Table Province',
     columns : ['id', 'Label', 'Latitude', 'Longitude', 'Zoom'],
     buttons : this.auth.userHasScopes(['write:province']) ? ['create', 'edit', 'remove'] : [],
     datasets : [],
     loading : false,
     width : '1300px'
  };

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.table.loading = true;
    this.http.get<Province[]>(`/api/provinces?filter=${this.encode.transform({order: 'label'})}`)
    .subscribe(data => {
          this.table.loading = false;
          this.table.datasets = data.map(province => {
            return {
              id : province.id,
              Label : province.label,
              Latitude : province.location.lat,
              Longitude : province.location.lng,
              Zoom : province.zoom
            };
          });
      }, error  => {
          this.table.loading = false;
          this.error.open(error);
      });
  }

  action(event) {
    this.form.openDialog('Form Province', event.actionType, event.value);
  }

  save(event) {
    let value: Province;
    if (event.actionType !== 'remove') {
      value = {
         label : event.value.Label,
         location : {
            lat : event.value.Latitude,
            lng : event.value.Longitude
         },
         zoom : event.value.Zoom
      };
    }
    switch (event.actionType) {
      case 'create':
        this.http.post<Province>('/api/provinces', value)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;
      case 'edit':
        this.http.put<Province>(`/api/provinces/${event.value.id}`, value)
        .subscribe(res => this.getData(), err => {
          this.getData();
          this.error.open(err);
        });
      break;
      case 'remove':
        this.http.delete(`/api/provinces/${event.value.id}`)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;
    }
  }

}
