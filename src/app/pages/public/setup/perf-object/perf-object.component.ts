import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormV2Component,
  FormV2,
  ErrorHandlerComponent,
  Table,
  DialogRemoveComponent,
  PerfObject,
  SortDataPipe,
  AuthService
} from '../../../../shared';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-perf-object',
  templateUrl: './perf-object.component.html',
  styleUrls: ['./perf-object.component.css']
})
export class PerfObjectComponent implements OnInit {

  constructor(public http: HttpClient, public auth:AuthService) {}
  @ViewChild('form') form: FormV2Component;
  @ViewChild('remove') remove: DialogRemoveComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  sort: SortDataPipe = new SortDataPipe();
  table: Table = {
    title : 'Table Performance Object',
    columns : ['id', 'Name', 'Unit', 'Period'],
    buttons : this.auth.userHasScopes(['write:perfObject']) ? ['create', 'remove'] : [],
    loading : false,
    datasets : [],
    width  : '1300px'
  };
  formConfig: FormV2 = {
    title : 'Form Performance Object',
    actionType : '',
    value : {},
    models : [
      {type: 'text', model : 'Name'}, {type: 'text', model: 'Unit'},
      {type: 'list', model: 'Period', listLabel: 'period', idSet: 'period',
        list: [{period: 'MIN5'}, {period: 'HOUR1'}, {period: 'HOUR6'}]
      }
    ]
  };

  ngOnInit() {
    this.getData();
  }


  getData() {
    this.table.loading = true;
    this.http.get<PerfObject[]>(`/api/perfobjects`).subscribe(res => {
        this.table.loading = false;
        res = this.sort.transform(res, 'name', 'asc');
        this.table.datasets = res.map(item => Object.assign({}, {
            id : item.id, Name : item.name, Unit : item.unit, Period : item.period
        }));
    }, err => {
      this.table.loading = false;
      this.error.open(err);
    });
  }


  action(event) {
    switch (event.actionType) {
      case 'create':
        this.formConfig.actionType = 'create';
        this.form.open(this.formConfig);
      break;
      case 'remove':
        this.remove.open({
          id : event.value.id,
          message : `Are you sure to remove ${event.value.Name}`
        });
      break;
    }
  }


  save(event) {
    const value: PerfObject = {
        name : event.value.Name,
        unit : event.value.Unit,
        period : event.value.Period
    };
    this.http.post<PerfObject>(`/api/perfobjects`, value)
    .subscribe(res => this.getData(), err => this.error.open(err));
  }

  saveRemove(event) {
    this.http.delete(`/api/perfobjects/${event}`)
    .subscribe(res => this.getData(), err => this.error.open(err));
  }

}
