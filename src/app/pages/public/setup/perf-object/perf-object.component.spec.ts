import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfObjectComponent } from './perf-object.component';

describe('PerfObjectComponent', () => {
  let component: PerfObjectComponent;
  let fixture: ComponentFixture<PerfObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
