import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormV2Component,
  FormV2,
  ErrorHandlerComponent,
  TableExpansion,
  Security,
  DialogRemoveComponent,
  UrlEncodePipe,
  Channel,
  ChannelType,
  AuthService
 } from '../../../../shared';
 import { Observable } from 'rxjs/Observable';
 import 'rxjs/add/observable/forkJoin';
 import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {

  constructor(public http: HttpClient,public auth:AuthService) {}
  @ViewChild('form') form: FormV2Component;
  @ViewChild('remove') remove: DialogRemoveComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  encode: UrlEncodePipe = new UrlEncodePipe();
  channelTypes: ChannelType[] = [];
  table: TableExpansion = {
    propertyParent  : ['Channel Type'],
    propertyChild   : ['Channel Name'],
    datasets        : [],
    buttons         : this.auth.userHasScopes(['write:channel']) ? 
    ['create', 'removeParent', 'editParent', 'editChild', 'removeChild'] : [],
    loading         : false
  };

  formChannelType: FormV2 = {
    title : 'Form Channel Type',
    actionType : '',
    value : {},
    models : [{type: 'text', model: 'Label'}]
  };
  formChannel: FormV2 = {
    title : 'Form Channel',
    actionType : '',
    value : {},
    models : [
      {type: 'list', model: 'Channel Type', listLabel: 'label', idSet: 'id', list: [], disabled: true},
      {type: 'text', model: 'Channel Name'}
    ]
  };
  actionType: string;

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.table.loading = true;
    this.http.get<ChannelType[]>(`/api/channel_types?filter=${this.encode.transform({
      order: 'label', include : ['channels']
    })}`).subscribe(result => {
      this.table.loading = false;
      this.channelTypes = result;
      this.table.datasets = result.map(res => {
          const childs = res.channels.map(channel => Object.assign({}, {
              id : channel.id, 'Channel Name' : channel.label, 'Channel Type' : res.label
          }));
          return { id : res.id, 'Channel Type' : res.label, childs : childs };
      });
    }, err => {
        this.table.loading = false;
        this.error.open(err);
    });
  }

  createChannelType() {
    this.formChannelType.actionType = 'create';
    this.actionType = 'createChannelType';
    this.form.open(this.formChannelType);
  }

  action(event) {
    switch (event.actionType) {
      case 'editParent':
        this.formChannelType.value  = {id : event.value.id, Label : event.value['Channel Type'] };
        this.formChannelType.actionType = 'edit';
        this.actionType = 'editChannelType';
        this.form.open(this.formChannelType);
      break;

      case 'removeParent':
        this.remove.open({
            id : event.value.id,
            message : `Are you sure to remove ${event.value['Channel Type']}`
        });
        this.actionType = 'removeChannelType';
      break;

      case 'create':
        this.formChannel.actionType = 'edit';
        this.formChannel.models[0].list = this.channelTypes;
        this.formChannel.value = event.value;
        this.form.open(this.formChannel);
        this.actionType = 'createChannel';
      break;

      case 'editChild':
        this.formChannel.actionType = 'edit';
        this.formChannel.value = event.value;
        this.formChannel.models[0].list = this.channelTypes;
        this.form.open(this.formChannel);
        this.actionType = 'editChannel';
      break;

      case 'removeChild':
        this.remove.open({
          id : event.value.id,
          message : `Are you sure to remove ${event.value['Channel Name']}`
        });
        this.actionType = 'removeChannel';
      break;
    }
  }

  save(event) {
    let channelType: ChannelType, channel: Channel;
    switch (this.actionType) {
      case 'createChannelType':
        channelType = { label : event.value.Label };
        this.http.post(`/api/channel_types`, channelType)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;

      case 'editChannelType':
        channelType = { label: event.value.Label };
        this.http.put(`/api/channel_types/${event.value.id}`, channelType)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;

      case 'createChannel':
        channel = {label: event.value['Channel Name'], channel_type_id: event.value['Channel Type']};
        this.http.post('/api/channels', channel)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;

      case 'editChannel':
        channel = {label: event.value['Channel Name'], channel_type_id: event.value['Channel Type']};
        this.http.put(`/api/channels/${event.value.id}`, channel)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;
    }
  }


  saveRemove(event) {
    switch (this.actionType) {
      case 'removeChannelType':
        this.http.delete(`/api/channel_types/${event}`)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;

      case 'removeChannel':
        this.http.delete(`/api/channels/${event}`)
        .subscribe(res => this.getData(), err => this.error.open(err));
      break;
    }
  }



}
