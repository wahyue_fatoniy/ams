import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

const NAVIGATION = [
  {path: 'province', label: 'Province', class: ''},
  {path: 'city', label: 'City', class: ''},
  {path: 'district', label: 'District', class: ''},
  {path: 'site', label: 'Site', class: ''},
  {path: 'equipment-type', label: 'Equipment Type', class: ''},
  {path: 'perf-object', label: 'Performance Object', class: ''},
  {path: 'channel', label: 'Channel', class: ''},
  {path: 'security', label: 'Security', class: ''}
];

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.css']
})
export class SetupComponent implements OnInit {
  constructor(public location: Location) {}

  navs: any = NAVIGATION;

  navActive(path: string) {
    this.navs.forEach(element => {
       element.class = element.path === path ? 'active' : '';
    });
  }

  ngOnInit() {
    const path   = this.location.path();
    const result = path.split('/');
    this.navActive(result[3]);
  }

}
