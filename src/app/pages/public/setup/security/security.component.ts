import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormV2Component,
  FormV2,
  ErrorHandlerComponent,
  TableExpansion,
  Security,
  DialogRemoveComponent,
  UrlEncodePipe,
  SecuritySite,
  Site,
  AuthService
 } from '../../../../shared';
 import { Observable } from 'rxjs/Observable';
 import 'rxjs/add/observable/forkJoin';
 import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {

  constructor(public http: HttpClient,public auth:AuthService) {}
  @ViewChild('form') form: FormV2Component;
  @ViewChild('remove') remove: DialogRemoveComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  encode: UrlEncodePipe = new UrlEncodePipe();
  sites: Site[] = [];
  securities: Security[] = [];
  table: TableExpansion = {
    propertyParent  : ['Username', 'Name', 'Phone', 'E-Mail'],
    propertyChild   : ['Site Name', 'Address'],
    datasets        : [],
    buttons         : this.auth.userHasScopes(['write:security']) ? 
    ['create', 'removeParent', 'removeChild'] : [],
    loading         : false
  };

  formSecurity: FormV2 = {
    title : 'Form Security',
    actionType : '',
    value : {},
    models : [
      {type: 'text', model : 'Name'}, {type: 'email', model: 'E-Mail'}, {type: 'phone', model: 'Phone'},
      {type: 'text', model: 'Username'}, {type: 'password', model: 'Password'}
    ]
  };
  formSecuritySite: FormV2 = {
    title : 'Form Assignment Site',
    actionType : '',
    value : {},
    models : [
      {type: 'list', model: 'Name', listLabel: 'name', idSet: 'id', list: [], disabled: true},
      {type: 'list', model: 'Site', listLabel: 'label', idSet: 'id', list: [], multiple: true}
    ]
  };
  actionType: string;

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.table.loading = true;
    Observable.forkJoin([
      this.http.get<Security[]>(`/api/securities?filter=${this.encode.transform({
        order: 'name',
        include : {
          relation : 'securitySites',
          scope : { include : ['site', 'security'] }
        }
      })}`),
      this.http.get<Site[]>(`/api/sites?filter=${this.encode.transform({order: 'label'})}`)
    ]).subscribe(result => {
        this.table.loading = false;
        this.securities    = result[0];
        this.sites         = result[1];
        this.table.datasets = result[0].map(res => {
            const childs = res.securitySites.map(site => Object.assign({}, {
              id : site.id, 'Site Name': site.site.label, Address : site.site.address, site_id : site.site_id,
              security : site.security
            }));
            return {
              id : res.id,
              Username : res.username,
              Name : res.name,
              Phone : res.phone,
              'E-Mail' : res.email,
              childs : childs
            };
        });
    }, err => {
      this.table.loading = false;
      this.error.open(err);
    });
  }

  createSecurity() {
    this.formSecurity.actionType = 'create';
    this.form.open(this.formSecurity);
    this.actionType = 'createSecurity';
  }

  action(event) {
    switch (event.actionType) {
      case 'removeParent':
        this.remove.open({
          id : event.value.id,
          message : `Are you sure to remove security ${event.value.Name}`
        });
        this.actionType = 'removeSecurity';
      break;

      case 'create':
        this.formSecuritySite.actionType = 'edit';
        this.formSecuritySite.models[0].list = this.securities;
        this.formSecuritySite.models[1].list = this.sites.filter(res => {
           const index = event.value.childs.findIndex(child => child.site_id === res.id);
           if (index === -1) {
              return res;
           }
        });
        this.formSecuritySite.value = {id: event.value.id, Name: event.value.Name};
        this.form.open(this.formSecuritySite);
        this.actionType = 'createChild';
      break;

      case 'removeChild':
        this.remove.open({
          id : event.value.id,
          message : `Are you sure to remove assignment ${event.value['Site Name']}`
          + ` to user ${event.value.security.name}`
        });
        this.actionType = 'removeChild';
      break;
    }
  }

  save(event) {
    switch (this.actionType) {
      case 'createSecurity':
         const security: Security = {
            name : event.value.Name,
            username : event.value.Username,
            email : event.value['E-Mail'],
            password : event.value.Password,
            phone : event.value.Phone
         };
         this.http.post<Security>(`/api/securities`, security)
         .subscribe(res => this.getData(), err => this.error.open(err));
      break;
      case 'createChild':
         let services = [], value: SecuritySite;
         event.value.Site.forEach(res => {
            value = { security_id : event.value.Name, site_id : res };
            services.push(this.http.post<SecuritySite>(`/api/security_sites`, value));
         });
         Observable.forkJoin(services).subscribe(res => this.getData(), err => this.error.open(err));
      break;
    }
  }

  saveRemove(event) {
      switch (this.actionType) {
        case 'removeSecurity':
          this.http.delete(`/api/securities/${event}`)
          .subscribe(res => this.getData(), err => this.error.open(err));
        break;

        case 'removeChild':
          this.http.delete(`/api/security_sites/${event}`)
          .subscribe(res => this.getData(), err => this.error.open(err));
        break;
      }
  }

}
