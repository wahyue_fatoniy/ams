import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css']
})
export class PublicComponent implements OnInit {

  constructor(public auth: AuthService) {}

  ngOnInit() {
  }

}
