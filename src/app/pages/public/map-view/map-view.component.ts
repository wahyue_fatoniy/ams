import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {
    Province,
    City,
    District,
    Site,
    DEFAULT_LOCATION,
    UrlEncodePipe,
    StorageService,
    ErrorHandlerComponent,
    Alarm,
    SocketService,
    ClearAlarmPipe
 } from '../../../shared';
 import { Observable } from 'rxjs/Observable';
 import 'rxjs/add/observable/forkJoin';


 @Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.css']
})
export class MapViewComponent implements OnInit {

  constructor(
    public http: HttpClient,
    public storage: StorageService,
    public router: Router,
    public socket: SocketService
  ) {}
  clear: ClearAlarmPipe = new ClearAlarmPipe();
  encode: UrlEncodePipe = new UrlEncodePipe();
  provinces: Province[] = [];
  cities: City[] = [];
  districts: District[] = [];
  sites: Site[] = [];
  alarms: Alarm[] = [];
  @ViewChild('error') error: ErrorHandlerComponent;

  list = {provinces: [], cities: [], districts: [], sites: []};
  search = '';
  pageListAlarm = false;
  id_province: number;
  id_city: number;
  id_district: number;
  id_site: number;
  map       = {center: DEFAULT_LOCATION, markers: []};
  loading = false;
  siteAlarm = 0;

  ngOnInit() {
    this.getData();
    this.socket.streaming('alarm.data').subscribe((res: Alarm) => {
       const index = this.alarms.findIndex(alarm => alarm.id === res.id );
       this.alarms = this.clear.transform(this.alarms,res);
       if (index === -1 && res.severity !== 'CLEAR'){
         this.alarms.push(res);
       }
       this.setAlarm();
    });
  }

  getData() {
    this.loading = true;
    Observable.forkJoin([
      this.http.get<Province>(`/api/provinces?filter=${this.encode.transform({order: 'label'})}`),
      this.http.get<City[]>(`/api/cities?filter=${this.encode.transform({order: 'label'})}`),
      this.http.get<District[]>(`/api/districts?filter=${this.encode.transform({order: 'label', include: ['city']})}`),
      this.http.get<Site[]>(`/api/sites?filter=${this.encode.transform({
        order: 'label',
        include: [
          { relation : 'district', scope : { include : ['city'] }},
          { relation : 'managedObjects'}
        ]
       })}`),
      this.http.get<Alarm[]>(`/api/alarms`)
    ]).subscribe(res => {
      this.loading = false;
      this.provinces = res[0];
      this.cities    = res[1];
      this.districts = res[2];
      this.sites     = res[3];
      this.alarms    = res[4];
      this.refresh();
    }, err => this.error.open(err));
  }

  refresh() {
    this.id_province = undefined;
    this.id_city = undefined;
    this.id_district = undefined;
    this.id_site = undefined;
    this.setAlarm();
    this.list = {
      provinces : Object.assign([], this.provinces),
      cities : Object.assign([], this.cities),
      districts : Object.assign([], this.districts),
      sites : Object.assign([], this.sites)
    };
    this.map.center = Object.assign({}, DEFAULT_LOCATION);
    this.setMarkers(this.sites);
  }


  setAlarm() {
    this.sites = this.sites.map(site => {
        let managed = [site.discobj_id];
        managed = managed.concat(site.managedObjects.map(res => res.discobj_id));
        const alarmSet = this.alarms.filter(res => {
           const index = managed.findIndex(obj => obj === res.alarmObj.id);
           if (index !== -1 ) {
             return res;
           }
        });
        site.alarm = alarmSet.length !== 0 ? true : false;
        return site;
    });
    this.siteAlarm = this.sites.filter(res => res.alarm == true).length;
    this.setMarkers(this.sites);
  }

  setMarkers(list: Array<any>) {
     this.map.markers  = list.map(res => Object.assign({}, {
        id : res.id,
        label : res.label,
        latitude : res.location.lat,
        longitude : res.location.lng,
        zoom : res.zoom,
        alarm : res.alarm
     }));
  }

  setCenter(list: Array<any>, id: number) {
     const index: number = list.findIndex(res => res.id === id);
     this.map.center = {
        label : list[index].label,
        latitude : list[index].location.lat,
        longitude : list[index].location.lng,
        zoom : list[index].zoom
     };
  }

  setProvince() {
    this.id_district = undefined;
    this.id_site = undefined;
    this.id_city = undefined;
    this.list.cities = this.cities.filter(res => res.province_id === this.id_province);
    this.list.districts = this.districts.filter(res => res.city.province_id === this.id_province);
    this.list.sites = this.sites.filter(res => res.district.city.province_id === this.id_province);
    this.setCenter(this.list.provinces, this.id_province);
  }

  setCity() {
    this.id_district = undefined;
    this.id_site = undefined;
    this.list.districts = this.districts.filter(res => res.city_id === this.id_city);
    this.list.sites = this.sites.filter(res => res.district.city_id === this.id_city);
    this.setCenter(this.list.cities, this.id_city);
  }

  setDistrict() {
    this.id_site = undefined;
    this.list.sites = this.sites.filter(res => res.district_id === this.id_district);
    this.setCenter(this.list.districts, this.id_district);
  }

  setSite() {
    this.setCenter(this.list.sites, this.id_site);
  }

  locationDetail(site) {
    this.storage.site = Object.assign({}, site);
    this.router.navigate(['home/site/alarm-site']);
  }

  setMarker(event) {
    const index: number = this.sites.findIndex(res => res.id === event.id);
    this.storage.site =  Object.assign({}, this.sites[index]);
    this.router.navigate(['home/site/alarm-site']);
  }


}
