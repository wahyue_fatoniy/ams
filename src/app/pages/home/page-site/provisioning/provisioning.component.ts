import { Component, OnInit, ViewChild } from '@angular/core';
import { StorageService, Discobject, ErrorHandlerComponent,
   Provisioning,
   FormProvisioningDialogComponent
  } from '../../../../shared';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-provisioning',
  templateUrl: './provisioning.component.html',
  styleUrls: ['./provisioning.component.css']
})
export class ProvisioningComponent implements OnInit {

  constructor(public storage:StorageService, public http:HttpClient){}
  @ViewChild('error') error:ErrorHandlerComponent;
  @ViewChild('form') form:FormProvisioningDialogComponent;
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  loading:boolean = false;
  provisionings:Provisioning[] = [];
  commandType: Object = {
    PL1BT: { padlock: 'LOCK' },
    L100w: { power: 'ON' },
    AC: { power: 'ON', temperature: '' },
    Chiller: { power: 'ON', temperature: '' },
    Pump: { power: 'ON' }
  }

  ngOnInit(){
    this.getData();  
  }

  getData(){
     this.loading = true;
     this.http.get<Discobject[]>(`/api/discobjects/${this.latitude}/${this.longitude}`)
     .subscribe(result=>{
       this.loading = false;
       this.provisionings = result.filter(res=>res.eqType.type != 'Site' && res.eqType.type != 'Zone')
          .map(res => {
          return Object.assign({}, {
            id: res.id,
            label: res.name,
            type: res.eqType['type'],
            address: this.storage.site.address,
            command: this.commandType[res.eqType['type']]
          })
        });
     },err=>{
        this.loading = true;
        this.error.open(err);
     });
  }

  action(event){
    this.form.openDialog(event);
  }

  save(event){
    console.log(event);
  }

}
