import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormV2Component,
  FormV2,
  ErrorHandlerComponent,
  TableExpansion,
  DialogRemoveComponent,
  UrlEncodePipe,
  Sensor,
  MappingSensor,
  Discobject,
  StorageService,
  ChannelType,
  SortDataPipe,
  AuthService
} from '../../../../shared';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-mapping-sensor',
  templateUrl: './mapping-sensor.component.html',
  styleUrls: ['./mapping-sensor.component.css']
})
export class MappingSensorComponent implements OnInit {
  constructor(
    public http: HttpClient,
    public storage: StorageService,
    public auth: AuthService
  ) {}
  @ViewChild('form') form: FormV2Component;
  @ViewChild('remove') remove: DialogRemoveComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  encode: UrlEncodePipe = new UrlEncodePipe();
  sensors: Sensor[] = [];
  discobjects: Discobject[] = [];
  channelTypes: ChannelType[] = [];
  sort: SortDataPipe = new SortDataPipe();
  table: TableExpansion = {
    propertyParent: ['Sensor', 'Sensor Type'],
    propertyChild: ['Channel', 'Managed Object', 'Equipment Type'],
    datasets: [],
    buttons: this.auth.userHasScopes(['write:mappingSensor'])
      ? ['create', 'removeParent', 'removeChild']
      : [],
    loading: false
  };

  formSensor: FormV2 = {
    title: 'Form Sensor',
    actionType: '',
    value: {},
    models: [
      { type: 'text', model: 'Sensor' },
      {
        type: 'list',
        model: 'Sensor Type',
        listLabel: 'label',
        idSet: 'id',
        list: []
      }
    ]
  };
  formMappingSensor: FormV2 = {
    title: 'Form Mapping Sensor',
    actionType: '',
    value: {},
    models: [
      {
        type: 'list',
        model: 'Sensor',
        listLabel: 'label',
        idSet: 'id',
        list: [],
        disabled: true
      },
      {
        type: 'list',
        model: 'Managed Object',
        listLabel: 'name',
        idSet: 'id',
        list: []
      },
      {
        type: 'list',
        model: 'Channel',
        listLabel: 'label',
        idSet: 'id',
        list: [],
        multiple: true
      }
    ]
  };
  actionType: string;

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.table.loading = true;
    Observable.forkJoin([
      this.http.get<Sensor[]>(
        `/api/sensors?filter=${this.encode.transform({
          order: 'label',
          where: { site_id: this.storage.site.id },
          include: [
            { relation: 'mappingSensors', scope: { include: ['channel'] } },
            { relation: 'channelType' }
          ]
        })}`
      ),
      this.http.get<Discobject[]>(
        `/api/discobjects/${this.latitude}/${this.longitude}/`
      ),
      this.http.get<ChannelType[]>(
        `/api/channel_types?filter=${this.encode.transform({
          order: 'label',
          include: ['channels']
        })}`
      )
    ]).subscribe(
      result => {
        this.table.loading = false;
        this.sensors = result[0];
        this.discobjects = this.sort.transform(result[1], 'name', 'asc');
        this.channelTypes = result[2];
        this.table.datasets = result[0].map(res => {
          const childs = res.mappingSensors.map(item => {
            const index: number = this.discobjects.findIndex(
              disc => disc.id === item.discobj_id
            );
            return {
              id: item.id,
              Channel: item.channel.label,
              'Managed Object': this.discobjects[index].name,
              'Equipment Type': this.discobjects[index].eqType.type,
              channel_id: item.channel_id
            };
          });
          return {
            id: res.id,
            Sensor: res.label,
            'Sensor Type': res.channelType.label,
            childs: childs,
            channel_type_id: res.channel_type_id
          };
        });
      },
      err => {
        this.table.loading = false;
        this.error.open(err);
      }
    );
  }

  createSensor() {
    this.formSensor.actionType = 'create';
    this.formSensor.models[1].list = this.channelTypes;
    this.actionType = 'createSensor';
    this.form.open(this.formSensor);
  }

  action(event) {
    switch (event.actionType) {
      case 'removeParent':
        this.remove.open({
          id: event.value.id,
          message: `Are you sure to remove ${event.value.Sensor}`
        });
        this.actionType = 'removeSensor';
        break;

      case 'create':
        this.formMappingSensor.actionType = 'edit';
        const channelTypeIndex = this.channelTypes.findIndex(
          res => res.id === event.value.channel_type_id
        );
        let channels = [];
        const used = event.value.childs;
        if (channelTypeIndex !== -1) {
          channels = this.channelTypes[channelTypeIndex].channels;
        }
        channels = channels.filter(res => {
          const index = used.findIndex(use => use.channel_id === res.id);
          if (index === -1) {
            return res;
          }
        });
        this.formMappingSensor.models[0].list = this.sensors;
        this.formMappingSensor.models[1].list = this.discobjects;
        this.formMappingSensor.models[2].list = channels;
        this.formMappingSensor.value = { Sensor: event.value['Sensor'] };
        this.actionType = 'createMappingSensor';
        this.form.open(this.formMappingSensor);
        break;

      case 'removeChild':
        this.actionType = 'removeMappingSensor';
        this.remove.open({
          id: event.value.id,
          message:
            `Are you sure to remove ${event.value.Channel} in ` +
            `managed object ${event.value['Managed Object']}`
        });
        break;
    }
  }

  save(event) {
    let sensor: Sensor, mapping: MappingSensor;
    switch (this.actionType) {
      case 'createSensor':
        sensor = {
          label: event.value.Sensor,
          channel_type_id: event.value['Sensor Type'],
          site_id: this.storage.site.id
        };
        this.http
          .post(`/api/sensors`, sensor)
          .subscribe(res => this.getData(), err => this.error.open(err));
        break;

      case 'createMappingSensor':
        const services = [];
        event.value.Channel.forEach(res => {
          mapping = {
            sensor_id: event.value.Sensor,
            channel_id: res,
            discobj_id: event.value['Managed Object']
          };
          services.push(this.http.post('/api/mapping_sensors', mapping));
        });
        Observable.forkJoin(services).subscribe(
          res => this.getData(),
          err => this.error.open(err)
        );
        break;
    }
  }

  saveRemove(event) {
    switch (this.actionType) {
      case 'removeSensor':
        this.http
          .delete(`/api/sensors/${event}`)
          .subscribe(res => this.getData(), err => this.error.open(err));
        break;

      case 'removeMappingSensor':
        this.http
          .delete(`/api/mapping_sensors/${event}`)
          .subscribe(res => this.getData(), err => this.error.open(err));
        break;
    }
  }
}
