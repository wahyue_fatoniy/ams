import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingSensorComponent } from './mapping-sensor.component';

describe('MappingSensorComponent', () => {
  let component: MappingSensorComponent;
  let fixture: ComponentFixture<MappingSensorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingSensorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
