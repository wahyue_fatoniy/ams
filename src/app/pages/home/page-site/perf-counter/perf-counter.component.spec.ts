import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfCounterComponent } from './perf-counter.component';

describe('PerfCounterComponent', () => {
  let component: PerfCounterComponent;
  let fixture: ComponentFixture<PerfCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
