import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormV2Component,
  FormV2,
  ErrorHandlerComponent,
  TableExpansion,
  DialogRemoveComponent,
  UrlEncodePipe,
  Discobject,
  PerfCounter,
  PerfObject,
  SortDataPipe,
  StorageService,
  AuthService
 } from '../../../../shared';
 import { Observable } from 'rxjs/Observable';
 import 'rxjs/add/observable/forkJoin';
 import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-perf-counter',
  templateUrl: './perf-counter.component.html',
  styleUrls: ['./perf-counter.component.css']
})
export class PerfCounterComponent implements OnInit {

  constructor(public http: HttpClient, public storage: StorageService,public auth:AuthService) {}
  @ViewChild('form') form: FormV2Component;
  @ViewChild('remove') remove: DialogRemoveComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  encode: UrlEncodePipe = new UrlEncodePipe();
  perfobjects: PerfObject[] = [];
  counters: PerfCounter[]   = [];
  discobjects: Discobject[] = [];
  sort: SortDataPipe = new SortDataPipe();
  table: TableExpansion = {
    propertyParent  : ['Managed Object', 'Equipment Type'],
    propertyChild   : ['Name', 'Period'],
    datasets        : [],
    buttons         : this.auth.userHasScopes(['write:perfCounter'])?['create']:[],
    loading         : false
  };

  formCounter: FormV2 = {
    title : 'Form Sensor',
    actionType : '',
    value : {},
    models : [
      {type: 'list', model: 'Managed Object', listLabel: 'name', idSet: 'id', list: [], disabled: true},
      {type: 'list', model: 'Performance Object', listLabel: 'label', idSet: 'id', list: [], multiple: true}
    ]
  };
  actionType: string;

  ngOnInit() {
    this.getData();
  }

  getData() {
      this.table.loading = true;
      Observable.forkJoin([
        this.http.get<PerfCounter[]>(`/api/perfcounters/${this.latitude}/${this.longitude}/`),
        this.http.get<Discobject>(`/api/discobjects/${this.latitude}/${this.longitude}/`),
        this.http.get<PerfObject[]>(`/api/perfobjects`)
      ]).subscribe(result => {
          this.table.loading = false;
          this.counters      = result[0];
          console.log(this.counters);
          this.discobjects   = this.sort.transform(result[1], 'name', 'asc');
          this.perfobjects   = this.sort.transform(result[2], 'name', 'asc');
          this.perfobjects   = this.perfobjects.map(res => Object.assign({}, res, {
            label : `${res.name} ${res.period}`
          }));
          this.table.datasets = this.discobjects.map(res => {
              const childs = this.counters.filter(counter => counter.discObj.id === res.id)
              .map(counter => Object.assign({}, {
                  id : counter.id,
                  Name : counter.perfObj.name,
                  Period : counter.perfObj.period,
                  perfobj_id : counter.perfObj.id,
                  'Managed Object' : res.name
              }));
              return {
                id : res.id,
                'Managed Object' : res.name,
                'Equipment Type' : res.eqType.type,
                childs : childs
             };
          });
      }, err => {
         this.table.loading = false;
         this.error.open(err);
      });
  }


  action(event) {
    switch (event.actionType) {
      case 'create':
        this.formCounter.actionType = 'edit';
        this.formCounter.value = {'Managed Object': event.value['Managed Object']};
        this.formCounter.models[0].list = this.discobjects;
        const used = event.value.childs;
        this.formCounter.models[1].list = this.perfobjects.filter(res => {
            const index = used.findIndex(use => use.perfobj_id === res.id);
            if (index === -1) {
              return res;
            }
        });
        this.form.open(this.formCounter);
      break;

      case 'removeChild':
        this.remove.open({
          id : event.value.id,
          message :  `Are you sure to remove ${event.value.Name} ${event.value.Period} `
          + `in managed object ${event.value['Managed Object']}`
        });
      break;
    }
  }

  save(event) {
    const services = []; let counter: PerfCounter;
    event.value['Performance Object'].forEach(res => {
        counter = {
          perfObj : { id : res },
          discObj : { id : event.value['Managed Object']}
        };
        services.push(this.http.post('/api/perfcounters', counter));
    });
    Observable.forkJoin(services).subscribe(res => this.getData(), err => this.error.open(err));
  }


  saveRemove(event) {
    this.http.delete(`/api/perfcounters/${event}`)
    .subscribe(res => this.getData(), err => this.error.open(err));
  }

}
