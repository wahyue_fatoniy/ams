import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  PerfMeasurement,
  Alarm,
  StorageService,
  ErrorHandlerComponent,
  PerfCounter,
  InfoHighchartPipe,
  Discobject,
  LineChart,
  MapArea,
  WidgetType1,
  WidgetType2,
  BarChart,
  ConvertionValuePipe,
  FormatDatePipe,
  ManagedObject,
  UrlEncodePipe,
  Site,
  Lamp,
  PadLock,
  AC,
  Item,
  SocketService,
  LineChartV2Component
} from '../../../../shared';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { element } from 'protractor';
import { build$ } from 'protractor/built/element';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html'
})
export class ViewComponent implements OnInit {
  constructor(
    public http: HttpClient,
    public storage: StorageService,
    public socket: SocketService
  ) {}
  loading = false;
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  convert: ConvertionValuePipe = new ConvertionValuePipe();
  info: InfoHighchartPipe = new InfoHighchartPipe();
  encode: UrlEncodePipe = new UrlEncodePipe();
  date: FormatDatePipe = new FormatDatePipe();
  counters: PerfCounter[] = [];
  measurements: PerfMeasurement[] = [];
  alarms: Alarm[] = [];
  discobjects: Discobject[] = [];
  equipment: Site;
  widget_total: WidgetType1 = {
    unit: 'W',
    value: 0,
    label: 'Total Energy',
    icon: 'fa fa-bolt'
  };
  widget_ac: WidgetType1 = {
    unit: 'W',
    value: 0,
    label: 'AC Energy',
    icon: 'fa fa-bolt'
  };
  widget_chiller: WidgetType1 = {
    unit: 'W',
    value: 0,
    label: 'Chiller Energy',
    icon: 'fa fa-bolt'
  };
  widget_other: WidgetType1 = {
    unit: 'W',
    value: 0,
    label: 'Other Energy',
    icon: 'fa fa-bolt'
  };
  widget_temperature: WidgetType2 = {
    unit: '°C',
    min: 0,
    max: 0,
    label: 'Room Temp',
    icon: 'fa fa-thermometer-full'
  };
  widget_humidity: WidgetType2 = {
    unit: '%',
    min: 0,
    max: 0,
    label: 'Room Humidity',
    icon: 'fa fa-tint'
  };
  barchart: any = {
    datasets: [],
    labels: [],
    chartType: localStorage.getItem('barchart') || 'daily'
  };
  items: MapArea = {
    loading: false,
    buildingView: false,
    ac: [],
    pump: [],
    chiller: [],
    atm: [],
    door: [],
    rack: [],
    cashier: [],
    lamp: [],
    temperature: [],
    lock: [],
    updatePosition: { equipment: [], building: [] },
    removeBuilding: []
  };
  linechart: LineChart = {
    chartType: localStorage.getItem('linechart-chartType') || 'daily',
    perfType: localStorage.getItem('linechart-perfType') || 'power',
    series: [],
    loading: false
  };
  @ViewChild('error') error: ErrorHandlerComponent;
  @ViewChild('chart') chart: LineChartV2Component;
  ngOnInit() {
    this.socket
      .streaming('perfmeasurement.data')
      .subscribe((res: PerfMeasurement) => {
        this.chart.addPoint(res);
        const indexCounter = this.counters.findIndex(
          (counter: PerfCounter) => counter.id === res.counter.id
        );
        const indexMeasurement = this.measurements.findIndex(
          (item: PerfMeasurement) => item.id === res.id
        );
        if (indexCounter !== -1 && indexMeasurement === -1) {
          res.counter = this.counters[indexCounter];
          this.measurements.push(res);
          this.getWidgets();
          this.getBarChart();
          this.getAreas();
        }
      });
    this.socket.streaming('alarm.data').subscribe((res: Alarm) => {
      const indexDiscobject = this.discobjects.findIndex(
        (disc: Discobject) => disc.id === res.alarmObj.id
      );
      const indexAlarm = this.alarms.findIndex(
        (alarm: Alarm) => alarm.id === res.id
      );
      if (indexDiscobject !== -1 && indexAlarm === -1) {
        this.alarms.push(res);
        this.getWidgets();
        this.getBarChart();
        this.getAreas();
      }
    });
    this.getData();
  }

  getData() {
    this.loading = true;
    Observable.forkJoin([
      this.http.get<Discobject[]>(
        `/api/discobjects/${this.latitude}/${this.longitude}/`
      ),
      this.http.get<PerfCounter[]>(
        `/api/perfcounters/${this.latitude}/${this.longitude}`
      ),
      this.http.get<PerfMeasurement[]>(
        `/api/perfmeasurements/${this.latitude}/${this.longitude}`
      ),
      this.http.get<Alarm[]>(`/api/alarms/${this.latitude}/${this.longitude}`),
      this.http.get<Site>(
        `/api/sites/findOne?filter=${this.encode.transform({
          where: { id: this.storage.site.id },
          include: ['managedObjects', 'items']
        })}`
      )
    ]).subscribe(
      result => {
        this.loading = false;
        this.discobjects = result[0];
        this.counters = result[1];
        this.measurements = result[2];
        this.alarms = result[3];
        this.equipment = result[4];
        this.getWidgets();
        this.getBarChart();
        this.getAreas();
        this.getLineChart();
      },
      err => {
        this.loading = false;
        this.error.open(err);
      }
    );
  }

  getLineChart() {
    localStorage.setItem('linechart-perfType', this.linechart.perfType);
    localStorage.setItem('linechart-chartType', this.linechart.chartType);
    const timeType = { daily: 'MIN5', weekly: 'HOUR1', monthly: 'HOUR6' };
    const counter = this.counters.filter(
      res => res.perfObj.period === timeType[this.linechart.chartType]
    );
    // selain perf type security series berdasarkan perfcounter, kalau security langsung menggunakan Discobject
    const seriesList = {
      power: counter.filter(res => res.perfObj.name === 'Daya'),
      temperature: counter.filter(
        res =>
          res.perfObj.name === 'Temperatur Ruang' ||
          res.perfObj.name === 'Temperatur Coil'
      ),
      humidity: counter.filter(res => res.perfObj.name === 'Kelembapan Relatif')
    };
    let series = seriesList[this.linechart.perfType];
    series = series.map(res => {
      // set value
      const data = this.measurements
        .filter(value => value.counter.id === res.id)
        .map(value =>
          Object.assign(
            {},
            {
              x: Date.parse(value.measurementTime),
              y: value.measurementValue,
              info: this.info.transform({
                time: value.measurementTime,
                value: value.measurementValue,
                eqType: value.counter.discObj.eqType.type,
                managedObj: value.counter.discObj.name,
                perfType: value.counter.perfObj.name
              })
            }
          )
        );

      // set series
      return Object.assign(
        {},
        {
          name: res.discObj.name,
          yAxis: ['power', 'temperature', 'humidity'].findIndex(
            // tslint:disable-next-line:no-shadowed-variable
            res => res === this.linechart.perfType
          ),
          data: data,
          showInLegend: true,
          type: 'line',
          cursor: 'pointer',
          marker: {
            enabled: data.length <= 15 ? true : false,
            symbol: `url(./assets/img/iconHighChart/${
              res.discObj.eqType.type
            }.png)`
          },
          perfCounter: res
        }
      );
    });
    this.linechart.series = series;
  }

  getAreas() {
    const list = this.equipment.managedObjects.map(res => {
      const index = this.discobjects.findIndex(
        discObj => discObj.id === res.discobj_id
      );
      return Object.assign({}, res, { discobject: this.discobjects[index] });
    });

    const lamp: Lamp[] = [],
      lock: PadLock[] = [],
      ac: AC[] = [];
    // tslint:disable-next-line:forin
    for (const i in list) {
      const set: any = {
        id: list[i].id,
        label: list[i].discobject.name,
        top: list[i].top,
        left: list[i].left,
        id_discobject: list[i].discobj_id
      };
      const type = list[i].discobject.eqType.type;
      switch (type) {
        case 'PL1BT':
          set['severity'] = 'clear';
          lock.push(set);
          break;
        case 'L100w':
          set['power'] = 0;
          lamp.push(set);
          break;
        case 'AC':
          set['power'] = 0;
          set['temperature'] = 0;
          set['severity'] = 'clear';
          ac.push(set);
          break;
      }
    }

    const datasets = this.measurements.filter(
      res => res.counter.perfObj.period === 'MIN5'
    );
    const power = datasets.filter(res => res.counter.perfObj.name === 'Energi');

    // generate power
    // tslint:disable-next-line:forin
    for (const i in power) {
      const lampIndex = lamp.findIndex(
        res => res.id_discobject === power[i].counter.discObj.id
      );
      const acIndex = ac.findIndex(
        res => res.id_discobject === power[i].counter.discObj.id
      );
      if (acIndex !== -1) {
        ac[acIndex].power += power[i].measurementValue;
      }
      if (lampIndex !== -1) {
        lamp[lampIndex].power += power[i].measurementValue;
      }
    }

    // generate ac temperature
    // tslint:disable-next-line:forin
    for (const i in datasets) {
      const coilIndex = ac.findIndex(
        res =>
          res.id_discobject === datasets[i].counter.discObj.id &&
          datasets[i].counter.perfObj.name === 'Temperatur Coil'
      );
      if (coilIndex !== -1) {
        ac[coilIndex].temperature = datasets[i].measurementValue;
      }
    }

    const alarms: Alarm[] = Object.assign([], this.alarms);
    // tslint:disable-next-line:forin
    for (const i in alarms) {
      const severity = alarms[i].severity.toLocaleLowerCase();
      const lockIndex = lock.findIndex(
        res => res.id_discobject === alarms[i].alarmObj.id
      );
      const acIndex = ac.findIndex(
        res => res.id_discobject === alarms[i].alarmObj.id
      );
      lock[lockIndex] =
        lockIndex === -1
          ? lock[lockIndex]
          : Object.assign({}, lock[lockIndex], { severity: severity });
      ac[acIndex] =
        acIndex === -1
          ? ac[acIndex]
          : Object.assign({}, ac[acIndex], { severity: severity });
    }

    this.items.lamp = lamp;
    this.items.lock = lock;
    this.items.ac = ac;
    this.items.atm = this.equipment.items.filter(res => res.type === 'atm');
    this.items.cashier = this.equipment.items.filter(
      res => res.type === 'cashier'
    );
    this.items.rack = this.equipment.items.filter(
      res =>
        res.type === 'rack2x2' || res.type === 'rack5x2' || res.type === 'rack5x1'
    );
    this.items.door = this.equipment.items.filter(
      res =>
        res.type === 'door-left1' ||
        res.type === 'door-top1' ||
        res.type === 'door-right1' ||
        res.type === 'door-bottom1' ||
        res.type === 'door-left2' ||
        res.type === 'door-top2' ||
        res.type === 'door-right2' ||
        res.type === 'door-bottom2'
    );
  }

  createBuilding(event) {
    const value: Item = {
      type: event.type,
      top: event.top,
      left: event.left,
      site_id: this.storage.site.id
    };
    this.http
      .post('/api/items', value)
      .subscribe(res => this.getData(), err => this.error.open(err));
  }

  mapAreaAction(event) {
    function Remove(list, id) {
      for (const i in list) {
        if (list[i].id === id) {
          // tslint:disable-next-line:radix
          list.splice(parseInt(i), 1);
        }
      }
      return list;
    }
    switch (event.type) {
      case 'removeBuilding':
        this.items.removeBuilding.push(event.value);
        Remove(this.items[event.value.buildingType], event.value.id);
        Remove(this.items.updatePosition.building, event.value.id);
        break;

      case 'cancel':
        this.items.updatePosition.equipment = [];
        this.items.updatePosition.building = [];
        this.items.buildingView = false;
        this.getAreas();
        break;

      case 'buildingView':
        this.items.buildingView = true;
        break;

      case 'save':
        this.items.buildingView = false;
        if (this.items.removeBuilding.length !== 0) {
          const removes = [];
          // tslint:disable-next-line:forin
          for (const i in this.items.removeBuilding) {
            removes.push(
              this.http.delete(`/api/items/${this.items.removeBuilding[i].id}`)
            );
          }
          Observable.forkJoin(removes).subscribe(
            res => this.getData(),
            err => this.error.open(err)
          );
        }
        if (this.items.updatePosition.equipment.length !== 0) {
          const equipments = [];
          // tslint:disable-next-line:no-shadowed-variable
          this.items.updatePosition.equipment.forEach(element => {
            const index = this.equipment.managedObjects.findIndex(
              res => element.id === res.id
            );
            const value = Object.assign(
              {},
              this.equipment.managedObjects[index],
              {
                top: element.top,
                left: element.left,
                id: undefined
              }
            );
            equipments.push(
              this.http.put(`/api/managed_objects/${element.id}`, value)
            );
          });
          Observable.forkJoin(equipments).subscribe(
            res => this.getData(),
            err => this.error.open(err)
          );
        }
        if (this.items.updatePosition.building.length !== 0) {
          const building = [];
          // tslint:disable-next-line:no-shadowed-variable
          this.items.updatePosition.building.forEach(element => {
            const index = this.equipment.items.findIndex(
              res => element.id === res.id
            );
            const value = Object.assign({}, this.equipment.items[index], {
              top: element.top,
              left: element.left,
              id: undefined
            });
            building.push(this.http.put(`/api/items/${element.id}`, value));
          });
          Observable.forkJoin(building).subscribe(
            res => this.getData(),
            err => this.error.open(err)
          );
        }
        break;

      default:
        const data = this.items.updatePosition[event.type];
        let valid = true;
        for (const i in data) {
          if (data[i].id === event.value.id) {
            valid = false;
            data[i] = event.value;
          }
        }
        if (valid) {
          data.push(event.value);
        }
        this.items.updatePosition[event.type] = data;
        break;
    }
  }

  getLabelBarChart() {
    const value = { daily: [], weekly: [], monthly: [] };
    value.daily[0] = this.date.transform(new Date(), 'DD MMMM YYYY');
    for (let i = 6; i >= 0; i--) {
      const date = new Date();
      date.setDate(date.getDate() - i);
      const set = this.date.transform(date, 'DD/MM/YYYY');
      value.weekly.push(set);
    }
    for (let i = 2; i >= 0; i--) {
      const date = new Date();
      date.setMonth(date.getMonth() - i);
      const set = this.date.transform(date, 'MMMM');
      value.monthly.push(set);
    }
    return value[this.barchart.chartType];
  }

  // Bar chart
  getBarChart() {
    localStorage.setItem('barchart', this.barchart.chartType);
    const labels = this.getLabelBarChart();
    this.barchart.labels = labels;
    let ac: any = {},
      chiller: any = {},
      store: any = {},
      other: any = {};
    const timeType = { daily: 'MIN5', weekly: 'HOUR1', monthly: 'HOUR6' };
    const format = {
      daily: 'DD MMMM YYYY',
      weekly: 'DD/MM/YYYY',
      monthly: 'MMMM'
    };
    const datasets = this.measurements.filter(
      res =>
        res.counter.perfObj.period === timeType[this.barchart.chartType] &&
        res.counter.perfObj.name === 'Energi'
    );
    // tslint:disable-next-line:forin
    for (const i in datasets) {
      const set = this.date.transform(
        datasets[i].measurementTime,
        format[this.barchart.chartType]
      );
      const eqType = datasets[i].counter.discObj.eqType.type;
      if (store[set] === undefined) {
        store[set] = 0;
      }
      if (ac[set] === undefined) {
        ac[set] = 0;
      }
      if (chiller[set] === undefined) {
        chiller[set] = 0;
      }
      if (other[set] === undefined) {
        other[set] = 0;
      }
      switch (eqType) {
        case 'Site':
          store[set] += datasets[i].measurementValue;
          break;
        case 'AC':
          store[set] += datasets[i].measurementValue;
          break;
        case 'Chiller':
          chiller[set] += datasets[i].measurementValue;
          break;
      }
    }
    ac = Object.keys(ac).map(key => ac[key]);
    chiller = Object.keys(chiller).map(key => chiller[key]);
    other = Object.keys(other).map(key => other[key]);
    store = Object.keys(store).map(key => store[key]);
    // tslint:disable-next-line:forin
    for (const i in store) {
      other[i] = store[i] - (ac[i] + chiller[i]);
    }
    const result = [
      { data: ac, label: 'AC', borderWidth: 2, borderStyle: 'dash' },
      { data: chiller, label: 'Chiller', borderWith: 2, borderStyle: 'dash' },
      { data: other, label: 'Other', borderWidth: 2, borderStyle: 'dash' }
    ];
    this.barchart.datasets = result;
  }

  getWidgets() {
    let ac: any = {},
      chiller: any = {},
      store: any = {},
      temperature: any = {},
      humidity: any = {};
    // menfilter data dicari yang daily saja
    const result = this.measurements.filter(
      res => res.counter.perfObj.period === 'MIN5'
    );
    // mencari nilai paling terakhir dari data measurement sesuai jenis nya
    // tslint:disable-next-line:forin
    for (const i in result) {
      const set = result[i].counter.discObj.name;
      if (
        result[i].counter.perfObj.name === 'Daya' &&
        result[i].counter.discObj.eqType.type === 'Site'
      ) {
        store[set] = result[i].measurementValue;
      }
      if (
        result[i].counter.perfObj.name === 'Daya' &&
        result[i].counter.discObj.eqType.type === 'AC'
      ) {
        ac[set] = result[i].measurementValue;
      }
      if (
        result[i].counter.perfObj.name === 'Daya' &&
        result[i].counter.discObj.eqType.type === 'Chiller'
      ) {
        chiller[set] = result[i].measurementValue;
      }
      if (
        result[i].counter.perfObj.name === 'Temperatur Ruang' ||
        result[i].counter.perfObj.name === 'Temperatur Ruang'
      ) {
        temperature[set] = result[i].measurementValue;
      }
      if (result[i].counter.perfObj.name === 'Kelembapan Relatif') {
        humidity[set] = result[i].measurementValue;
      }
    }
    // merubah json menjadi array
    ac = Object.keys(ac).map(key => ac[key]);
    chiller = Object.keys(chiller).map(key => chiller[key]);
    store = Object.keys(store).map(key => store[key]);
    temperature = Object.keys(temperature).map(key => temperature[key]);
    humidity = Object.keys(humidity).map(key => humidity[key]);
    // mentotal nilai array daya
    ac = ac.reduce((a, b) => a + b, 0);
    chiller = chiller.reduce((a, b) => a + b, 0);
    store = store.reduce((a, b) => a + b, 0);
    const other = store - (ac + chiller);
    this.widget_total.value = store;
    this.widget_ac.value = ac;
    this.widget_chiller.value = chiller;
    this.widget_other.value = other;
    // conversi nilai satuan
    const widgets = [
      'widget_total',
      'widget_ac',
      'widget_chiller',
      'widget_other'
    ];
    // tslint:disable-next-line:forin
    for (const i in widgets) {
      const convert = this.convert.transform(this[widgets[i]].value, 'W');
      this[widgets[i]].value = convert.converted;
      this[widgets[i]].unit = convert.unit;
    }
    if (temperature.length !== 0) {
      this.widget_temperature.min = this.decimalConvert(
        Math.min.apply(null, temperature)
      );
      this.widget_temperature.max = this.decimalConvert(
        Math.max.apply(null, temperature)
      );
    }
    if (humidity.length !== 0) {
      this.widget_humidity.min = this.decimalConvert(
        Math.min.apply(null, humidity)
      );
      this.widget_humidity.max = this.decimalConvert(
        Math.max.apply(null, humidity)
      );
    }
  }

  decimalConvert(value) {
    return parseFloat(value.toFixed(1));
  }
}
