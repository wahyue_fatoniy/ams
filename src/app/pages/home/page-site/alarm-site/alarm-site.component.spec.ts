import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlarmSiteComponent } from './alarm-site.component';

describe('AlarmComponent', () => {
  let component: AlarmSiteComponent;
  let fixture: ComponentFixture<AlarmSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlarmSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlarmSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
