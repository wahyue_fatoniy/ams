import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormatDatePipe, Table, StorageService, Alarm, Discobject, DialogImageComponent,
  DialogRemoveComponent, AuthService, ErrorHandlerComponent, ClearAlarmPipe
} from '../../../../shared';


@Component({
  selector: 'app-alarm-site',
  templateUrl: './alarm-site.component.html',
  styleUrls: ['./alarm-site.component.css']
})
export class AlarmSiteComponent implements OnInit {

  constructor(public http: HttpClient, public storage: StorageService, public auth: AuthService) {}
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  date: FormatDatePipe = new FormatDatePipe();
  @ViewChild('remove') remove: DialogRemoveComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  @ViewChild('imageView') imageView:DialogImageComponent;
  clear: ClearAlarmPipe = new ClearAlarmPipe();
  table: Table = {
    title : `Table alarm`,
    buttons : ['acknowledge'],
    columns : ['id', 'Type', 'Managed object', 'Severity', 'Message', 'Active Time', 'Acknowledge User', 'Acknowledge Time'],
    loading : false,
    datasets : [],
    width : '1200px'
  };

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.table.loading = true;
      this.http.get<Alarm[]>(`/api/alarms/${this.latitude}/${this.longitude}`)
      .subscribe(res => {
        this.table.loading = false;
        this.table.datasets = res.sort((a, b) => Date.parse(b.actTime) - Date.parse(a.actTime))
        .map(alarm => {
            return {
                id  :  alarm.id,
                Type : alarm.type,
                'Managed object' : alarm.alarmObj.name,
                Severity : alarm.severity,
                Message :  alarm.description,
                'Active Time' : this.date.transform(alarm.actTime, 'DD/MM/YYYY hh:mm:ss'),
                'Acknowledge User' : alarm.ackUser,
                'Acknowledge Time' : alarm.ackTime == null ? alarm.ackTime :
                this.date.transform(alarm.ackTime, 'DD/MM/YYYY hh:mm:ss')
            };
        });
    }, err => this.error.open(err));
  }

  action(event) {
    switch (event.actionType) {
      case 'acknowledge':
        this.remove.open({
            id : event.value.id,
            message : `Are you sure to acknowledge ${event.value.Message}`,
            acknowledge : true
        });
      break;
      case 'imageLink':
        this.imageView.open(event.value);
      break;
    }
  }

  save(event) {
    const name = this.auth.getProfile().nickname;
    this.http.post(`/api/alarms/acknowledge/${event}/${name}/`, {})
    .subscribe(result => this.getData(), err => this.error.open(err));
  }

}
