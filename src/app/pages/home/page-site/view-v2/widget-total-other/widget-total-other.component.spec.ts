import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetTotalOtherComponent } from './widget-total-other.component';

describe('WidgetTotalOtherComponent', () => {
  let component: WidgetTotalOtherComponent;
  let fixture: ComponentFixture<WidgetTotalOtherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetTotalOtherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTotalOtherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
