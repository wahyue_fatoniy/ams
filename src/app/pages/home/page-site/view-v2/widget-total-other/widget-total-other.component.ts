import { Component, OnChanges, OnInit, Input, SimpleChanges } from '@angular/core';
import {  ConvertionValuePipe } from '../../../../../shared';

@Component({
  selector: 'app-widget-total-other',
  templateUrl: './widget-total-other.component.html'
})

export class WidgetTotalOtherComponent implements OnChanges, OnInit {

  constructor() { }
  @Input() total: number;
  @Input() ac: number;
  @Input() chiller: number;
  label = 'Daya Lain-lain';
  unit = 'W';
  value = 0;
  convert: ConvertionValuePipe = new ConvertionValuePipe();

  ngOnInit() {
    this.setOtherValue();
  }

  ngOnChanges(changes: SimpleChanges) {
    let running = true;
    // tslint:disable-next-line:forin
    for (const i in changes) {
      running = !changes[i].firstChange;
    }
    if (running) {
      this.setOtherValue();
    }
  }

  setOtherValue() {
    const total = this.total ? this.total : 0;
    const ac = this.ac ? this.ac : 0;
    const chiller = this.chiller ? this.chiller : 0;
    const other = total - (ac + chiller);
    console.info('[widget total other] hitung other dari nilai total - (ac + chiller),'
      + ' Total=' + total + ', AC=' + ac + ', chiller=' + chiller + ', Other=' + other
    );
    const convert = this.convert.transform(other, 'W');
    this.value = convert.converted;
    this.unit = convert.unit;
  }

}
