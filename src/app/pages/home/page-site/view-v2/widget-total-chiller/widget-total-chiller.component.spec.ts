import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetTotalChillerComponent } from './widget-total-chiller.component';

describe('WidgetTotalChillerComponent', () => {
  let component: WidgetTotalChillerComponent;
  let fixture: ComponentFixture<WidgetTotalChillerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetTotalChillerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTotalChillerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
