import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetRoomTempComponent } from './widget-room-temp.component';

describe('WidgetRoomTempComponent', () => {
  let component: WidgetRoomTempComponent;
  let fixture: ComponentFixture<WidgetRoomTempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetRoomTempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetRoomTempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
