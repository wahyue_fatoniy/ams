import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { StorageService, PerfCounter, PerfMeasurement, SocketService } from '../../../../../shared';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { mergeMap, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { element } from 'protractor';


@Component({
  selector: 'app-widget-room-temp',
  templateUrl: './widget-room-temp.component.html'
})

export class WidgetRoomTempComponent implements OnInit {

  constructor(private storage: StorageService, private http: HttpClient, private socket: SocketService) { }
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  label = 'Temperatur';
  max = 0;
  min = 0;
  date: DatePipe = new DatePipe('id');
  idCounters: number[] = [];
  today = new Date();
  start = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
  end   = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59);
  loading: boolean;
  measurements: PerfMeasurement[] = [];

  ngOnInit() {
    this.socket.streaming('perfmeasurement.data').subscribe((measurement: PerfMeasurement) => {
      if (this.idCounters.findIndex(value => value === measurement.counter.id) !== -1) {
        console.info('[widget room temp] terima data dari kafka', measurement);
        this.measurements.push(measurement);
        this.generateMaxMin(this.measurements);
      }
    });

    this.loading = true;
    this.getPerfcounter().subscribe(counters => {
        this.idCounters = counters.map(counter => counter.id);
        this.getPerfmeasurements(counters).subscribe(measurements => {
            this.loading = false;
            this.measurements = measurements;
            this.generateMaxMin(measurements);
        });
    });
  }


  generateMaxMin(measurements: PerfMeasurement[]) {
    let managedObj = {};
    measurements.forEach(measurement => managedObj[measurement.counter.id] = measurement.measurementValue);
    console.info('[widget room temp] cari nilai terbesar dan terkecil', managedObj);
    const values = Object.keys(managedObj).map(key => managedObj[key]);
    if (values.length !== 0) {
      this.max = Math.max.apply(null, values);
      this.min = Math.min.apply(null, values);
      this.max = parseFloat(this.max.toFixed(1));
      this.min = parseFloat(this.min.toFixed(1));
    }
  }

  getPerfcounter(): Observable<PerfCounter[]> {
    return this.http.get<PerfCounter[]>('/api/perfcounters/' + this.latitude + '/' + this.longitude )
    .pipe(
      map(counters => {
        const result  = counters.filter(counter =>
            (counter.perfObj.name === 'Temperatur Coil' || counter.perfObj.name === 'Temperatur Ruang')
            && counter.perfObj.period === 'MIN5'
        );
        console.info('[widget room temp] ambil data perfcounter, period=MIN5, type=Temperatur coil/Temperatur ruang , EqType=AC');
        console.debug(result);
        return result;
      })
    );
  }

  getPerfmeasurements(counters: PerfCounter[]): Observable<PerfMeasurement[]> {
    const start = this.date.transform(this.start, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    const end = this.date.transform(this.end, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    // tslint:disable-next-line:prefer-const
    let services = [];
    counters.forEach(counter =>
      services.push(
        this.http.get(`/api/perfmeasurements/${counter.id}/${start}/${end}`
      ))
    );
    if (services.length === 0) {
      console.info('[widget room temp] counters tidak ada perfmeasurements callbacl []');
      console.debug([]);
      return of([]);
    } else {
      return Observable.forkJoin(services).pipe(
        map(measurements => {
          const result = [].concat.apply([], measurements);
          console.info(`[widget room temp] ambil data perfmeasurements, ID=`
            + this.idCounters + ', Start time=' + start + ', End time' + end
          );
          console.debug(result);
          return result;
        })
      );
    }
  }

}
