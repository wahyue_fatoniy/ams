import { Component, OnInit } from '@angular/core';
import {
  Discobject,
  StorageService,
  Building,
} from '../../../../../shared';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { pipe } from 'rxjs/util/pipe';
import { map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { forkJoin } from 'rxjs/observable/forkJoin';

export interface ManagedObject {
  id ?: number;
  discobj_id: number;
  site_id: number;
  top: number;
  left: number;
  discobject?: Discobject;
}

interface DefaultPosition  {
  PL1BT?: ManagedObject[];
  AC?: ManagedObject[];
  Chiller?: ManagedObject[];
  Pompa?: ManagedObject[];
}

interface LoadingItem {
  PL1BT?: boolean;
  AC?: boolean;
  Chiller?: boolean;
  Pompa?: boolean;
}

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls : ['./area.component.css']
})
export class AreaComponent implements OnInit {

  constructor(private http: HttpClient, private storage: StorageService) {}
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  idSite: number = this.storage.site.id;
  loading: boolean;
  loadingItems: LoadingItem = {};
  viewType = 'default';
  width: number;
  defaultItemPositions: DefaultPosition = {};
  changeItemPositions: DefaultPosition = {};
  items: DefaultPosition = {};

  ngOnInit() {
    this.loading = true;
    this.getManagedObjects().subscribe(managedObjs => {
        managedObjs.forEach(managedObj => {
          const key = managedObj.discobject.eqType.type;
          this.loadingItems[key] = true;
          this.items[key] = this.items[key] ? [ ...this.items[key], managedObj ] : [managedObj];
        });
        this.loading = false;
        const width = document.getElementById('container').offsetWidth;
        this.width = width;
        this.defaultItemPositions  = JSON.parse(JSON.stringify(this.items));
    });
  }


  onCancel() {
    this.items = JSON.parse(JSON.stringify(this.defaultItemPositions));
  }

  onSave() {
    let items = Object.keys(this.changeItemPositions).map(key => this.changeItemPositions[key]);
    items = [].concat.apply([], items);
    this.saveItems(items)
    .subscribe(item => console.info('[area] update position Managed Object', items));
    // tslint:disable-next-line:forin
    for (let key in this.changeItemPositions) {
      this.changeItemPositions[key].forEach(managedObj => {
          const index = this.defaultItemPositions[key].findIndex(item => item.id === managedObj.id);
          if (index !== -1) {
              this.defaultItemPositions[key][index] = JSON.parse(JSON.stringify(managedObj));
          }
      });
    }
  }


  saveItems(items: ManagedObject[]) {
    let services = [];
    items.forEach(item =>
      services = [this.http.put(`/api/managed_objects/${item.id}`, item), ...services ]
    );
    return Observable.forkJoin(services);
  }

  getManagedObjects(): Observable<ManagedObject[]> {
    const filter = encodeURIComponent(JSON.stringify({where: {site_id: this.idSite}}));
    return Observable.forkJoin([
      this.http.get<ManagedObject[]>(`/api/managed_objects?filter=` + filter),
      this.http.get<Discobject[]>(`/api/discobjects/${this.latitude}/${this.longitude}/`)
    ]).pipe(
        map(result => {
            result[0].forEach(managedObj => {
                const index = result[1].findIndex(discobj => discobj.id === managedObj.discobj_id);
                managedObj.discobject = result[1][index];
            });
            console.info(`[area] ambil data managed object, latitude=${this.latitude}, longitude=${this.longitude}`);
            console.debug(result[0]);
            return result[0];
        })
    );
  }


  convertLeft(value: number) {
    const width = document.getElementById('container').offsetWidth;
    value = (value * width) / 100;
    return value;
  }

  test(event) {
    console.log(event);
  }

  getLoading(): boolean {
    if (!this.loading && !this.loadingItems.PL1BT ) {
      return false;
    } else {
      return true;
    }
  }

}
