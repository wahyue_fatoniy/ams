import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertLeft'
})
export class ConvertLeftPipe implements PipeTransform {

  transform(value: number, width: number, typeConvert: string): number {
    switch (typeConvert) {
      case 'px':
          value  = (width / 100) * value;
      break;
      case '%':
          value = (value / width) * 100;
      break;
    }
    return parseFloat(value.toFixed(1));
  }

}
