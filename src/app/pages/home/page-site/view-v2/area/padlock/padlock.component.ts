import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ManagedObject } from '../area.component';
import { ConvertLeftPipe } from '../convert-left.pipe';
import { PerfMeasurement, PerfCounter, StorageService, SocketService, Alarm } from '../../../../../../shared';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { pipe } from 'rxjs/util/pipe';



@Component({
  selector: 'app-padlock',
  templateUrl: './padlock.component.html'
})
export class PadlockComponent implements OnInit {

  constructor(private http: HttpClient, private storage: StorageService, private socket: SocketService) {}
  @Input() managedObjects: ManagedObject[];
  @Input() width: number;
  @Input() draggabled: boolean;
  @Output() statusLoading: EventEmitter<any> = new EventEmitter<any>();
  changePositions: ManagedObject[] = [];
  convert: ConvertLeftPipe = new ConvertLeftPipe();
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  today = new Date();
  start = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
  end   = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59);
  date = new DatePipe('id');
  loading: boolean;
  severities = {};
  idManagedObjects: number[] = this.managedObjects ? this.managedObjects.map(managedObj => managedObj.discobj_id) : [];
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChangePosition: EventEmitter<ManagedObject[]> = new EventEmitter<ManagedObject[]>();

  ngOnInit() {
    this.loading = true;
    this.getAlarms().subscribe(alarms => {
      this.loading = false;
      alarms.forEach(alarm => this.severities[alarm.alarmObj.id] = alarm.severity.toLowerCase());
    });
  }

  getSeverity(id): string {
    return this.severities[id] ? this.severities[id] : 'clear';
  }

  getAlarms(): Observable<Alarm[]> {
    return this.http.get<Alarm[]>(`/api/alarms/${this.latitude}/${this.longitude}/`)
    .pipe(
      map(alarms => {
        alarms = alarms.filter(alarm =>
          this.managedObjects.findIndex(managed => managed.discobj_id === alarm.alarmObj.id) !== -1
        );
        this.statusLoading.emit(false);
        console.info('[padlock] ambil data alarms');
        console.debug(alarms);
        return alarms;
      })
    );
  }

  onDrop(padlock: ManagedObject) {
    let left = document.getElementById(padlock.id.toString()).offsetLeft;
    const top = document.getElementById(padlock.id.toString()).offsetTop;
    left = this.convert.transform(left, this.width, '%');
    padlock.left = left;
    padlock.top = top;
    const index = this.changePositions.findIndex(position => position.id === padlock.id);
    if (index === -1) {
      this.changePositions = [padlock, ...this.changePositions];
    } else  {
      this.changePositions[ index ] = padlock;
    }
    this.onChangePosition.emit(this.changePositions);
  }

}
