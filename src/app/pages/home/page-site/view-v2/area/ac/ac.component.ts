import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ManagedObject } from '../area.component';
import { ConvertLeftPipe } from '../convert-left.pipe';
import { PerfMeasurement, PerfCounter, StorageService, SocketService, Alarm } from '../../../../../../shared';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { pipe } from 'rxjs/util/pipe';


@Component({
  selector: 'app-ac',
  templateUrl: './ac.component.html'
})
export class AcComponent implements OnInit {


  constructor(private http: HttpClient, private storage: StorageService, private socket: SocketService) {}
  @Input() managedObjects: ManagedObject[];
  @Input() width: number;
  @Input() draggabled: boolean;
  changePositions: ManagedObject[] = [];
  convert: ConvertLeftPipe = new ConvertLeftPipe();
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  today = new Date();
  start = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
  end   = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59);
  date = new DatePipe('id');
  loading: boolean;
  severities = {};
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChangePosition: EventEmitter<ManagedObject[]> = new EventEmitter<ManagedObject[]>();

  ngOnInit() {
  }



}
