import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomTempHumidityComponent } from './room-temp-humidity.component';

describe('RoomTempHumidityComponent', () => {
  let component: RoomTempHumidityComponent;
  let fixture: ComponentFixture<RoomTempHumidityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomTempHumidityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomTempHumidityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
