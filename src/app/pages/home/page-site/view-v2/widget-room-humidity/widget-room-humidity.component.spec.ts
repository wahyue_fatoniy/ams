import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetRoomHumidityComponent } from './widget-room-humidity.component';

describe('WidgetRoomHumidityComponent', () => {
  let component: WidgetRoomHumidityComponent;
  let fixture: ComponentFixture<WidgetRoomHumidityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetRoomHumidityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetRoomHumidityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
