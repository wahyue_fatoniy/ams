import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetTotalEnergyComponent } from './widget-total-energy.component';

describe('WidgetTotalEnergyComponent', () => {
  let component: WidgetTotalEnergyComponent;
  let fixture: ComponentFixture<WidgetTotalEnergyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetTotalEnergyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTotalEnergyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
