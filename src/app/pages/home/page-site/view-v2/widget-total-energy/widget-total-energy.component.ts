import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { of } from 'rxjs/observable/of';
import { StorageService, PerfCounter, PerfMeasurement, ConvertionValuePipe, SocketService } from '../../../../../shared';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { mergeMap, map, filter, flatMap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-widget-total-energy',
  templateUrl: './widget-total-energy.component.html'
})
export class WidgetTotalEnergyComponent implements OnInit {

  constructor(private storage: StorageService, private http: HttpClient, private socket: SocketService) { }
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  convert: ConvertionValuePipe = new ConvertionValuePipe();
  label = 'Daya Site';
  value = 0;
  unit  = 'W';
  date: DatePipe  = new DatePipe('id');
  idCounter: number;
  today = new Date();
  start = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
  end   = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59);
  loading: boolean;
  @Output() setValue: EventEmitter<number> = new EventEmitter<number>();

  ngOnInit() {
    this.socket.streaming('perfmeasurement.data')
    .subscribe((measurement: PerfMeasurement) => {
        if (this.idCounter === measurement.counter.id) {
          console.info('[widget total energy] terima data dari kafka', measurement);
          this.convertValue(measurement);
        }
    });

    this.loading = true;
    this.getPerfcounter().subscribe(counters => {
        this.idCounter = counters.length === 0 ? undefined : counters[0].id;
        this.getPerfmeasurements(counters).subscribe(measurements => {
            this.loading = false;
            this.convertValue(measurements.length === 0 ? undefined : measurements[ measurements.length - 1 ]);
        });
    });
  }

  getPerfcounter(): Observable<PerfCounter[]> {
    return this.http.get<PerfCounter[]>('/api/perfcounters/' + this.latitude + '/' + this.longitude )
    .pipe(
      map(counters => {
        const result  = counters.filter(counter =>
            counter.perfObj.name === 'Daya'
            && counter.perfObj.period === 'MIN5'
            && counter.discObj.eqType.type === 'Site'
        );
        console.info('[widget total energy] ambil data perfcounter, period=MIN5, type=Daya, eqType=Site');
        console.debug(result);
        return result;
      })
    );
  }

  getPerfmeasurements(counters: PerfCounter[]): Observable<PerfMeasurement[]> {
    const start = this.date.transform(this.start, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    const end = this.date.transform(this.end, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    // tslint:disable-next-line:prefer-const
    let services = [];
    counters.forEach(element =>
      services.push(
        this.http.get(`/api/perfmeasurements/${element.id}/${start}/${end}`
      ))
    );
    if (services.length === 0) {
      console.info('[widget total energy] counters tidak ada perfmeasurements callback []');
      console.debug([]);
      return of([]);
    } else {
      return Observable.forkJoin(services).pipe(
        map(measurements => {
          const result = [].concat.apply([], measurements);
          console.info(`[widget total energy] ambil data perfmeasurements, ID=`
            + this.idCounter + ', Start time=' + start + ', End time' + end
          );
          console.debug(result);
          return result;
        })
      );
    }
  }

  convertValue(measurement?: PerfMeasurement) {
      if (measurement) {
        const convert = this.convert.transform(measurement.measurementValue, 'W');
        this.value = convert.converted;
        this.unit = convert.unit;
        this.setValue.emit(convert.value);
      }
  }
}
