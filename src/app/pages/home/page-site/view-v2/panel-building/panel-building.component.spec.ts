import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelBuildingComponent } from './panel-building.component';

describe('PanelBuildingComponent', () => {
  let component: PanelBuildingComponent;
  let fixture: ComponentFixture<PanelBuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelBuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelBuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
