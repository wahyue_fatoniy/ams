import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetTotalAcComponent } from './widget-total-ac.component';

describe('WidgetTotalAcComponent', () => {
  let component: WidgetTotalAcComponent;
  let fixture: ComponentFixture<WidgetTotalAcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetTotalAcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTotalAcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
