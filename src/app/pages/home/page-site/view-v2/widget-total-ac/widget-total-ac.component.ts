import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { StorageService, PerfCounter, PerfMeasurement, ConvertionValuePipe, SocketService } from '../../../../../shared';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { mergeMap, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-widget-total-ac',
  templateUrl: './widget-total-ac.component.html',
  styles: []
})
export class WidgetTotalAcComponent implements OnInit {

  constructor(private storage: StorageService, private http: HttpClient, private socket: SocketService) { }
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  convert: ConvertionValuePipe = new ConvertionValuePipe();
  label = 'Daya AC';
  value = 0;
  unit  = 'W';
  date: DatePipe = new DatePipe('id');
  idCounters: number[] = [];
  today = new Date();
  start = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
  end   = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59);
  loading: boolean;
  measurements: PerfMeasurement[];
  @Output() setValue: EventEmitter<number> = new EventEmitter<number>();

  ngOnInit() {
    this.socket.streaming('perfmeasurement.data')
    .subscribe((measurement: PerfMeasurement) => {
        if (this.idCounters.findIndex(value => value === measurement.counter.id) !== -1) {
          console.info('[widget total ac] terima data dari kafka', measurement);
          this.measurements.push(measurement);
          this.onSetValue(this.measurements);
        }
    });

    this.loading = true;
    this.getPerfcounter().subscribe(counters => {
      this.idCounters = counters.map(counter => counter.id);
      this.getPerfmeasurements(counters)
      .subscribe(measurements => {
          this.loading = false;
          this.measurements = measurements;
          this.onSetValue(measurements);
      });
    });
  }

  onSetValue(measurements: PerfMeasurement[]) {
    let managedObj = {};
    measurements.forEach(element =>
      managedObj[ element.counter.id ] = element.measurementValue);
    console.info('[widget total ac] total semua nilai terakhir dari managed object ac', managedObj);
    const values = Object.keys(managedObj).map(key => managedObj[key] );
    const total = values.reduce((a, b) => a + b, 0);
    const convert = this.convert.transform(total, 'W');
    this.value = convert.converted;
    this.unit = convert.unit;
    this.setValue.emit(convert.value);
  }

  getPerfcounter(): Observable<PerfCounter[]> {
    return this.http.get<PerfCounter[]>('/api/perfcounters/' + this.latitude + '/' + this.longitude )
    .pipe(
      map(counters => {
        const result  = counters.filter(counter =>
            counter.perfObj.name === 'Daya'
            && counter.perfObj.period === 'MIN5'
            && counter.discObj.eqType.type === 'AC'
        );
        console.info('[widget total ac] ambil data perfcounter, period=MIN5, type=Daya, EqType=AC');
        console.debug(result);
        return result;
      })
    );
  }

  getPerfmeasurements(counters: PerfCounter[]): Observable<PerfMeasurement[]> {
    const start = this.date.transform(this.start, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    const end = this.date.transform(this.end, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    // tslint:disable-next-line:prefer-const
    let services = [];
    counters.forEach(element =>
      services.push(
        this.http.get(`/api/perfmeasurements/${element.id}/${start}/${end}`
      ))
    );
    if (services.length === 0) {
      console.info('[widget total ac] counters tidak ada perfmeasurements callback []');
      console.debug([]);
      return of([]);
    } else {
      return Observable.forkJoin(services).pipe(
        map(measurements => {
          const result = [].concat.apply([], measurements);
          console.info(`[widget total ac] ambil data perfmeasurements, ID=`
            + this.idCounters + ', Start time=' + start + ', End time' + end
          );
          console.debug(result);
          return result;
        })
      );
    }
  }

}
