import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartEnergyComponent } from './chart-energy.component';

describe('ChartEnergyComponent', () => {
  let component: ChartEnergyComponent;
  let fixture: ComponentFixture<ChartEnergyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartEnergyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartEnergyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
