import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { StorageService, SocketService, PerfCounter, PerfMeasurement } from '../../../../../shared';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, mergeMap } from 'rxjs/operators';
import { pipe } from 'rxjs/util/pipe';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { of } from 'rxjs/observable/of';
import { OPTIONS } from './option.config';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';

interface ChartType {
  value: string;
  label: string;
}

interface Dataset {
  data: number[];
  label: string; // AC | Chiller | Other
  borderWidth: number; // 2
  borderStyle: string; // "dash"
}

@Component({
  selector: 'app-chart-energy',
  templateUrl: './chart-energy.component.html'
})

export class ChartEnergyComponent implements OnInit {

  constructor(private storage: StorageService, private http: HttpClient, private socket: SocketService) { }
  timeType = localStorage.getItem('barchartType') ? localStorage.getItem('barchartType') : 'MIN5';
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  date: DatePipe = new DatePipe('id');
  idCounters: number[] = [];
  today = new Date();
  start = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
  end   = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59);
  loading: boolean;
  measurements: PerfMeasurement[] = [];
  counters: PerfCounter[] = [];
  labels: string[] = [];
  datasets: Dataset[] = [
    { data: [], label: 'Other', borderWidth: 2, borderStyle: 'dash' },
    { data: [], label: 'AC', borderWidth: 2, borderStyle: 'dash' },
    { data: [], label: 'Chiller', borderWidth: 2, borderStyle: 'dash'}
  ];
  options = OPTIONS;
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  timeTypes: ChartType[] = [
    {value: 'MIN5', label: 'Daily'},
    {value: 'HOUR1', label: 'Weekly'},
    {value: 'HOUR6', label: 'Monthly'}
  ];
  formatDate: string;

  ngOnInit() {
    this.socket.streaming('perfmeasurement.data').subscribe((measurement: PerfMeasurement) => {
        if (this.idCounters.findIndex(counter => counter === measurement.counter.id) !== -1) {
          console.info('[chart energy] terima data dari kafka', measurement);
          this.measurements.push(measurement);
          this.setChart(false);
        }
    });
    this.renderChart();
  }

  renderChart() {
    localStorage.setItem('barchartType', this.timeType);
    this.loading = true;
    this.getPerfcounters().subscribe(counters => {
        this.idCounters = counters.map(counter => counter.id);
        this.counters = counters;
        this.getPerfmeasurements(counters).subscribe(measurements => {
          this.loading = false;
          this.measurements = measurements;
          this.setChart(true);
        });
    });
  }

  setLabels() {
    this.labels = [];
    let weekly, today, monthly;
    switch (this.timeType) {
      case 'MIN5':
        this.formatDate = 'longDate';
        this.labels = [this.date.transform(this.today, this.formatDate)];
      break;
      case 'HOUR1':
        this.formatDate = 'mediumDate';
        for (let i = 0; i < 7; i++) {
          today = new Date();
          weekly = this.date.transform(today.setDate(today.getDate() - i), this.formatDate);
          this.labels = [weekly, ...this.labels];
        }
      break;
      case 'HOUR6':
        this.formatDate = 'MMMM';
        for (let i = 0; i < 3; i++) {
          today = new Date();
          monthly = this.date.transform(today.setMonth(today.getMonth() - i), this.formatDate);
          this.labels = [monthly, ...this.labels];
        }
      break;
    }
  }

  setChart(updateChart?: boolean) {
    this.setLabels();
    let site = {}, ac = {}, chiller = {}, other = {};
    for (let i = 0; i < this.labels.length; i++) {
      site[ this.labels[i] ] = 0;
      ac[ this.labels[i] ] = 0;
      chiller[ this.labels[i] ] = 0;
      other[ this.labels[i] ] = 0;
    }
    this.measurements.forEach(measurement => {
        const index = this.counters.findIndex(counter => measurement.counter.id === counter.id);
        measurement.counter = this.counters[index];
        const label = this.date.transform(measurement.measurementTime, this.formatDate);
        switch (measurement.counter.discObj.eqType.type) {
          case 'Site': site[ label ] += measurement.measurementValue;
              break;
          case 'AC': ac[ label ] += measurement.measurementValue;
              break;
          case 'Chiller': chiller[ label ] += measurement.measurementValue;
              break;
        }
    });
    this.labels.forEach(label => other[label] = site[label] - (ac[label] - chiller[label]) );
    console.info('[chart energy] total energi Site', site);
    console.info('[chart energy] total energi AC', ac);
    console.info('[chart energy] total energi Chiller', chiller);
    console.info('[chart energi] total energi other', other);
    this.datasets[0].data = Object.keys(other).map(key => other[key]);
    this.datasets[1].data = Object.keys(ac).map(key => ac[key]);
    this.datasets[2].data = Object.keys(chiller).map(key => chiller[key]);
    if (updateChart) {
      if (this.chart && this.chart.chart && this.chart.chart.config) {
        this.chart.chart.config.data.labels = this.labels;
        this.chart.chart.update();
      }
    }
  }



  getPerfcounters(): Observable<PerfCounter[]> {
    return this.http.get<PerfCounter[]>('/api/perfcounters/' + this.latitude + '/' + this.longitude )
    .pipe(
      map(counters => {
        const result  = counters.filter(counter =>
            counter.perfObj.name === 'Energi'
            && counter.perfObj.period === this.timeType
        );
        console.info(`[chart energy] ambil data perfcounter, period=${this.timeType}, type=Energi`);
        console.debug(result);
        return result;
      })
    );
  }

  getPerfmeasurements(counters: PerfCounter[]): Observable<PerfMeasurement[]> {
    this.start = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
    if (this.timeType === 'HOUR1') {
      this.start.setDate(this.start.getDate() - 6);
    } else if (this.timeType === 'HOUR6') {
      this.start.setDate(1);
      this.start.setMonth(this.start.getMonth() - 2);
    }
    const start = this.date.transform(this.start, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    const end = this.date.transform(this.end, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    // tslint:disable-next-line:prefer-const
    let services = [];
    counters.forEach(counter =>
      services.push(
        this.http.get(`/api/perfmeasurements/${counter.id}/${start}/${end}`
      ))
    );
    if (services.length === 0) {
      console.info('[chart energy] counters tidak ada perfmeasurements callbacl []');
      console.debug([]);
      return of([]);
    } else {
      return Observable.forkJoin(services).pipe(
        map(measurements => {
          const result = [].concat.apply([], measurements);
          console.info(`[chart energy] ambil data perfmeasurements, ID=`
            + this.idCounters + ', Start time=' + start + ', End time=' + end
          );
          console.debug(result);
          return result;
        })
      );
    }
  }

}
