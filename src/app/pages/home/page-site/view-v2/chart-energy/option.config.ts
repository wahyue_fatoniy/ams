import { ConvertionValuePipe } from '../../../../../shared';

export let OPTIONS: any = {
    animation: { duration: 1000 },
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    legend: {
        onHover: function (e) {
            e.target.style.cursor = 'pointer';
        }
    },
    tooltips: {
        mode: 'index',
        callbacks: {
            // Use the footer callback to display the sum of the items showing in the tooltip
            footer: function (tooltipItems, data) {
                let sum = 0;
                tooltipItems.forEach(function (tooltipItem) {
                    sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                });
                return 'Total : ' + sum;
            },
        },
        footerFontStyle: 'normal'
    },
    hover: {
        mode: 'index',
        intersect: true
    },
    scales: {
        yAxes: [{
            stacked: true,
            ticks: {
                beginAtZero: true,
                userCallback: function (value, index, values) {
                    value = new ConvertionValuePipe().transform(value, 'WH');
                    return `${value.converted} ${value.unit}`;
                }
            }
        }],
        xAxes: [{
            stacked: true,
            ticks: {
                beginAtZero: true
            }
        }]
    }
};
