import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import {
  StorageService,
  SocketService,
  PerfCounter,
  PerfMeasurement,
  ConvertionValuePipe,
  InfoHighchartPipe
 } from '../../../../../shared';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, mergeMap } from 'rxjs/operators';
import { pipe } from 'rxjs/util/pipe';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { of } from 'rxjs/observable/of';
import { Chart } from 'angular-highcharts';

interface Type {
  value: string;
  label: string;
}

interface DataHighchart {
  x: number;
  y: number;
  info: string;
}

interface SeriesHighchart {
    idCounter: number;
    name: string;
    yAxis: number;
    data: DataHighchart[];
    cursor: string; // pointer
    marker: {enabled: boolean; symbol: string}; // marker :{symbol:`url(link.png)`}
    type: string; // line atau scatter
    showInLegend: boolean;
}

@Component({
  selector: 'app-chart-periodic',
  templateUrl: './chart-periodic.component.html',
  styles: []
})
export class ChartPeriodicComponent implements OnInit {

  constructor(private storage: StorageService, private http: HttpClient, private socket: SocketService) { }
  chartType = localStorage.getItem('linechartType') ? localStorage.getItem('linechartType') : 'Daya';
  timeType = localStorage.getItem('linechartTime') ? localStorage.getItem('linechartTime') : 'MIN5';
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  convert: ConvertionValuePipe = new ConvertionValuePipe();
  date: DatePipe = new DatePipe('id');
  idCounters: number[] = [];
  today = new Date();
  start = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
  end   = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59);
  loading: boolean;
  measurements: PerfMeasurement[] = [];
  counters: PerfCounter[] = [];
  timeTypes: Type[] = [
    {value: 'MIN5', label: 'Daily'},
    {value: 'HOUR1', label: 'Weekly'},
    {value: 'HOUR6', label: 'Monthly'}
  ];
  chartTypes: Type[] = [
    {value: 'Daya', label: 'Power'},
    {value: 'Temperatur', label: 'Temperature'},
    {value: 'Kelembapan Relatif', label: 'Humidity'}
  ];
  series: SeriesHighchart[] = [];
  options = {
      title   : {text: '', style: {display: 'none'}},
      credits : {enabled: false},
      yAxis   : [
        {title: {text: null}, labels: {formatter: function() {
          const convert = new ConvertionValuePipe().transform(this.value, 'W');
          return `${convert.converted} ${convert.unit}`;
        }}},
        {title: {text: null}, labels: {format: '{value}°C'}},
        {title: {text: null}, labels: {format: '{value} %'}}
      ],
      tooltip : {formatter: function () {return this.point.info; }, useHTML: true},
      xAxis   : {type: 'datetime', labels: {overflow: 'justify'}},
      series  : []
  };
  chart;


  ngOnInit() {
    this.socket.streaming('perfmeasurement.data').subscribe((measurement: PerfMeasurement) => {
      if ( this.idCounters.findIndex(counter => counter === measurement.counter.id) !== -1 ) {
        console.info('[chart periodic] terima data dari kafka', measurement);
        const indexCounter = this.counters.findIndex(counter => measurement.counter.id === counter.id);
        measurement.counter = this.counters[indexCounter];
        const date = new Date(measurement.measurementTime);
        date.setHours(date.getHours() + 7);
        const value: DataHighchart = {
            x : Date.parse(date.toString()),
            y : measurement.measurementValue,
            info : this.infoHighchart(measurement)
        };
        const indexSeries = this.series.findIndex(result => result.idCounter === measurement.counter.id);
        this.chart.addPoint(value, indexSeries);
      }
    });
    this.renderChart();
  }

  renderChart() {
    localStorage.setItem('linechartType', this.chartType);
    localStorage.setItem('linechartTime', this.timeType);
    this.loading = true;
    this.getPerfcounters().subscribe(counters => {
        this.idCounters = counters.map(counter => counter.id);
        this.counters = counters;
        this.getPerfmeasurements(counters).subscribe(measurements => {
            this.loading = false;
            this.measurements = measurements;
            this.onSetChart();
        });
    });
  }

  infoHighchart(measurement: PerfMeasurement) {
    const units = {'Temperatur Ruang': '°C', 'Temperatur Coil': '°C', 'Daya': 'W', 'Kelembapan Relatif': '%'};
      let unit  = units[measurement.counter.perfObj.name];
      if (unit === 'W') {
          const convert = this.convert.transform(measurement.measurementValue, unit);
          unit  = convert.unit;
          measurement.measurementValue = convert.converted;
      }
      return `
        <strong>Time</strong> : ${this.date.transform(measurement.measurementTime, 'medium')}</br>
        <strong>Type</strong> : ${measurement.counter.discObj.eqType.type} </br>
        <strong>Value</strong> : ${measurement.measurementValue} ${unit} </br>
        <strong>Managed Object </strong> : ${measurement.counter.discObj.name}
      `;
  }

  onSetChart() {
    this.series = this.counters.map(counter => {
       let datasets = [];
       this.measurements.forEach(measurement => {
          if (measurement.counter.id === counter.id) {
            const date = new Date(measurement.measurementTime);
            date.setHours(date.getHours() + 7);
            const value: DataHighchart = {
                x : Date.parse(date.toString()),
                y : measurement.measurementValue,
                info : this.infoHighchart(measurement)
            };
            datasets = [value, ...datasets];
          }
       });
       const series = {
          idCounter : counter.id,
          name : counter.discObj.name,
          showInLegend : true,
          data : datasets,
          yAxis : ['Daya', 'Temperatur', 'Kelembapan Relatif'].findIndex(value => value === this.chartType),
          cursor : 'pointer',
          type : 'line',
          marker: {
            enabled: datasets.length <= 15 ? true : false,
            symbol: `url(./assets/img/iconHighChart/${counter.discObj.eqType.type}.png)`
          },
        };
        return series;
    });
    this.options.series = this.series;
    this.chart = new Chart(this.options);
    console.info('[chart periodic] set data chart', this.series);
  }

  getPerfcounters(): Observable<PerfCounter[]> {
    return this.http.get<PerfCounter[]>('/api/perfcounters/' + this.latitude + '/' + this.longitude )
    .pipe(
      map(counters => {
        const result  = counters.filter(counter => {
          if (
            this.chartType === 'Temperatur' &&
            (counter.perfObj.name === 'Temperatur Ruang' || counter.perfObj.name === 'Temperatur Coil')
            && counter.perfObj.period === this.timeType
          ) {
            return true;
          } else if (
            counter.perfObj.name === this.chartType &&
            counter.perfObj.period === this.timeType
          ) {
            return true;
          } else  {
            return false;
          }
        });
        console.info(`[chart periodic] ambil data perfcounter, period=${this.timeType}, type=${this.chartType}`);
        console.debug(result);
        return result;
      })
    );
  }

  getPerfmeasurements(counters: PerfCounter[]): Observable<PerfMeasurement[]> {
    this.start = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
    if (this.timeType === 'HOUR1') {
      this.start.setDate(this.start.getDate() - 6);
    } else if (this.timeType === 'HOUR6') {
      this.start.setDate(1);
      this.start.setMonth(this.start.getMonth() - 2);
    }
    const start = this.date.transform(this.start, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    const end = this.date.transform(this.end, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    // tslint:disable-next-line:prefer-const
    let services = [];
    counters.forEach(counter =>
      services.push(
        this.http.get(`/api/perfmeasurements/${counter.id}/${start}/${end}`
      ))
    );
    if (services.length === 0) {
      console.info('[chart periodic] counters tidak ada perfmeasurements callbacl []');
      console.debug([]);
      return of([]);
    } else {
      return Observable.forkJoin(services).pipe(
        map(measurements => {
          const result = [].concat.apply([], measurements);
          console.info(`[chart periodic] ambil data perfmeasurements, ID=`
            + this.idCounters + ', Start time=' + start + ', End time=' + end
          );
          console.debug(result);
          return result;
        })
      );
    }
  }

}
