import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartPeriodicComponent } from './chart-periodic.component';

describe('ChartPeriodicComponent', () => {
  let component: ChartPeriodicComponent;
  let fixture: ComponentFixture<ChartPeriodicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartPeriodicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartPeriodicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
