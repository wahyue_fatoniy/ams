import { Component, OnInit, ViewChild } from '@angular/core';
import { Table, StorageService } from '../../../../shared';

@Component({
  selector: 'app-provisioning-history',
  templateUrl: './provisioning-history.component.html',
  styleUrls: ['./provisioning-history.component.css']
})
export class ProvisioningHistoryComponent implements OnInit {

  constructor(public storage:StorageService){}
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  table:Table = {
    title:`Table remote control history`, 
    datasets:[], 
    loading:false,
    datepicker : true,
    columns :['id',"User","Managed Object","Location", "Command","Date","Status"]
  };
  
  ngOnInit() {
    this.setDatasets(); 
  }

  action(event){

  }

  setDatasets(){

  }

}
