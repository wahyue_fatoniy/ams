import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvisioningHistoryComponent } from './provisioning-history.component';

describe('ProvisioningHistoryComponent', () => {
  let component: ProvisioningHistoryComponent;
  let fixture: ComponentFixture<ProvisioningHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvisioningHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvisioningHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
