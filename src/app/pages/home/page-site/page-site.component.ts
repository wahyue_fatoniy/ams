import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-page-site',
  templateUrl: './page-site.component.html',
  styleUrls: ['./page-site.component.css']
})
export class PageSiteComponent implements OnInit {
  constructor(public location: Location) {}

  navs: any = [
    {path: 'view', label: 'Site View', class: ''},
    {path: '', label: 'Inventory', class: 'dropdown', children: [
        {path: 'managed-object', label: 'Managed Object'},
        {path: 'perf-counter', label: 'Performance Counter'}
    ]},
    {path: 'mapping-sensor', label: 'Mapping Sensor'},
    {path: 'report', label: 'Report'},
    {path: '', label: 'Alarm', class: 'dropdown', children: [
        {path: 'alarm-site', label: 'Alarm Active'},
        {path: 'history', label: 'Alarm History'}
    ]},
    {path: '', label: 'Setting', class: 'dropdown', children: [
        {path: 'provisioning', label: 'Remote Control'},
        {path: 'provisioning-history', label: 'Remote Control History'}
    ]}
  ];



  ngOnInit() {
    this.active();
  }

  active() {
    const path   = this.location.path();
    const paths = path.split('/');
    this.navs = this.navs.map(res => {
        if (res.children) {
           const index = res.children.findIndex(child => child.path === paths[3]);
           res.class = index !== -1 ? 'dropdown active' : 'dropdown';
        } else {
           res.class = res.path === paths[3] ? 'active' : '';
        }
        return res;
    });
  }

}
