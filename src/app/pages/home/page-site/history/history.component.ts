import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import {
  Table,
  ErrorHandlerComponent,
  StorageService,
  FormatDatePipe,
  DialogRemoveComponent,
  Alarm,
  DialogImageComponent
} from '../../../../shared';
import { Observable } from 'rxjs/Observable';
import { map, filter } from 'rxjs/operators';
import 'rxjs/util/pipe';

export interface AlarmData {
  id: string;
 Type: string;
 'Managed Object': string;
 Severity: string;
 Message: string;
 'Active Time': string;
 'Clear Time': string;
 'Acknowledge User': string;
 'Acknowledge Time': string;
}

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  constructor(public http: HttpClient, public storage: StorageService) {}
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  date: DatePipe = new DatePipe('id');
  staticDate: string;
  @ViewChild('error') error: ErrorHandlerComponent;
  @ViewChild('imageView') imageView: DialogImageComponent;
  table: Table = {
    title: 'Table history alarm',
    buttons: [],
    columns: [
      'id',
      'Type',
      'Managed object',
      'Severity',
      'Message',
      'Active Time',
      'Clear Time',
      'Acknowledge User',
      'Acknowledge Time'
    ],
    loading: false,
    datasets: [],
    width: '1300px',
    datepicker: true
  };

  ngOnInit() {
    this.setDatasets(new Date().toISOString(), true);
  }

  getData(date: string, startDate?: string): Observable<AlarmData[]> {
    const d = new Date(date);
    let start;
    if (startDate !== undefined) {
      start = new Date(startDate);
      start.setHours(0);
      start.setMinutes(0);
      start.setSeconds(0);
    }
    d.setHours(23);
    d.setMinutes(59);
    d.setSeconds(59);
    const endTime = this.date.transform(d, 'yyyy-MM-ddTHH:mm:ss', '+0000');
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);
    const startTime = start ? this.date.transform(start, 'yyyy-MM-ddTHH:mm:ss', '+0000')
    : this.date.transform(d, 'yyyy-MM-ddTHH:mm:ss', '+0000');

    const urlLink =  `/api/alarms/log/${startTime}Z/${endTime}Z/${this.latitude}/${this.longitude}/`;
    const formatDate = 'dd/MM/yyyy HH:mm:ss';
    return this.http.get(urlLink)
    .pipe(
      map((res: any) => {
          return res.sort((a, b) => Date.parse(b.actTime) - Date.parse(a.actTime))
          .map(alarm => {
              return {
                id: alarm.id,
                Type: alarm.type,
                'Managed object': alarm.alarmObj.name,
                Severity: alarm.severity,
                Message: alarm.description,
                'Active Time': this.date.transform(alarm.actTime, formatDate),
                'Clear Time': alarm.clearTime == null ? '' : this.date.transform(alarm.clearTime, formatDate),
                'Acknowledge User': alarm.ackUser,
                'Acknowledge Time': alarm.ackTime == null ? '' : this.date.transform(alarm.ackTime, formatDate)
              };
          });
      })
    );
  }

  setDatasets(date: string, firstCall?: boolean) {
    let dateStart;
    if (firstCall === true) {
      const myDate = new Date(date);
      myDate.setDate(myDate.getDate() - 90);
      dateStart = this.date.transform(myDate, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
    }
    const staticDate = dateStart ? date + dateStart : date;
    if ( this.staticDate !== staticDate) {
      this.staticDate = staticDate;
      this.table.loading = true;
      this.table.datasets = JSON.parse(JSON.stringify([]));
      this.getData(date, dateStart).subscribe(result => {
        this.table.loading = false;
        this.table.datasets = JSON.parse(JSON.stringify(result));
      }, err => {
        this.table.loading = false;
        this.error.open(err);
      });
    }
  }

  action(event) {
    switch (event.actionType) {
      case 'datepicker':
        this.setDatasets(event.value);
        break;
      case 'imageLink':
        this.imageView.open(event.value);
        break;
    }
  }
}
