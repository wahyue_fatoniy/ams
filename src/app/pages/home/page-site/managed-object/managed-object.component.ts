import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Table,
  FormV2,
  StorageService,
  Discobject,
  EquipmentType,
  ErrorHandlerComponent,
  FormV2Component,
  DialogRemoveComponent,
  ManagedObject,
  UrlEncodePipe,
  AuthService
 } from '../../../../shared';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'app-managed-object',
  templateUrl: './managed-object.component.html',
  styleUrls: ['./managed-object.component.css']
})
export class ManagedObjectComponent implements OnInit {

  constructor(public storage: StorageService, public http: HttpClient,public auth:AuthService) {}
  table: Table = {
    title    : `Table managed object`,
    loading  : false,
    columns  : ['id', 'Name', 'Equipment Type', 'Brand', 'Series', 'Parent'],
    buttons  : this.auth.userHasScopes(['write:managedObject'])?['create', 'remove']:[],
    datasets : []
  };
  @ViewChild('form') form: FormV2Component;
  @ViewChild('remove') remove: DialogRemoveComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  encode: UrlEncodePipe = new UrlEncodePipe();
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  config: FormV2 = {
    title :  `Form managed object`,
    actionType : '',
    value  : {},
    models : [
        {type: 'text', model: 'Name'},
        {type: 'list', model: 'Equipment Type', idSet: 'id', listLabel: 'label', list: [], event: 'Verify parent'},
        {type: 'list', model: 'Parent', idSet: 'id', listLabel: 'name', list: [], required: false}
    ]
  };
  equipmentTypes: EquipmentType[] = [];
  discobjects: Discobject[] = [];
  managedObjects: ManagedObject[] = [];
  ngOnInit() {
    this.setDatasets();
  }


  setDatasets() {
    this.table.loading = true;
    Observable.forkJoin([
      this.http.get<Discobject[]>(`/api/discobjects/${this.latitude}/${this.longitude}/`),
      this.http.get<EquipmentType[]>(`/api/equipmenttype`),
      this.http.get<ManagedObject[]>(`/api/managed_objects?filter=${this.encode.transform({
        include: ['site'],
        where : { site_id : this.storage.site.id }
      })}`)
    ]).subscribe((result: any) => {
         this.table.loading = false;
         this.discobjects    = result[0];
         this.equipmentTypes = result[1];
         this.managedObjects = result[2];
         this.table.datasets = result[0].filter(res => res.parent != null).map(res => {
            return {
              id : res.id,
              Name : res.name,
              'Equipment Type' : res.eqType.type,
              Brand : res.eqType.brand,
              Series : res.eqType.series,
              Parent  : res.parent.eqType.type == 'Site' ? '' : res.parent.name
            };
         });
    }, err => this.error.open(err));
  }

  action(event) {
    if (event.actionType == 'create') {
        this.config.actionType = event.actionType;
        this.config.models[1].list  = this.equipmentTypes.filter(res => res.type != 'Site').map(res =>
          Object.assign({}, res, {label: `${res.type} ${res.brand} ${res.series}`})
        );
        this.config.models[2].list  = this.discobjects.filter(res => res.eqType.type == 'Zone');
        this.form.open(this.config);
    } else if (event.actionType == 'remove') {
       this.remove.open({
          id : event.value.id,
          message :  `Are you sure to remove ${event.value['Name']}`
       });
    }
  }

  save(event) {
    const index = this.discobjects.findIndex(res => res.eqType.type == 'Site');
    const disc: Discobject = {
      name : event.value.Name,
      eqType : {id: event.value['Equipment Type']},
      parent  : { id : event.value.Parent == undefined ? this.discobjects[index].id : event.value['Parent']},
      location : { latitude : this.latitude, longitude : this.longitude }
    };
    this.http.post<Discobject>('/api/discobjects', disc).subscribe(result => {
        const value: ManagedObject = {
          site_id : this.storage.site.id,
          discobj_id : result.id,
          top : 50,
          left : 20
        };
        this.http.post<ManagedObject>('/api/managed_objects', value)
        .subscribe(managed => this.setDatasets(), err => this.error.open(err));
    }, err => this.error.open(err));
  }

  saveRemove(event) {
   const index = this.managedObjects.findIndex(res => res.discobj_id == event);
   const value = this.managedObjects[index];
   Observable.forkJoin([
      this.http.delete(`/api/discobjects/${event}`),
      this.http.delete(`/api/managed_objects/${value.id}`)
   ]).subscribe(result => this.setDatasets(), err => this.error.open(err));
  }


}
