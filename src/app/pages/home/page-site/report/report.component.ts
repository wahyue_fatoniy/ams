import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { StorageService, ErrorHandlerComponent, HOST } from '../../../../shared';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  @ViewChild('error') error: ErrorHandlerComponent;
  constructor(private http: HttpClient, public domSanitizer: DomSanitizer, public storage: StorageService) { }
  report: SafeResourceUrl;
  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;

  ngOnInit() {
    const loginUrl = `${HOST}:8080/jasperserver/rest/login?j_username=jasperadmin&j_password=jasperadmin`;
    const reportUrl: string = `${HOST}:8080/jasperserver/rest_v2/reports/reports/Report/amsAlarmReport.html?`
    + `latitude=${this.latitude}&longitude=${this.longitude}`;
    this.http.get(loginUrl, { withCredentials: true }).subscribe(
      result => {
        this.http.get(reportUrl, { responseType: 'blob', withCredentials: true }).subscribe(
          blobFile => {
            const objectURL = window.URL.createObjectURL(blobFile);
            this.report = this.domSanitizer.bypassSecurityTrustResourceUrl(objectURL);
          }, err => this.error.open(err));
      }, err => this.error.open(err));
  }

}
