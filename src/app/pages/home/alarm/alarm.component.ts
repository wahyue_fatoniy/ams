import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  FormatDatePipe,
  Table,
  StorageService,
  Alarm,
  Discobject,
  DialogImageComponent,
  DialogRemoveComponent,
  AuthService,
  ErrorHandlerComponent
} from '../../../shared';

@Component({
  selector: 'app-alarm',
  templateUrl: './alarm.component.html',
  styleUrls: ['./alarm.component.css']
})

export class AlarmComponent implements OnInit {
  constructor(
    public http: HttpClient,
    public storage: StorageService,
    public auth: AuthService
  ) {}
  date: FormatDatePipe = new FormatDatePipe();
  @ViewChild('remove') remove: DialogRemoveComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  @ViewChild('imageView') imageView: DialogImageComponent;
  table: Table = {
    title: 'Table alarm',
    buttons: ['acknowledge'],
    columns: [
      'id',
      'Site',
      'Type',
      'Managed object',
      'Severity',
      'Message',
      'Active Time',
      'Acknowledge User',
      'Acknowledge Time'
    ],
    loading: false,
    datasets: [],
    width: '1300px'
  };

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.table.loading = true;
    this.http.get<Alarm[]>(`/api/alarms`).subscribe(
      res => {
        this.table.loading = false;
        this.table.datasets = res
          .sort((a, b) => Date.parse(b.actTime) - Date.parse(a.actTime))
          .map(alarm => {
            console.log(alarm);
            return {
              id: alarm.id,
              Site:
                alarm.alarmObj.parent == null
                  ? alarm.alarmObj.name
                  : alarm.alarmObj.parent.name,
              Type: alarm.type,
              'Managed object': alarm.alarmObj.name,
              Severity: alarm.severity,
              Message: alarm.description,
              'Active Time': this.date.transform(
                alarm.actTime,
                'DD/MM/YYYY hh:mm:ss'
              ),
              'Acknowledge User': alarm.ackUser,
              'Acknowledge Time':
                alarm.ackTime == null
                  ? alarm.ackTime
                  : this.date.transform(alarm.ackTime, 'DD/MM/YYYY hh:mm:ss')
            };
          });
      },
      err => this.error.open(err)
    );
  }

  action(event) {
    switch (event.actionType) {
      case 'acknowledge':
        this.remove.open({
          id: event.value.id,
          message: `Are you sure to acknowledge ${event.value.Message}`,
          acknowledge: true
        });
        break;
      case 'imageLink':
        this.imageView.open(event.value);
        break;
    }
  }

  save(event) {
    const name = this.auth.getProfile().nickname;
    this.http
      .post(`/api/alarms/acknowledge/${event}/${name}/`, {})
      .subscribe(result => this.getData(), err => this.error.open(err));
  }
}
