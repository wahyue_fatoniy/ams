import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { 
  Site, UrlEncodePipe, ErrorHandlerComponent, FormDashboardDialogComponent, FormDashboardValue,
  StorageService, Information, Gauge, Alarm, PerfMeasurement, SocketService, PerfCounter, Discobject,
  DialogRemoveComponent
 } from '../../../shared';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

export interface WidgetSettingView {
  view         : string,
  informations : Array<string>, 
}

export interface PerfcounterWidget {
  label     : string;
  value     : number;
  unit      : string;
}

export interface WidgetSettingView {
 view         : string,
 informations : Array<string>, 
}

export interface Widget {
 id           : number;
 label        : string;
 latitude     : number;
 longitude    : number;
 config       : WidgetSettingView;
}

export interface WidgetView {
  id    : number;
  label : string;
  gauge ?: Gauge;
  informations ?: Information[];
  loading : boolean;
}

const DEFAULT_CONFIG:WidgetSettingView =  {view:"Energy",informations:["Temperature","Humidity","Alarm"]};

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    public http: HttpClient, 
    public storage:StorageService,
    public socket:SocketService
  ){
    this.storage.defaultWidget = this.storage.defaultWidget == null ? DEFAULT_CONFIG : this.storage.defaultWidget;
    this.storage.widgets = this.storage.widgets == null ? []:this.storage.widgets;
  }
  encode: UrlEncodePipe = new UrlEncodePipe();
  @ViewChild('form') form:FormDashboardDialogComponent;
  @ViewChild('error') error: ErrorHandlerComponent;
  @ViewChild('remove') remove:DialogRemoveComponent;
  sites: Site[] = [];
  loading:boolean = false;
  alarms:Alarm[] = [];
  measurements:PerfMeasurement[] = [];
  widgetViews:WidgetView[] = [];
  counters:PerfCounter[] = [];
  discobjects:Discobject[] = [];
  ngOnInit() {
    this.getData();
    this.socket.streaming('perfmeasurement.data').subscribe((res:PerfMeasurement)=>{
       const indexCounter = this.counters.findIndex(counter=> counter.id === res.counter.id);
       const indexMeasurement = this.measurements.findIndex(measurement=>measurement.id === res.id);
       if(indexCounter !== -1 && indexMeasurement === -1){
          res.counter = this.counters[indexCounter];
          this.measurements.push(res);
          this.setWidgets();
       } 
    });
    this.socket.streaming('alarm.data').subscribe((res:Alarm)=>{
        const indexDiscobject = this.discobjects.findIndex(discobject=>discobject.id===res.alarmObj.id);
        const indexAlarm = this.alarms.findIndex(alarm=>alarm.id===res.id);
        if(indexDiscobject !== -1 && indexAlarm === -1){
          res.alarmObj = this.discobjects[indexDiscobject];
          this.alarms.push(res);
          this.setWidgets();
        }
    })
  }

  getData() {
    this.loading = true;
    var servicesAlarms = [];
    var servicesMeasurements = [];
    var servicesDiscobjects = [];
    var servicesCounters = [];
    this.storage.widgets.forEach((res:Widget)=>{
        servicesDiscobjects.push(this.http.get<Discobject[]>(`/api/discobjects/${res.latitude}/${res.longitude}`)),
        servicesCounters.push(this.http.get<PerfCounter[]>(`/api/perfcounters/${res.latitude}/${res.longitude}`)),
        servicesAlarms.push(this.http.get<Alarm[]>(`/api/alarms/${res.latitude}/${res.longitude}`));
        servicesMeasurements.push(
          this.http.get<PerfMeasurement[]>(`/api/perfmeasurements/${res.latitude}/${res.longitude}`
        ));
    });
    var allServices = [
      this.http.get<Site[]>(`/api/sites?filter=${this.encode.transform({order : 'label'})}`)
    ];
    if(this.storage.widgets.length !== 0 ){
      allServices = [
          this.http.get<Site[]>(`/api/sites?filter=${this.encode.transform({order : 'label'})}`),
          Observable.forkJoin(servicesAlarms),
          Observable.forkJoin(servicesMeasurements),
          Observable.forkJoin(servicesDiscobjects),
          Observable.forkJoin(servicesCounters)
      ];
    }
    Observable.forkJoin(allServices).subscribe((result:any)=>{
         this.loading = false;
         this.sites = result[0];
         if(this.storage.widgets.length !== 0){
            result[1].forEach(alarm=>this.alarms = this.alarms.concat(alarm));
            result[2].forEach(measurement=>this.measurements= this.measurements.concat(measurement));
            result[3].forEach(discobject=>this.discobjects = this.discobjects.concat(discobject));
            result[4].forEach(counter=>this.counters=this.counters.concat(counter));
         }
         this.setWidgets();
    },err=>{
      this.loading = false;
      this.error.open(err);
    });
  }


  setAlarmCount(latitude:number,longitude:number):number{
    const alarms:Alarm[] = this.alarms.filter(alarm=>
      alarm.alarmObj.location.latitude == latitude &&
      alarm.alarmObj.location.longitude == longitude
    );
    return alarms.length;
  }

  setMaximumTemperature(latitude:number,longitude:number):number{
    var value = 0;
    var result:any = {};
    this.counters.forEach(res=>{
        if(res.perfObj.period === "MIN5" && 
          res.discObj.location.latitude === latitude &&
          res.discObj.location.longitude === longitude
        ){
          if(res.perfObj.name === 'Temperatur Ruang' || res.perfObj.name === 'Temperatur Coil'){
            result[ res.id ] = 0;
          }   
        }
    });
    this.measurements.forEach(res=>{
        if(result[res.counter.id]!= undefined){
           result[res.counter.id] = res.measurementValue;
        }
    })
    result = Object.keys(result).map(key=>result[key]);
    if(result.length !== 0){
      value = Math.max.apply(null, result);
    }
    return value;
  }

  setMaximumHumidity(latitude:number,longitude:number):number{
    var value = 0;
    var result:any = {};
    this.counters.forEach(res=>{
        if(res.perfObj.period === "MIN5" && 
          res.discObj.location.latitude === latitude &&
          res.discObj.location.longitude === longitude
        ){
          if(res.perfObj.name === 'Kelembapan Relatif'){
            result[ res.id ] = 0;
          }   
        }
    });
    this.measurements.forEach(res=>{
        if(result[res.counter.id]!= undefined){
           result[res.counter.id] = res.measurementValue;
        }
    })
    result = Object.keys(result).map(key=>result[key]);
    if(result.length !== 0){
      value = Math.max.apply(null, result);
    }
    return value;
  }

  setPower(latitude:number,longitude:number):number{
    var value = 0;
    this.measurements.forEach(res=>{
      if(res.counter.discObj.location.latitude === latitude && 
        res.counter.discObj.location.longitude === longitude &&
        res.counter.perfObj.name === 'Daya' &&
        res.counter.perfObj.period === 'MIN5' &&
        res.counter.discObj.eqType.type === 'Site'
      ){
        value = res.measurementValue;
      }
    })
    return value;
  }

  setEnergy(latitude:number,longitude:number):number{
    var value = 0;
    this.measurements.forEach(res=>{
      if(res.counter.discObj.location.latitude === latitude &&
        res.counter.discObj.location.longitude === longitude &&
        res.counter.perfObj.name === 'Energi' &&
        res.counter.perfObj.period === 'MIN5' &&
        res.counter.discObj.eqType.type === 'Site'
      ){
          value += res.measurementValue
      }   
    })
    return value;
  }

  setWidgets(){
    this.widgetViews = [];
    this.storage.widgets.forEach(site=>{
        var informations = [];
        site.config.informations.forEach(info=>{
           var value = 0;
           switch(info){
              case 'Temperature': value = this.setMaximumTemperature(site.latitude,site.longitude);
                break;

              case 'Humidity': value = this.setMaximumHumidity(site.latitude,site.longitude);
                break;

              case 'Alarm': value = this.setAlarmCount(site.latitude,site.longitude);
                break;
           }
           informations.push({type:info,value:value})
        });
        var valueGauge = 0; 
        switch(site.config.view){
          case 'Power': valueGauge = this.setPower(site.latitude,site.longitude); 
            break;
          
          case 'Energy': valueGauge = this.setEnergy(site.latitude,site.longitude);
            break;
        }
        const value:WidgetView = {
          id : site.id,
          label : site.label,
          loading : false,
          gauge : {type : site.config.view , value : valueGauge },
          informations : informations
        };
        this.widgetViews.push(value);
    })
  }

  editDefaultWidget(){
    const config:FormDashboardValue = {
      actionType : 'create', 
      label : 'Form default view widget', 
      configWidget : this.storage.defaultWidget 
    }
    this.form.open(config);
  }

  setSite(event:Site) {
    var widgets = Object.assign([],this.storage.widgets);
    const value:Widget = {
      id : event.id,
      label : event.label,
      latitude : event.location.lat,
      longitude : event.location.lng,
      config : this.storage.defaultWidget
    }
    const index:number = this.storage.widgets.findIndex(res=>res.id===event.id);
    if(index === -1){
      widgets.push(value);
    }     
    this.storage.widgets = Object.assign([],widgets);
    this.getData();
  }

  checkStorage(site:Site):boolean{
    const index:number = this.storage.widgets.findIndex(res=>res.id===site.id);
    return index === -1;
  }

  countSiteNotSet():boolean{
    var widgets = this.sites.filter(site=>this.checkStorage(site));
    return widgets.length === 0;
  }

  save(event){
    switch(event.actionType){
      case 'create':
        this.storage.defaultWidget = Object.assign({},event.config);
      break;

      case 'edit':
        var widgets = Object.assign([],this.storage.widgets);
        const index:number = widgets.findIndex(res=>res.id === event.id);
        widgets[index].config = event.config;
        this.storage.widgets = Object.assign([],widgets);
        this.setWidgets();
      break;
    }
  }

  actionWidget(event){
    const index:number = this.storage.widgets.findIndex(res=>res.id===event.id);
    switch(event.actionType){
      case 'edit':
        const config:FormDashboardValue = {
          id : event.id,
          actionType : 'edit', 
          label : `Form ${this.storage.widgets[index].label}`, 
          configWidget : this.storage.widgets[index].config 
        }
        this.form.open(config);
      break;
      case 'remove':
        this.remove.open({
            id : event.id,
            message : `Are you sure to remove ${this.storage.widgets[index].label}`
        })
      break;
    }
  }

  saveRemove(event) {
    var widgets = Object.assign([],this.storage.widgets);
    widgets = widgets.filter(res=>res.id!==event);
    this.storage.widgets = Object.assign([],widgets);
    this.setWidgets();
  }

}
