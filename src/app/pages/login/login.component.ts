import { Component, OnInit } from '@angular/core';
import {  AuthService } from '../../shared';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor( public auth: AuthService, public router: Router) {}

  ngOnInit() {
    if (!this.auth.isAuthenticated()) {
      this.auth.loginView();
    } else {
      this.router.navigate(['/public']);
    }
  }

}
