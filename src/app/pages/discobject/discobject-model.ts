import { EqType } from '../setup/eq-type/eq-type-model';
import { HttpErrorResponse } from '@angular/common/http';

export interface Discobject {
  id?: number;
  location: { latitude: number; longitude: number };
  eqType: EqType;
  name: string;
  parent?: Discobject;
}

export interface IDiscobject {
  location: { latitude: number; longitude: number };
  parent: { id: number } | null;
  eqType: { id: number };
  name: string;
}

const DEFAULT: Discobject = {
  id: null,
  name: null,
  eqType: { id: null, type: null, brand: null, series: null },
  location: { latitude: null, longitude: null },
  parent: null
};

export class DiscobjectModel {
  constructor() {}
  list: Discobject[];
  loading: boolean;
  statusError: HttpErrorResponse;

  setAll(list: Discobject[]) {
    console.info('[discobject] ambil data managed object');
    console.debug(list);
    this.loading = false;
    this.list = list;
  }

  getPadlock(latitude?: number, longitude?: number) {
    let values =
      latitude && longitude
        ? this.list.filter(
            i =>
              i.location.latitude === latitude &&
              i.location.longitude === longitude
          )
        : this.list;
      return values.filter(value => value.eqType.type === 'PL1BT');
  }

  getById(id: number): Discobject {
    const index = this.list.findIndex(discobject => discobject.id === id);
    return index === -1 ? DEFAULT : this.list[index];
  }

  getError(): { name: string; message: string } {
    let name: string, message: string;
    name = this.statusError.name;
    message = this.statusError.message;
    if (this.statusError.error) {
      if (this.statusError.error.error) {
        name = this.statusError.error.error.name
          ? this.statusError.error.error.name
          : name;
        message = this.statusError.error.error.message
          ? this.statusError.error.error.message
          : message;
      }
    }
    return { name: name, message: message };
  }

  findByType(type: string) {
    return this.list.filter(discobject => discobject.eqType.type === type);
  }

  setError(error: HttpErrorResponse) {
    this.statusError = error;
    this.loading = false;
    console.error('[discobject] terdeteksi error', this.getError());
  }

  remove(id: number) {
    console.info('[discobject] hapus data discobject, ID = ' + id);
    this.loading = false;
    this.list = this.list.filter(discobject => discobject.id !== id);
  }
}
