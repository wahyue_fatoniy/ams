import { TestBed, inject } from '@angular/core/testing';

import { DiscobjectService } from './discobject.service';

describe('DiscobjectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DiscobjectService]
    });
  });

  it('should be created', inject([DiscobjectService], (service: DiscobjectService) => {
    expect(service).toBeTruthy();
  }));
});
