import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { pipe } from 'rxjs/util/pipe';
import 'rxjs/add/observable/throw';
import { Discobject } from './discobject-model';
import { catchError } from 'rxjs/operators';
import { StorageService } from '../../shared';
const URL  = '/api/discobjects';

@Injectable()
export class DiscobjectService {

  constructor(private http: HttpClient, private storage: StorageService) { }

  latitude: number = this.storage.site.location.lat;
  longitude: number = this.storage.site.location.lng;
  getAllDiscobjects(): Observable<Discobject[]> {
    return this.http.get<Discobject[]>(URL)
    .pipe(
      catchError(error => Observable.throw(error))
    );
  }

}
