import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TemplateModule } from '../template/template.module';
import { MapViewComponent } from './public/map-view/map-view.component';
import { SetupComponent } from './public/setup/setup.component';
import { ProvinceComponent } from './public/setup/province/province.component';
import { CityComponent } from './public/setup/city/city.component';
import { DistrictComponent } from './public/setup/district/district.component';
import { SiteComponent } from './public/setup/site/site.component';
import { CallbackComponent } from './callback/callback.component';
import { LoginComponent } from './login/login.component';
import { PageSiteComponent } from './home/page-site/page-site.component';
import { HistoryComponent } from './home/page-site/history/history.component';
import { AlarmSiteComponent } from './home/page-site/alarm-site/alarm-site.component';
import { PublicComponent } from './public/public.component';
import { AngularMaterialModule } from '../angular-material.module';
import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { EquipmentTypeComponent } from './public/setup/equipment-type/equipment-type.component';
import { ToolsModule } from '../tools.module';
import { ReportComponent } from './home/page-site/report/report.component';
import { ManagedObjectComponent } from './home/page-site/managed-object/managed-object.component';
import { PerfObjectComponent } from './public/setup/perf-object/perf-object.component';
import { SecurityComponent } from './public/setup/security/security.component';
import { ChannelComponent } from './public/setup/channel/channel.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { ViewComponent } from './home/page-site/view/view.component';
import { MappingSensorComponent } from './home/page-site/mapping-sensor/mapping-sensor.component';
import { PerfCounterComponent } from './home/page-site/perf-counter/perf-counter.component';
import { AlarmComponent } from './home/alarm/alarm.component';
import { ProvisioningComponent } from './home/page-site/provisioning/provisioning.component';
import { ProvisioningHistoryComponent } from './home/page-site/provisioning-history/provisioning-history.component';
import { registerLocaleData } from '@angular/common';
import localesId from '@angular/common/locales/id';
import { WidgetTotalEnergyComponent } from './home/page-site/view-v2/widget-total-energy/widget-total-energy.component';
import { ViewV2Component } from './home/page-site/view-v2/view-v2.component';
import { WidgetTotalAcComponent } from './home/page-site/view-v2/widget-total-ac/widget-total-ac.component';
import { WidgetTotalChillerComponent } from './home/page-site/view-v2/widget-total-chiller/widget-total-chiller.component';
import { WidgetTotalOtherComponent } from './home/page-site/view-v2/widget-total-other/widget-total-other.component';
import { WidgetRoomTempComponent } from './home/page-site/view-v2/widget-room-temp/widget-room-temp.component';
import { WidgetRoomHumidityComponent } from './home/page-site/view-v2/widget-room-humidity/widget-room-humidity.component';
import { ChartEnergyComponent } from './home/page-site/view-v2/chart-energy/chart-energy.component';
import { ChartPeriodicComponent } from './home/page-site/view-v2/chart-periodic/chart-periodic.component';
import { AreaComponent } from './home/page-site/view-v2/area/area.component';
import { PanelBuildingComponent } from './home/page-site/view-v2/panel-building/panel-building.component';
import { PadlockComponent } from './home/page-site/view-v2/area/padlock/padlock.component';
import { DraggableDirective } from './home/page-site/view-v2/area/draggable.directive';
import { ConvertLeftPipe } from './home/page-site/view-v2/area/convert-left.pipe';
import { AcComponent } from './home/page-site/view-v2/area/ac/ac.component';
import { ChillerComponent } from './home/page-site/view-v2/area/chiller/chiller.component';
import { PumpComponent } from './home/page-site/view-v2/area/pump/pump.component';
import { RoomTempHumidityComponent } from './home/page-site/view-v2/area/room-temp-humidity/room-temp-humidity.component';
import { DashboardPadlockComponent } from './page-padlock/dashboard-padlock/dashboard-padlock.component';
import { WorkerComponent } from './page-padlock/worker/worker.component';
import { SidebarPadlockComponent } from './page-padlock/sidebar-padlock/sidebar-padlock.component';
import { WorkerTableComponent } from './page-padlock/worker/worker-table/worker-table.component';
import { WorkerFormComponent } from './page-padlock/worker/worker-form/worker-form.component';
import { WorkerService } from './page-padlock/worker/worker.service';
import { PhotoProfileComponent } from './page-padlock/worker/worker-table/photo-profile/photo-profile.component';
import { ValidationImageDirective } from './validation-image';
import { PagePadlockComponent } from './page-padlock/page-padlock.component';
import { WorkOrderComponent } from './page-padlock/work-order/work-order.component';
import { WorkOrderTableComponent } from './page-padlock/work-order/work-order-table/work-order-table.component';
import { WorkOrderFormComponent } from './page-padlock/work-order/work-order-form/work-order-form.component';
import { WorkOrderService } from './page-padlock/work-order/work-order.service';
import { SiteService } from './setup/site/site.service';
import { AccessKeyService } from './access-key/access-key.service';
import { DiscobjectService } from './discobject/discobject.service';
import { EqTypeComponent } from './setup/eq-type/eq-type.component';
import { WorkOrderHistoryComponent } from './page-padlock/work-order-history/work-order-history.component';
import {
  WorkOrderHistoryTableComponent
} from './page-padlock/work-order-history/work-order-history-table/work-order-history-table.component';
import { WorkOrderHistoryService } from './page-padlock/work-order-history/work-order-history.service';
import { AccessKeyHistoryComponent } from './page-padlock/access-key-history/access-key-history.component';
import {
  AccessKeyHistoryTableComponent
} from './page-padlock/access-key-history/access-key-history-table/access-key-history-table.component';
import { AccessKeyHistoryService } from './page-padlock/access-key-history/access-key-history.service';
import { RequestKeyService } from './page-padlock/request-key/request-key.service';
import { DialogRemoveComponent } from './page-padlock/dialog-remove/dialog-remove.component';
import { DialogNoteComponent } from './page-padlock/dialog-note/dialog-note.component';
registerLocaleData(localesId);

@NgModule({
  imports: [
    CommonModule,
    TemplateModule,
    AngularMaterialModule,
    SharedModule,
    RouterModule,
    HttpClientModule,
    ToolsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    MapViewComponent,
    SetupComponent,
    ProvinceComponent,
    CityComponent,
    DistrictComponent,
    SiteComponent,
    CallbackComponent,
    LoginComponent,
    PageSiteComponent,
    HistoryComponent,
    AlarmSiteComponent,
    PublicComponent,
    EquipmentTypeComponent,
    ReportComponent,
    ManagedObjectComponent,
    PerfObjectComponent,
    SecurityComponent,
    ChannelComponent,
    HomeComponent,
    DashboardComponent,
    ViewComponent,
    MappingSensorComponent,
    PerfCounterComponent,
    AlarmComponent,
    ProvisioningComponent,
    ProvisioningHistoryComponent,
    WidgetTotalEnergyComponent,
    ViewV2Component,
    WidgetTotalAcComponent,
    WidgetTotalChillerComponent,
    WidgetTotalOtherComponent,
    WidgetRoomTempComponent,
    WidgetRoomHumidityComponent,
    ChartEnergyComponent,
    ChartPeriodicComponent,
    AreaComponent,
    PanelBuildingComponent,
    PadlockComponent,
    DraggableDirective,
    ConvertLeftPipe,
    AcComponent,
    ChillerComponent,
    PumpComponent,
    RoomTempHumidityComponent,
    DashboardPadlockComponent,
    WorkerComponent,
    SidebarPadlockComponent,
    WorkerTableComponent,
    WorkerFormComponent,
    PhotoProfileComponent,
    ValidationImageDirective,
    PagePadlockComponent,
    WorkOrderComponent,
    WorkOrderTableComponent,
    WorkOrderFormComponent,
    EqTypeComponent,
    WorkOrderHistoryComponent,
    WorkOrderHistoryTableComponent,
    AccessKeyHistoryComponent,
    AccessKeyHistoryTableComponent,
    DialogRemoveComponent,
    DialogNoteComponent
  ],
  entryComponents : [PhotoProfileComponent, WorkerFormComponent, WorkOrderFormComponent, DialogRemoveComponent, DialogNoteComponent],
  providers: [
    { provide: LOCALE_ID, useValue: 'id' }, WorkOrderHistoryService, AccessKeyHistoryService,
    WorkerService, WorkOrderService, SiteService, AccessKeyService, DiscobjectService, RequestKeyService
  ]
})
export class PagesModule { }
