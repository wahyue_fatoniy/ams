import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { pipe } from 'rxjs/util/pipe';
import 'rxjs/add/observable/throw';
import { AccessKey } from './access-key-model';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
const URL  = '/api/access_keys';

@Injectable()
export class AccessKeyService {

  constructor(private http: HttpClient) { }

  getAllAccessKeys(): Observable<AccessKey[]> {
    return this.http.get<AccessKey[]>(URL)
    .pipe(
      catchError(error => Observable.throw(error))
    );
  }

  createAccessKey(discobjs: number[], work_order_id: number): Observable<AccessKey[]> {
    const possible = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`;
    let key, services = [];
    discobjs.forEach(discobj_id => {
      key = '';
      for (let i = 0; i < 10; i++) {
        key += possible.charAt(Math.floor(Math.random() * possible.length));
      }
       const value: AccessKey = {
          work_order_id : work_order_id, discobj_id : discobj_id, key : key
       };
       services.push(this.http.post(URL, value));
    });
    if (services.length === 0) {
      return of([]);
    } else  {
      return Observable.forkJoin(services);
    }
  }

}
