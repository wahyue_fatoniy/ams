import { HttpErrorResponse } from '@angular/common/http';

export interface AccessKey {
  id?: number;
  work_order_id: number;
  discobj_id: number;
  key: string;
}

const DEFAULT: AccessKey = {
  id: null,
  work_order_id: null,
  discobj_id: null,
  key: null
};


export class AccessKeyModel {


  constructor() {}
  list: AccessKey[];
  loading: boolean;
  statusError: HttpErrorResponse;

  setAll(list: AccessKey[]) {
    console.info('[access key] ambil data access key');
    console.debug(list);
    this.loading = false;
    this.list = list;
  }

  findByWorkOrder(id: number): AccessKey[] {
    return this.list.filter(key => key.work_order_id === id);
  }

  getById(id: number): AccessKey {
    const index = this.list.findIndex(w => w.id === id);
    return index === -1 ? DEFAULT : this.list[index];
  }

  getError(): {name: string, message: string} {
    let name: string, message: string;
    name = this.statusError.name;
    message = this.statusError.message;
    if (this.statusError.error) {
        if (this.statusError.error.error) {
          name = this.statusError.error.error.name ? this.statusError.error.error.name : name;
          message = this.statusError.error.error.message ? this.statusError.error.error.message : message;
        }
    }
    return {name: name, message: message};
  }

  setError(error: HttpErrorResponse) {
    this.statusError = error;
    this.loading = false;
    console.error('[access key] terdeteksi error', this.getError());
  }

  add(value: AccessKey) {
    console.info('[access key] tambah data access key');
    console.debug(value);
    this.loading = false;
    this.list = [value, ...this.list];
  }

  concatAll(values: AccessKey[]) {
    this.loading = false;
    this.list = this.list.concat(values);
    console.info('[Access key] Add All values access key', this.list);
  }

  edit(id: number, value: AccessKey) {
    console.info('[access key] ubah data access key, ID = ' + id);
    console.debug(value);
    this.loading = false;
    this.list = this.list.map(key => key.id === id ? value : key);
  }

  remove(id: number) {
    console.info('[access key] hapus data access key, ID = ' + id);
    this.loading = false;
    this.list = this.list.filter(key => key.id !== id);
  }

}
