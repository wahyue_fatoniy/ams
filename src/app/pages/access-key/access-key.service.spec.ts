import { TestBed, inject } from '@angular/core/testing';

import { AccessKeyService } from './access-key.service';

describe('AccessKeyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccessKeyService]
    });
  });

  it('should be created', inject([AccessKeyService], (service: AccessKeyService) => {
    expect(service).toBeTruthy();
  }));
});
