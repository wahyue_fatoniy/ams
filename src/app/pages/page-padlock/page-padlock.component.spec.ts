import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagePadlockComponent } from './page-padlock.component';

describe('PagePadlockComponent', () => {
  let component: PagePadlockComponent;
  let fixture: ComponentFixture<PagePadlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagePadlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagePadlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
