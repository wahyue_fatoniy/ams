import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AuthService } from '../../../shared';
import { Router } from '@angular/router';

declare var $: any;
declare interface RouteInfo {
    scopes ?: Array<string>;
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
   {path: 'padlock/dashboard-padlock', title: 'Dashboard', icon: 'dashboard', class: ''},
   {path: 'padlock/worker', title: 'Worker', icon: 'contacts', class: ''},
   {path: 'padlock/work-order', title: 'Work Order', icon: 'file_copy', class: ''},
   {path: 'padlock/work-order-history', title: 'Work Order History', icon: 'bookmarks', class: ''},
   {path: 'padlock/access-key-history', title: 'Access Key History', icon: 'vpn_key', class: ''}
];


@Component({
  selector: 'app-sidebar-padlock',
  templateUrl: './sidebar-padlock.component.html',
  styles: []
})
export class SidebarPadlockComponent implements OnInit {

  menuItems: any[];
  constructor(
      public location: Location, public auth: AuthService, public router: Router
  ) {}

  ngOnInit() {
    this.menuItems = Object.assign([], ROUTES);
    this.navActiveInit();

  }

  navActiveInit() {
    const path: any = this.location.path();
    const params = path.split('/');
    const param  = `${params[1]}/${params[2]}`;
    this.menuItems  = this.menuItems.map(res => {
      res.class = res.path === param ? 'active' : '';
      return res;
    });
  }

  setLink(path: string) {
    this.router.navigate([path]);
    this.menuItems = this.menuItems.map(res => {
       res.class = res.path === path ? 'active' : '';
       return res;
    });
  }



  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  }

}
