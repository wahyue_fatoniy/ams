import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarPadlockComponent } from './sidebar-padlock.component';

describe('SidebarPadlockComponent', () => {
  let component: SidebarPadlockComponent;
  let fixture: ComponentFixture<SidebarPadlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarPadlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarPadlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
