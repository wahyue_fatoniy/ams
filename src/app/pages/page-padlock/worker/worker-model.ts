import { HttpErrorResponse } from '@angular/common/http';


export interface Worker {
  id?: number;
  email: string;
  name: string;
  phone: string;
  profile: string;
  company: string;
  username: string;
  password?: string;
}

const DEFAULT: Worker = {id: null, name: null, phone: null,
  profile: null, company: null, email: null, username: null
};

export class WorkerModel {
  constructor() {}
  list: Worker[];
  loading: boolean;
  statusError: HttpErrorResponse;

  setAll(list: Worker[]) {
    console.info('[worker] ambil data worker');
    console.debug(list);
    this.loading = false;
    this.list = list;
  }


  getById(id: number): Worker {
    const index = this.list.findIndex(worker => worker.id === id);
    return index === -1 ? DEFAULT : this.list[index];
  }

  getError(): {name: string, message: string} {
    let name: string, message: string;
    name = this.statusError.name;
    message = this.statusError.message;
    if (this.statusError.error) {
        if (this.statusError.error.error) {
          name = this.statusError.error.error.name ? this.statusError.error.error.name : name;
          message = this.statusError.error.error.message ? this.statusError.error.error.message : message;
        }
    }
    return {name: name, message: message};
  }

  setError(error: HttpErrorResponse) {
    this.statusError = error;
    this.loading = false;
    console.error('[worker] terdeteksi error', this.getError());
  }

  add(value: Worker) {
    console.info('[worker] tambah data worker');
    console.debug(value);
    this.loading = false;
    this.list = [value, ...this.list];
  }

  edit(id: number, value: Worker) {
    console.info('[worker] ubah data worker, ID = ' + id);
    console.debug(value);
    this.loading = false;
    this.list = this.list.map(worker => worker.id === id ? value : worker);
  }

  remove(id: number) {
    console.info('[worker] hapus data worker, ID = ' + id);
    this.loading = false;
    this.list = this.list.filter(worker => worker.id !== id);
  }

}
