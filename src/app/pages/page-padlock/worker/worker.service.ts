import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { pipe } from 'rxjs/util/pipe';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/observable/throw';
import { Worker } from './worker-model';
import { catchError, map } from 'rxjs/operators';
const URL  = '/api/workers';

@Injectable()
export class WorkerService {

  constructor(private http: HttpClient) { }

  getAllWorkers(): Observable<Worker[]> {
    const filter  = {order: 'name'};
    const encode = encodeURIComponent(JSON.stringify(filter));
    return this.http.get<Worker[]>(`${URL}?filter=${encode}`)
    .pipe(
      map(results => results.map(w => Object.assign({
        id: w.id, name: w.name, email : w.email, phone : w.phone,
        company : w.company, profile: w.profile, username : w.username
      }))),
      catchError(error => Observable.throw(error))
    );
  }

  addWorker(value: Worker) {
     value.profile = `/api/containers/worker/download/${value.profile}`;
     value.password = 'admin';
     return this.http.post<Worker>(URL, value)
     .pipe(
       catchError(error => Observable.throw(error))
     );
  }

  editWorker(id: number, value: Worker) {
    value.profile = `/api/containers/worker/download/${value.profile}`;
    const filter  = {id : id};
    const encode = encodeURIComponent(JSON.stringify(filter));
    return this.http.post(`${URL}/update?where=${encode}`, value)
    .pipe(
      catchError(error => Observable.throw(error))
    );
  }

  removeWorker(value: Worker) {
    this.removeProfile([ value.profile ]).subscribe(result => result);
    return this.http.delete(`${URL}/${value.id}`)
    .pipe(
      catchError(error => Observable.throw(error))
    );
  }

  removeProfile(images: string[]) {
    let services = [];
    images.forEach(image => {
         const values = image.split('/');
         const id = values[ values.length - 1];
         services.push(this.http.delete(`/api/containers/worker/files/${id}`));
    });
    if (services.length === 0) {
      return of([]);
    } else {
      return Observable.forkJoin(services);
    }
  }

}
