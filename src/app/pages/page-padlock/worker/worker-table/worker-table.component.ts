import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { WorkerModel, Worker } from '../worker-model';
import { WorkerService } from '../worker.service';
import { Action, TABLE_STYLE, Filter, AppModel } from '../../../../app.component';
import { PhotoProfileComponent } from './photo-profile/photo-profile.component';
import { WorkerFormComponent } from '../worker-form/worker-form.component';
import { DialogRemoveComponent } from '../../dialog-remove/dialog-remove.component';


@Component({
  selector: 'app-worker-table',
  templateUrl: './worker-table.component.html',
  styles: [TABLE_STYLE]
})

export class WorkerTableComponent implements OnInit {

  constructor(
    private workerService: WorkerService,
    private dialog: MatDialog
  ) {}
  workerModel: WorkerModel = new WorkerModel();
  dataSource: MatTableDataSource<Worker> = new MatTableDataSource([]);
  title = 'Table Worker';
  pageSize;
  pageIndex = 0;
  @Input() enabledAction: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  columns: string[] = ['no', 'profile', 'username', 'email', 'name', 'phone', 'company'];
  typeFilters: Filter[] = [
    {value: 'all', label: 'All Column'}, {value: 'username', label: 'IMEI'}, {value: 'email', label: 'E-Mail'},
     {value: 'name', label: 'Name'}, {value: 'phone', label: 'Phone Number'}, {value: 'company', label: 'Company'}
  ];
  typeFilter = 'all';
  app: AppModel<Worker> = new AppModel<Worker>();
  actionType: string;

  ngOnInit() {
    this.workerModel.loading = true;
    this.workerService.getAllWorkers()
    .subscribe(
      workers => this.onSetTable(workers),
      error => this.workerModel.setError(error)
    );
  }

  onSetTable(list: Worker[]) {
    this.columns = this.enabledAction ? [...this.columns, 'action'] : this.columns;
    this.workerModel.setAll(list);
    const PAGE = localStorage.getItem('pageSize');
    // tslint:disable-next-line:radix
    this.pageSize = PAGE ? parseInt(PAGE) : 25;
    this.reloadTable();
  }

  reloadTable() {
    this.dataSource = new MatTableDataSource(this.workerModel.list);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (value: Worker, keyword: string) =>
      this.app.filterPredicate(value, keyword, this.typeFilter);
  }

  onSetPage(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    localStorage.setItem('pageSize', event.pageSize);
  }

  onClickAction(event: Action<Worker>) {
    this.actionType = event.actionType;
    if (event.actionType !== 'remove') {
       const dialogRef = this.dialog.open(WorkerFormComponent, {
          data : event.value,  width: '900px'
       });
       dialogRef.afterClosed().subscribe(
         result => this.onSave(result),
         error => this.workerModel.setError(error)
       );
    } else {
      const removeRef = this.dialog.open(DialogRemoveComponent, {
        data : {id: event.value.id, message: `Are you sure to remove ${event.value.name} ?`}
      });
      removeRef.afterClosed().subscribe(result => this.onRemove(result));
    }
  }

  onRemove(event) {
    if (event) {
       this.workerModel.loading = true;
       const worker = this.workerModel.getById(event);
       this.workerService.removeWorker(worker).subscribe(
         result => (this.workerModel.remove(event), this.reloadTable()),
         error => this.workerModel.setError(error)
       );
    }
  }

  onSave(value: Worker) {
     if (value && this.actionType === 'create') {
        this.workerModel.loading = true;
        this.workerService.addWorker(value).subscribe(
          worker => (this.workerModel.add(worker), this.reloadTable()),
          error => this.workerModel.setError(error)
        );
     } else if (value && this.actionType === 'edit') {
        this.workerModel.loading = true;
        this.workerService.editWorker(value.id, value).subscribe(
          worker => (this.workerModel.edit(value.id, value), this.reloadTable()),
          error => this.workerModel.setError(error)
        );
     }
  }

  onClickProfile(id: number) {
    const value: Worker = this.workerModel.getById(id);
    const dialogRef = this.dialog.open(PhotoProfileComponent, {
        data : {title: value.name, link: value.profile}
    });
  }

}
