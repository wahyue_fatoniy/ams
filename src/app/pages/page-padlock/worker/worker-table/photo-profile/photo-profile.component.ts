import { Component, Inject} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

interface Profile {
  title: string;
  link: string;
}

@Component({
  selector: 'app-photo-profile',
  templateUrl: './photo-profile.component.html'
})
export class PhotoProfileComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: Profile) { }

}
