import { Component, OnInit, Inject, Optional } from '@angular/core';
import { MAT_DIALOG_DATA, ErrorStateMatcher } from '@angular/material';
import { Worker } from '../worker-model';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
  FormGroupDirective,
  NgForm
} from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { DatePipe } from '@angular/common';
import { WorkerService } from '../worker.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-worker-form',
  templateUrl: './worker-form.component.html',
  styles: [
    `
      input[type="file"] {
        display: none;
      }
      .button-row {
        display: flex;
        align-items: center;
        justify-content: space-around;
      }
    `
  ]
})
export class WorkerFormComponent implements OnInit {

  constructor(
    @Optional()
    @Inject(MAT_DIALOG_DATA)
    public value: Worker,
    public builder: FormBuilder,
    private workerService: WorkerService
  ) {}

  profile: FileUploader = new FileUploader({url: `/api/containers/worker/upload`});
  matcher = new MyErrorStateMatcher();
  date: DatePipe = new DatePipe('id');
  form: FormGroup = this.builder.group({
    id: new FormControl(null),
    username : new FormControl(null, [Validators.required]),
    name: new FormControl(null, [Validators.required]),
    email: new FormControl(null, [Validators.required, Validators.email]),
    phone: new FormControl(null, [Validators.required]),
    company: new FormControl(null, [Validators.required]),
    profile: new FormControl(null, [Validators.required, Validators.pattern(/\.(gif|jpg|jpeg|tiff|png)$/i) ])
  });
  removeImages: string[] = [];

  getPictureName() {
    const index = this.profile.queue.length - 1;
    const results  = this.profile.queue[ index ].file.name.split('.');
    const type     = results[ results.length - 1  ];
    let text = '';
    const possible = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`;
    const date  = this.date.transform(new Date(), 'ddMMyyyyhhmmss');
    for (let i = 0; i < 10; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    const value = `${date}${text}.${type}`;
    this.profile.queue[index].file.name = value;
    return value;
  }

  onSetProfile() {
    const set  = this.form.getRawValue();
    if (set.profile !== null) {
       this.removeImages.push(set.profile);
    }
    set.profile = this.getPictureName();
    this.form.setValue(set);
  }


  onSave() {
    if (this.profile) {
       this.profile.uploadAll();
    }
    this.workerService.removeProfile(this.removeImages).subscribe(result => result);
  }

  ngOnInit() {
    if (this.value) {
      this.form.setValue(this.value);
      const values = this.value.profile.split('/');
      this.form.controls.profile.setValue(values[values.length - 1]);
    }
  }

}
