import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-worker',
  template: `
    <app-worker-table [enabledAction]="true"></app-worker-table>
  `
})
export class WorkerComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

}
