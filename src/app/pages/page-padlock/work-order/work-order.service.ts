import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { pipe } from 'rxjs/util/pipe';
import 'rxjs/add/observable/throw';
import { of } from 'rxjs/observable/of';
import { WorkOrder } from './work-order-model';
import { catchError, map } from 'rxjs/operators';
import { WorkOrderHistory } from '../work-order-history/work-order-history-model';
import { WorkOrderHistoryService } from '../work-order-history/work-order-history.service';
import { DatePipe } from '@angular/common';
import { AccessKeyService } from '../../access-key/access-key.service';
const URL  = '/api/work_orders';


@Injectable()
export class WorkOrderService {

  constructor(
    private http: HttpClient,
    private history: WorkOrderHistoryService,
    private accessKey: AccessKeyService) { }
  date: DatePipe = new DatePipe('id');

  getAllWorkOrders(): Observable<WorkOrder[]> {
    const filter  = {order: 'start DESC'};
    const encode = encodeURIComponent(JSON.stringify(filter));
    return this.http.get<WorkOrder[]>(`${URL}?filter=${encode}`)
    .pipe(
      catchError(error => Observable.throw(error))
    );
  }

  dateToInteger(date) {
     return Date.parse(date);
  }

  editStatus(id: number, status: string, note?: string) {
     const filter  = {id: id};
     const encode = encodeURIComponent(JSON.stringify(filter));
     return this.http.post(`${URL}/update?where=${encode}`, {status: status, note: note});
  }

  verifyExpireds(list: WorkOrder[]): Observable<WorkOrder[]> {
     let services = [];
     let history: WorkOrderHistory;
     const status = {expired: false, finish: false, idle: true, request: true, approve: true, reject: true};
     list.forEach(value => {
        const endDate = Date.parse(new Date(value.end).toString());
        const now = Date.parse(new Date().toString());
        if (now >= endDate && status[ value.status ] ) {
            services = [...services, this.history.createHistory({work_order_id: value.id, status: 'expired'})];
            services = [...services, this.editStatus(value.id, 'expired')];
            value.status = 'expired';
        }
     });
     if (services.length !== 0) {
       Observable.forkJoin(services).subscribe(result => result);
     }
     return of(list);
  }

  setDate(date, hour: number, minute: number, second: number): string {
    let value = new Date(date);
    value.setHours(hour);
    value.setMinutes(minute);
    value.setSeconds(second);
    return this.date.transform(value, 'yyyy-MM-ddTHH:mm:ss', '+00:00') + 'Z';
  }

  addWorkOrder(value: WorkOrder): Observable<WorkOrder> {
     value.start = this.setDate(value.start, 0, 0, 0);
     value.end = this.setDate(value.end, 23, 59, 59);
     value.status = 'idle';
     return this.http.post<WorkOrder>(URL, value)
     .pipe(
        catchError(error => Observable.throw(error))
      );
  }

}
