import { HttpErrorResponse } from '@angular/common/http';

export interface WorkOrder {
  id?: number;
  worker_id: number;
  worker?: string;
  site_id: number;
  site?: string;
  start: string;
  end: string;
  project: string;
  activity: string;
  status: string; // idle, expired, request, approve, reject,
  note?: string;
  access?: number[];
}

const DEFAULT: WorkOrder = {
  id: null,
  worker_id: null,
  site_id: null,
  start: null,
  end: null,
  project: null,
  activity: null,
  status: null,
  access: []
};

export class WorkOrderModel {

  constructor() {}
  list: WorkOrder[];
  loading: boolean;
  statusError: HttpErrorResponse;

  setAll(list: WorkOrder[]) {
    console.info('[work order] ambil data work order');
    console.debug(list);
    this.loading = false;
    this.list = list;
  }

  getById(id: number): WorkOrder {
    const index = this.list.findIndex(w => w.id === id);
    return index === -1 ? DEFAULT : this.list[index];
  }

  getError(): {name: string, message: string} {
    let name: string, message: string;
    name = this.statusError.name;
    message = this.statusError.message;
    if (this.statusError.error) {
        if (this.statusError.error.error) {
          name = this.statusError.error.error.name ? this.statusError.error.error.name : name;
          message = this.statusError.error.error.message ? this.statusError.error.error.message : message;
        }
    }
    return {name: name, message: message};
  }

  setError(error: HttpErrorResponse) {
    this.statusError = error;
    this.loading = false;
    console.error('[work order] terdeteksi error', this.getError());
  }

  add(value: WorkOrder) {
    console.info('[work order] tambah data work order');
    this.loading = false;
    this.list = [value, ...this.list];
    console.debug(this.list);
  }

  edit(id: number, value: WorkOrder) {
    console.info('[work order] ubah data work order, ID = ' + id);
    console.debug(value);
    this.loading = false;
    this.list = this.list.map(w => w.id === id ? value : w);
  }

  remove(id: number) {
    console.info('[work order] hapus data work order, ID = ' + id);
    this.loading = false;
    this.list = this.list.filter(w => w.id !== id);
  }

}
