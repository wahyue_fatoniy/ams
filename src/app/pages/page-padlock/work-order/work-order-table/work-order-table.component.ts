import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog
} from '@angular/material';
import { WorkOrderModel, WorkOrder } from '../work-order-model';
import { WorkOrderService } from '../work-order.service';
import {
  Action,
  TABLE_STYLE,
  Filter,
  AppModel
} from '../../../../app.component';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs/Observable';
import { WorkerService } from '../../worker/worker.service';
import { WorkerModel, Worker } from '../../worker/worker-model';
import { SiteService } from '../../../setup/site/site.service';
import { SiteModel, Site } from '../../../setup/site/site-model';
import { DatePipe } from '@angular/common';
import { DiscobjectService } from '../../../discobject/discobject.service';
import {
  DiscobjectModel,
  Discobject
} from '../../../discobject/discobject-model';
import { AccessKeyService } from '../../../access-key/access-key.service';
import {
  AccessKeyModel,
  AccessKey
} from '../../../access-key/access-key-model';
import { WorkOrderFormComponent } from '../work-order-form/work-order-form.component';
import { Moment } from 'moment';
import { SocketService } from '../../../../shared';

interface Status {
  icon: string;
  color: string;
}

interface Statuses {
  idle: Status;
  expired: Status;
  request: Status;
  approve: Status;
  reject: Status;
  finish: Status;
}

@Component({
  selector: 'app-work-order-table',
  templateUrl: './work-order-table.component.html',
  styles: [
    TABLE_STYLE,
    `
      .mat-column-status {
        width: 100px;
      }
      .mat-column-access {
        width: 180px;
      }

      mat-expansion-panel {
        width: 150px;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
      }
    `
  ]
})
export class WorkOrderTableComponent implements OnInit {
  constructor(
    private workOrderService: WorkOrderService,
    private dialog: MatDialog,
    private workerService: WorkerService,
    private siteService: SiteService,
    private discobjectService: DiscobjectService,
    private accessKeyService: AccessKeyService,
    private socket: SocketService
  ) {}

  workOrderModel: WorkOrderModel = new WorkOrderModel();
  workerModel: WorkerModel = new WorkerModel();
  siteModel: SiteModel = new SiteModel();
  discobjectModel: DiscobjectModel = new DiscobjectModel();
  accessKeyModel: AccessKeyModel = new AccessKeyModel();
  dataSource: MatTableDataSource<WorkOrder> = new MatTableDataSource([]);
  date: DatePipe = new DatePipe('id');
  title = 'Table Work Order';
  pageSize;
  pageIndex = 0;
  @Input() enabledAction: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  columns: string[] = [
    'no',
    'worker',
    'site',
    'start',
    'end',
    'project',
    'activity',
    'access',
    'status',
    'note'
  ];
  typeFilters: Filter[] = [
    { value: 'all', label: 'All Column' },
    { value: 'worker', label: 'Worker' },
    { value: 'site', label: 'Site' },
    { value: 'start', label: 'Start Date' },
    { value: 'end', label: 'End Date' },
    { value: 'project', label: 'Project' },
    { value: 'activity', label: 'Activity' },
    { value: 'status', label: 'Status' },
    { value: 'note', label: 'Note'}
  ];
  typeFilter = 'all';
  app: AppModel<WorkOrder> = new AppModel<WorkOrder>();
  status: Statuses = {
    idle: { icon: 'sentiment_dissatisfied', color: '#5bc0de' },
    request: { icon: 'person_add', color: '#f0ad4e' },
    expired: { icon: 'av_timer', color: '#ababab' },
    finish: { icon: 'done_all', color: '#5cb85c' },
    approve: { icon: 'assignment_turned_in', color: '#337ab7' },
    reject: { icon: 'close', color: '#d9534f' }
  };

  ngOnInit() {
    this.socket.streaming('work.order')
    .subscribe((result: WorkOrder) => (this.workOrderModel.edit(result.id, result), this.reloadTable()));

    this.workOrderModel.loading = true;
    Observable.forkJoin([
      this.workOrderService.getAllWorkOrders(),
      this.workerService.getAllWorkers(),
      this.siteService.getAllSites(),
      this.discobjectService.getAllDiscobjects(),
      this.accessKeyService.getAllAccessKeys()
    ]).subscribe(
      result =>
        this.onSetTable(result[0], result[1], result[2], result[3], result[4]),
      error => this.workOrderModel.setError(error)
    );
  }

  onSetTable(
    workOrders: WorkOrder[],
    workers: Worker[],
    sites: Site[],
    discobjects: Discobject[],
    accessKeys: AccessKey[]
  ) {
    this.workOrderService.verifyExpireds(workOrders).subscribe(expireds => {
      this.workerModel.setAll(workers);
      this.siteModel.setAll(sites);
      this.discobjectModel.setAll(discobjects);
      this.accessKeyModel.setAll(accessKeys);
      this.workOrderModel.setAll(expireds);
      const PAGE = localStorage.getItem('pageSize');
      // tslint:disable-next-line:radix
      this.pageSize = PAGE ? parseInt(PAGE) : 25;
      this.reloadTable();
    });
  }

  reloadTable() {
    this.dataSource = new MatTableDataSource(this.workOrderModel.list);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (value: WorkOrder, keyword: string) => {
      value.worker = this.workerModel.getById(value.worker_id).name;
      value.site = this.siteModel.getById(value.site_id).label;
      return this.app.filterPredicate(value, keyword, this.typeFilter);
    };
  }

  getAccess(id: number): AccessKey[] {
    return this.accessKeyModel.findByWorkOrder(id);
  }

  onSetPage(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    localStorage.setItem('pageSize', event.pageSize);
  }

  onClickAction(event: Action<WorkOrder>) {
    const dialogRef = this.dialog.open(WorkOrderFormComponent, {
      width: '900px',
      data: {
        discobjectModel: this.discobjectModel,
        workerModel: this.workerModel,
        siteModel: this.siteModel
      }
    });
    dialogRef.afterClosed().subscribe(result => this.onSave(result));
  }

  setDate(date, hour: number, minute: number, second: number): string {
    let value = new Date(date);
    value.setHours(hour);
    value.setMinutes(minute);
    value.setSeconds(second);
    return this.date.transform(value, 'yyyy-MM-ddTHH:mm:ss', '+00:00') + 'Z';
  }

  onSave(value: WorkOrder) {
    if (value) {
      this.workOrderModel.loading = true;
      this.workOrderService.addWorkOrder(value).subscribe(
        result => {
          this.accessKeyService
            .createAccessKey(value.access, result.id)
            .subscribe(key => {
              this.workOrderModel.loading = false;
              this.workOrderModel.add(result);
              this.accessKeyModel.concatAll(key);
              this.reloadTable();
            });
        },
        error => this.workerModel.setError(error)
      );
    }
  }

}
