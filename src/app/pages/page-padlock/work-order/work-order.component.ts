import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work-order',
  template: `
    <app-work-order-table [enabledAction]="true">
    </app-work-order-table>
  `
})
export class WorkOrderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
