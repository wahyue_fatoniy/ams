import { Component, OnInit, Inject } from '@angular/core';
import { WorkOrder } from '../work-order-model';
import { DiscobjectModel, Discobject } from '../../../discobject/discobject-model';
import { WorkerModel } from '../../worker/worker-model';
import { SiteModel, Site } from '../../../setup/site/site-model';
import { MAT_DIALOG_DATA, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from '../../worker/worker-form/worker-form.component';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';

interface WorkOrderForm {
   workerModel: WorkerModel;
   discobjectModel: DiscobjectModel;
   siteModel: SiteModel;
}

@Component({
  selector: 'app-work-order-form',
  templateUrl: './work-order-form.component.html',
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'id'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ]
})

export class WorkOrderFormComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: WorkOrderForm,
    private formBuilder: FormBuilder
  ) { }
  form: FormGroup = this.formBuilder.group({
     id : new FormControl(null),
     worker_id : new FormControl(null, [Validators.required]),
     site_id : new FormControl(null, [Validators.required]),
     start : new FormControl(null, [Validators.required]),
     end : new FormControl(null, [Validators.required]),
     project: new FormControl(null, [Validators.required]),
     activity: new FormControl(null, [Validators.required]),
     access: new FormControl([], [Validators.required])
  });
  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  site: Site;
  startMinDate: Date = new Date();
  endMinDate: Date = new Date();

  ngOnInit() {
  }

  siteChange(id: number) {
    this.site = this.data.siteModel.getById(id);
    this.form.controls.access.reset();
  }

  getPadlock(): Discobject[] {
     if (this.site ) {
        return this.data.discobjectModel
       .getPadlock(this.site.location.lat, this.site.location.lng);
     } else {
        return [];
     }
  }

}
