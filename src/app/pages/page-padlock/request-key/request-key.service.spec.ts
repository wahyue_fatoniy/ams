import { TestBed, inject } from '@angular/core/testing';

import { RequestKeyService } from './request-key.service';

describe('RequestKeyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequestKeyService]
    });
  });

  it('should be created', inject([RequestKeyService], (service: RequestKeyService) => {
    expect(service).toBeTruthy();
  }));
});
