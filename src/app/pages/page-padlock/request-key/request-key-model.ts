import { HttpErrorResponse } from '@angular/common/http';


export interface RequestKey {
  id?: number;
  work_order_id: number;
  picture: string;
}

const DEFAULT: RequestKey = {id: null, work_order_id: null, picture: null};

export class RequestKeyModel {
  constructor() {}
  list: RequestKey[];
  loading: boolean;
  statusError: HttpErrorResponse;

  setAll(list: RequestKey[]) {
    console.info('[request key] ambil data request key');
    console.debug(list);
    this.loading = false;
    this.list = list;
  }

  getById(id: number): RequestKey {
    const index = this.list.findIndex(key => key.id === id);
    return index === -1 ? DEFAULT : this.list[index];
  }

  getError(): {name: string, message: string} {
    let name: string, message: string;
    name = this.statusError.name;
    message = this.statusError.message;
    if (this.statusError.error) {
        if (this.statusError.error.error) {
          name = this.statusError.error.error.name ? this.statusError.error.error.name : name;
          message = this.statusError.error.error.message ? this.statusError.error.error.message : message;
        }
    }
    return {name: name, message: message};
  }

  setError(error: HttpErrorResponse) {
    this.statusError = error;
    this.loading = false;
    console.error('[access key history] terdeteksi error', this.getError());
  }

  add(value: RequestKey) {
    console.info('[request key] tambah data request key');
    console.debug(value);
    this.loading = false;
    this.list = [value, ...this.list];
  }

  edit(id: number, value: RequestKey) {
    console.info('[request key] ubah data request key, ID = ' + id);
    console.debug(value);
    this.loading = false;
    this.list = this.list.map(key => key.id === id ? value : key);
  }

  remove(id: number) {
    console.info('[request key] hapus data request key, ID = ' + id);
    this.loading = false;
    this.list = this.list.filter(key => key.id !== id);
    console.debug(this.list);
  }

}
