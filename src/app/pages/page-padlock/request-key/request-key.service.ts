import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { pipe } from 'rxjs/util/pipe';
import 'rxjs/add/observable/throw';
import { RequestKey } from './request-key-model';
import { catchError } from 'rxjs/operators';
const URL  = '/api/request_keys';


@Injectable()
export class RequestKeyService {

  constructor(private http: HttpClient) { }

  getAllRequestKeys(): Observable<RequestKey[]> {
    const filter  = {order: 'id DESC'};
    const encode = encodeURIComponent(JSON.stringify(filter));
    return this.http.get<RequestKey[]>(`${URL}?filter=${encode}`)
    .pipe(
      catchError(error => Observable.throw(error))
    );
  }

  removeRequestKey(id: number) {
    return this.http.delete(`${URL}/${id}`)
    .pipe(
      catchError(error => Observable.throw(error))
    );
  }

}
