import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

interface RemoveDialog {
  id: number;
  message: string;
}

@Component({
  selector: 'app-dialog-remove',
  templateUrl: './dialog-remove.component.html',
  styles: []
})
export class DialogRemoveComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: RemoveDialog) { }

  ngOnInit() {
  }

}
