import { Component, OnInit } from '@angular/core';
import { RequestKeyService } from '../request-key/request-key.service';
import { RequestKeyModel, RequestKey } from '../request-key/request-key-model';
import { WorkOrderService } from '../work-order/work-order.service';
import { WorkerService } from '../worker/worker.service';
import { SiteService } from '../../setup/site/site.service';
import { WorkOrderModel } from '../work-order/work-order-model';
import { WorkerModel } from '../worker/worker-model';
import { SiteModel } from '../../setup/site/site-model';
import { Observable } from 'rxjs/Observable';
import { MatDialog } from '@angular/material';
import { DialogRemoveComponent } from '../dialog-remove/dialog-remove.component';
import { WorkOrderHistoryService } from '../work-order-history/work-order-history.service';
import { SocketService } from '../../../shared';
import { DialogNoteComponent } from '../dialog-note/dialog-note.component';

@Component({
  selector: 'app-dashboard-padlock',
  templateUrl: './dashboard-padlock.component.html',
  styles: [
    `
      .profile {
        height: 40px;
        width: 40px;
        cursor: pointer;
        margin-bottom: 10px;
      }
    `
  ]
})
export class DashboardPadlockComponent implements OnInit {
  constructor(
    private requestService: RequestKeyService,
    private orderService: WorkOrderService,
    private workerService: WorkerService,
    private siteService: SiteService,
    private dialog: MatDialog,
    private historyService: WorkOrderHistoryService,
    private socket: SocketService
  ) {}

  requestModel: RequestKeyModel = new RequestKeyModel();
  orderModel: WorkOrderModel = new WorkOrderModel();
  workerModel: WorkerModel = new WorkerModel();
  siteModel: SiteModel = new SiteModel();

  ngOnInit() {
    this.socket.streaming('request.key').subscribe((result: RequestKey) => {
      this.requestModel.add(result);
    });
    this.requestModel.loading = true;
    Observable.forkJoin([
      this.requestService.getAllRequestKeys(),
      this.orderService.getAllWorkOrders(),
      this.workerService.getAllWorkers(),
      this.siteService.getAllSites()
    ]).subscribe(
      result => {
        this.requestModel.setAll(result[0]);
        this.orderModel.setAll(result[1]);
        this.workerModel.setAll(result[2]);
        this.siteModel.setAll(result[3]);
      },
      error => this.requestModel.setError(error)
    );
  }

  getProfile(work_order_id: number): string {
     const order = this.orderModel.getById(work_order_id);
     const profile = this.workerModel.getById(order.worker_id).profile;
     return `url('${profile}')`;
  }

  getWorker(work_order_id: number): string {
    const order = this.orderModel.getById(work_order_id);
    return this.workerModel.getById(order.worker_id).name;
  }

  getSite(work_order_id: number): string {
    const order = this.orderModel.getById(work_order_id);
    return this.siteModel.getById(order.site_id).label;
  }


  onApprove(id: number) {
     const dialogRef = this.dialog.open(DialogRemoveComponent, {
        data : {id: id, message: 'Are you sure to approve ?'}
     });
     dialogRef.afterClosed().subscribe(result => {
        if (result) {
            const orderId = this.requestModel.getById(result).work_order_id;
            Observable.forkJoin([
               this.requestService.removeRequestKey(result),
               this.orderService.editStatus(orderId, 'approve'),
               this.historyService.createHistory({ status : 'approve', work_order_id: orderId})
            ]).subscribe(
              success => this.requestModel.remove(result),
              error => this.requestModel.setError(error)
            );
        }
     });
  }


  onReject(id: number) {
    const dialogRef = this.dialog.open(DialogNoteComponent, {
       data : id
    });
    dialogRef.afterClosed().subscribe(result => {
       if (result) {
        const orderId = this.requestModel.getById(result.id).work_order_id;
        const picture = this.requestModel.getById(result.id).picture;
        Observable.forkJoin([
          this.requestService.removeRequestKey(result.id),
          this.orderService.editStatus(orderId, 'reject', result.note),
          this.historyService.createHistory({
            status : 'reject',
            work_order_id: orderId,
            note: result.note,
            picture: picture
          })
       ]).subscribe(
         success => this.requestModel.remove(result.id),
         error => this.requestModel.setError(error)
       );
       }
    });
  }

}
