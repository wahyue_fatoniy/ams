import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPadlockComponent } from './dashboard-padlock.component';

describe('DashboardPadlockComponent', () => {
  let component: DashboardPadlockComponent;
  let fixture: ComponentFixture<DashboardPadlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPadlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPadlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
