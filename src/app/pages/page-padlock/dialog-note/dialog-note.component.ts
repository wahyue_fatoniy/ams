import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-note',
  templateUrl: './dialog-note.component.html'
})

export class DialogNoteComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: number) { }

  note: string;
  ngOnInit() {
  }

}
