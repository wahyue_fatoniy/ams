import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { pipe } from 'rxjs/util/pipe';
import 'rxjs/add/observable/throw';
import { WorkOrderHistory } from './work-order-history-model';
import { catchError } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
const URL = '/api/work_order_histories';

@Injectable()
export class WorkOrderHistoryService {
  constructor(private http: HttpClient) {}
  date: DatePipe = new DatePipe('id');

  getAllWorkOrderHistories(): Observable<WorkOrderHistory[]> {
    const filter = { order: 'date DESC' };
    const encode = encodeURIComponent(JSON.stringify(filter));
    return this.http
      .get<WorkOrderHistory[]>(`${URL}?filter=${encode}`)
      .pipe(catchError(error => Observable.throw(error)));
  }

  createHistory(value: WorkOrderHistory) {
    const today = this.date.transform(new Date(), 'yyyy-MM-ddTHH:mm:ss', '+00:00') + 'Z';
    value.date  = today;
    return this.http.post(URL, value);
  }
}
