import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work-order-history',
  template: `
    <app-work-order-history-table>
    </app-work-order-history-table>
  `
})
export class WorkOrderHistoryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
