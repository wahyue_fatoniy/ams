import { HttpErrorResponse } from '@angular/common/http';
import { WorkOrder } from '../work-order/work-order-model';


export interface WorkOrderHistory {
  id?: number;
  work_order_id: number;
  status: string;
  picture?: string;
  note?: string;
  date?: Date | string;
  worker?: string;
  site?: string;
  project?: string;
}

const DEFAULT: WorkOrderHistory = {
  id: null,
  work_order_id: null,
  status: null,
  picture: null,
  note: null,
  date: null
};


export class WorkOrderHistoryModel {

  constructor() {}
  list: WorkOrderHistory[];
  loading: boolean;
  statusError: HttpErrorResponse;

  setAll(list: WorkOrderHistory[]) {
    console.info('[work order history] ambil data work order history');
    console.debug(list);
    this.loading = false;
    this.list = list;
  }

  getById(id: number): WorkOrderHistory {
    const index = this.list.findIndex(w => w.id === id);
    return index === -1 ? DEFAULT : this.list[index];
  }

  getError(): {name: string, message: string} {
    let name: string, message: string;
    name = this.statusError.name;
    message = this.statusError.message;
    if (this.statusError.error) {
        if (this.statusError.error.error) {
          name = this.statusError.error.error.name ? this.statusError.error.error.name : name;
          message = this.statusError.error.error.message ? this.statusError.error.error.message : message;
        }
    }
    return {name: name, message: message};
  }

  setError(error: HttpErrorResponse) {
    this.statusError = error;
    this.loading = false;
    console.error('[work order history] terdeteksi error', this.getError());
  }

  add(value: WorkOrderHistory) {
    console.info('[work order history] tambah data work order history');
    console.debug(value);
    this.loading = false;
    this.list = [value, ...this.list];
  }

  edit(id: number, value: WorkOrderHistory) {
    console.info('[work order history] ubah data work order history, ID = ' + id);
    console.debug(value);
    this.loading = false;
    this.list = this.list.map(w => w.id === id ? value : w);
  }

  remove(id: number) {
    console.info('[work order history] hapus data work order history, ID = ' + id);
    this.loading = false;
    this.list = this.list.filter(w => w.id !== id);
  }

}
