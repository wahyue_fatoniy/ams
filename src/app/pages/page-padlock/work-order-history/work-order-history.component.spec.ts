import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkOrderHistoryComponent } from './work-order-history.component';

describe('WorkOrderHistoryComponent', () => {
  let component: WorkOrderHistoryComponent;
  let fixture: ComponentFixture<WorkOrderHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkOrderHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkOrderHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
