import { TestBed, inject } from '@angular/core/testing';

import { WorkOrderHistoryService } from './work-order-history.service';

describe('WorkOrderHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkOrderHistoryService]
    });
  });

  it('should be created', inject([WorkOrderHistoryService], (service: WorkOrderHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
