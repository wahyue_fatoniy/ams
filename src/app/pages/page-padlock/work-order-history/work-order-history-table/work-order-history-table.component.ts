import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { TABLE_STYLE, Filter, AppModel, Action } from '../../../../app.component';
import { WorkOrderService } from '../../work-order/work-order.service';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { WorkerService } from '../../worker/worker.service';
import { SiteService } from '../../../setup/site/site.service';
import { WorkOrderModel, WorkOrder } from '../../work-order/work-order-model';
import { WorkerModel, Worker } from '../../worker/worker-model';
import { SiteModel, Site } from '../../../setup/site/site-model';
import { WorkOrderHistory, WorkOrderHistoryModel } from '../work-order-history-model';
import { DatePipe } from '@angular/common';
import { WorkOrderHistoryService } from '../work-order-history.service';
import { Observable } from 'rxjs/Observable';
import { PhotoProfileComponent } from '../../worker/worker-table/photo-profile/photo-profile.component';
import { SocketService } from '../../../../shared';

interface Status {
  icon: string;
  color: string;
}

interface Statuses {
  idle: Status;
  expired: Status;
  request: Status;
  approve: Status;
  reject: Status;
  finish: Status;
}

@Component({
  selector: 'app-work-order-history-table',
  templateUrl: './work-order-history-table.component.html',
  styles: [TABLE_STYLE, `
      .mat-column-status {
        width: 100px;
      }

      .mat-column-access {
        width: 180px;
      }

      mat-expansion-panel{
        width: 150px;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
      }
  `]
})
export class WorkOrderHistoryTableComponent implements OnInit {

  constructor(
    private workOrderService: WorkOrderService,
    private dialog: MatDialog,
    private workerService: WorkerService,
    private siteService: SiteService,
    private workOrderHistoryService: WorkOrderHistoryService,
    private socket: SocketService
  ) {}

  workOrderModel: WorkOrderModel = new WorkOrderModel();
  workerModel: WorkerModel = new WorkerModel();
  siteModel: SiteModel = new SiteModel();
  historyModel: WorkOrderHistoryModel = new WorkOrderHistoryModel();
  dataSource: MatTableDataSource<WorkOrderHistory> = new MatTableDataSource([]);
  date: DatePipe = new DatePipe('id');
  title = 'Table Work Order History';
  pageSize;
  pageIndex = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  columns: string[] = ['no', 'picture', 'worker', 'site', 'project', 'note', 'date', 'status'];
  typeFilters: Filter[] = [
    {value: 'all', label: 'All Column'}, {value: 'worker', label: 'Worker'},
    {value: 'site', label: 'Site'}, {value: 'project', label: 'Project'},
    {value: 'note', label: 'Note'}, {value: 'date', label: 'Date'},
    {value: 'status', label: 'Status'}
  ];
  typeFilter = 'all';
  app: AppModel<WorkOrderHistory> = new AppModel<WorkOrderHistory>();
  status: Statuses = {
    idle: {icon: 'sentiment_dissatisfied', color: '#5bc0de'},
    request: {icon: 'person_add', color: '#f0ad4e'},
    expired: {icon: 'av_timer', color: '#ababab'},
    finish: {icon: 'done_all', color: '#5cb85c'},
    approve: {icon: 'assignment_turned_in', color: '#337ab7'},
    reject: {icon: 'close', color: '#d9534f'}
  };

  ngOnInit() {
    this.socket.streaming('order.history')
    .subscribe((result: WorkOrderHistory) => (this.historyModel.add(result), this.reloadTable()));

    this.historyModel.loading = true;
    Observable.forkJoin([
      this.workOrderService.getAllWorkOrders(),
      this.workerService.getAllWorkers(),
      this.siteService.getAllSites(),
      this.workOrderHistoryService.getAllWorkOrderHistories()
    ]).subscribe(
      result => this.onSetTable(result[0], result[1], result[2], result[3]),
      error => this.workOrderModel.setError(error)
    );
  }

  onSetTable(
    workOrders: WorkOrder[],
    workers: Worker[],
    sites: Site[],
    histories: WorkOrderHistory[]
  ) {
    this.workerModel.setAll(workers);
    this.siteModel.setAll(sites);
    this.workOrderModel.setAll(workOrders);
    this.historyModel.setAll(histories);
    this.reloadTable();
  }

  reloadTable() {
    const PAGE = localStorage.getItem('pageSize');
    // tslint:disable-next-line:radix
    this.pageSize = PAGE ? parseInt(PAGE) : 25;
    this.dataSource = new MatTableDataSource(this.historyModel.list);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (value: WorkOrderHistory, keyword: string) => {
      value.worker = this.getWorker(value.work_order_id);
      value.site   = this.getSite(value.work_order_id);
      value.project = this.workOrderModel.getById(value.work_order_id).project;
      return this.app.filterPredicate(value, keyword, this.typeFilter);
    };
  }

  onSetPage(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    localStorage.setItem('pageSize', event.pageSize);
  }

  onClickAction(event: Action<WorkOrderHistory>) {
    console.log(event);
  }

  getWorker(work_order_id: number): string {
    const workOrder: WorkOrder = this.workOrderModel.getById(work_order_id);
    return this.workerModel.getById(workOrder.worker_id).name;
  }

  getSite(work_order_id: number): string {
    const workOrder: WorkOrder = this.workOrderModel.getById(work_order_id);
    return this.siteModel.getById(workOrder.site_id).label;
  }

  onClickProfile(id: number) {
    const value: WorkOrderHistory = this.historyModel.getById(id);
    const dialogRef = this.dialog.open(PhotoProfileComponent, {
        data : {
          title: this.getWorker(value.work_order_id), link: value.picture
        }
    });
  }

}
