import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessKeyHistoryTableComponent } from './access-key-history-table.component';

describe('AccessKeyHistoryTableComponent', () => {
  let component: AccessKeyHistoryTableComponent;
  let fixture: ComponentFixture<AccessKeyHistoryTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessKeyHistoryTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessKeyHistoryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
