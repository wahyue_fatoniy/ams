import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { AccessKeyHistory, AccessKeyHistoryModel } from '../access-key-history-model';
import { AccessKeyHistoryService } from '../access-key-history.service';
import { AccessKeyService } from '../../../access-key/access-key.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Filter, AppModel, TABLE_STYLE } from '../../../../app.component';
import { DiscobjectService } from '../../../discobject/discobject.service';
import { WorkOrderService } from '../../work-order/work-order.service';
import { WorkerService } from '../../worker/worker.service';
import { SiteService } from '../../../setup/site/site.service';
import { Observable } from 'rxjs/Observable';
import { AccessKey, AccessKeyModel } from '../../../access-key/access-key-model';
import { DiscobjectModel, Discobject } from '../../../discobject/discobject-model';
import { WorkOrderModel, WorkOrder } from '../../work-order/work-order-model';
import { WorkerModel, Worker } from '../../worker/worker-model';
import { SiteModel, Site } from '../../../setup/site/site-model';
import { SocketService } from '../../../../shared';

@Component({
  selector: 'app-access-key-history-table',
  templateUrl: './access-key-history-table.component.html',
  styles: [TABLE_STYLE, `
  .mat-column-status {
    width: 100px;
  }
  .mat-column-access {
    width: 180px;
  }

  mat-expansion-panel{
    width: 150px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
  }
`]
})

export class AccessKeyHistoryTableComponent implements OnInit {

  constructor(
    private historiesService: AccessKeyHistoryService,
    private accessService: AccessKeyService,
    private discService: DiscobjectService,
    private orderService: WorkOrderService,
    private workerService: WorkerService,
    private siteService: SiteService,
    private socket: SocketService
  ) {}

  historyModel: AccessKeyHistoryModel = new AccessKeyHistoryModel();
  accessModel: AccessKeyModel = new AccessKeyModel();
  discModel: DiscobjectModel = new DiscobjectModel();
  orderModel: WorkOrderModel = new WorkOrderModel();
  workerModel: WorkerModel = new WorkerModel();
  siteModel: SiteModel = new SiteModel();

  dataSource: MatTableDataSource<AccessKeyHistory> = new MatTableDataSource([]);
  title = 'Table Access Key History';
  pageSize;
  pageIndex = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  columns: string[] = ['no', 'site', 'name', 'worker', 'project', 'date'];
  typeFilters: Filter[] = [
    {value: 'all', label: 'All Column'}, {value: 'site', label: 'Site'},
    {value: 'name', label: 'Name'}, {value: 'worker', label: 'Worker'},
    {value: 'project', label: 'Project'}, {value: 'date', label: 'Date'}
  ];
  typeFilter = 'all';
  app: AppModel<AccessKeyHistory> = new AppModel<AccessKeyHistory>();

  ngOnInit() {
    this.socket.streaming('key.history')
    .subscribe((result: AccessKeyHistory) => (this.historyModel.add(result), this.reloadTable()));
    this.historyModel.loading = true;
    Observable.forkJoin([
        this.historiesService.getAllAccessKeyHistories(),
        this.accessService.getAllAccessKeys(),
        this.discService.getAllDiscobjects(),
        this.orderService.getAllWorkOrders(),
        this.workerService.getAllWorkers(),
        this.siteService.getAllSites()
    ]).subscribe(
      result => this.onSetTable(result[0], result[1], result[2], result[3], result[4], result[5]),
      error => this.historyModel.setError(error)
    );
  }

  onSetTable(
    histories: AccessKeyHistory[],
    keys: AccessKey[],
    discobjs: Discobject[],
    orders: WorkOrder[],
    workers: Worker[],
    sites: Site[]
  ) {
      this.historyModel.setAll(histories);
      this.accessModel.setAll(keys);
      this.discModel.setAll(discobjs);
      this.orderModel.setAll(orders);
      this.workerModel.setAll(workers);
      this.siteModel.setAll(sites);
      this.reloadTable();
  }


  reloadTable() {
    const PAGE = localStorage.getItem('pageSize');
    // tslint:disable-next-line:radix
    this.pageSize = PAGE ? parseInt(PAGE) : 25;
    this.dataSource = new MatTableDataSource(this.historyModel.list);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (value: AccessKeyHistory, keyword: string) => {
      value.worker = this.getWorker(value.access_key_id);
      value.site = this.getSite(value.access_key_id);
      value.name = this.getName(value.access_key_id);
      value.project = this.getProject(value.access_key_id);
      return this.app.filterPredicate(value, keyword, this.typeFilter);
    };
  }


  getWorker(access_key_id: number): string {
    const access = this.accessModel.getById(access_key_id);
    const order = this.orderModel.getById(access.work_order_id);
    return this.workerModel.getById(order.worker_id).name;
  }

  getProject(access_key_id: number): string {
    const access = this.accessModel.getById(access_key_id);
    return this.orderModel.getById(access.work_order_id).project;
  }

  getSite(access_key_id: number): string {
    const access = this.accessModel.getById(access_key_id);
    const order = this.orderModel.getById(access.work_order_id);
    return this.siteModel.getById(order.site_id).label;
  }

  getName(access_key_id: number): string {
    const access = this.accessModel.getById(access_key_id);
    return this.discModel.getById(access.discobj_id).name;
  }

  onSetPage(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    localStorage.setItem('pageSize', event.pageSize);
  }

}
