import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { pipe } from 'rxjs/util/pipe';
import 'rxjs/add/observable/throw';
import { AccessKeyHistory } from './access-key-history-model';
import { catchError } from 'rxjs/operators';
const URL  = '/api/access_key_histories';


@Injectable()
export class AccessKeyHistoryService {

  constructor(private http: HttpClient) { }

  getAllAccessKeyHistories(): Observable<AccessKeyHistory[]> {
    const filter  = {order: 'date DESC'};
    const encode = encodeURIComponent(JSON.stringify(filter));
    return this.http.get<AccessKeyHistory[]>(`${URL}?filter=${encode}`)
    .pipe(
      catchError(error => Observable.throw(error))
    );
  }

}
