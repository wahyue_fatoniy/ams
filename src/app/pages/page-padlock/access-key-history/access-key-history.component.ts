import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-access-key-history',
  template: `
    <app-access-key-history-table></app-access-key-history-table>
  `
})

export class AccessKeyHistoryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
