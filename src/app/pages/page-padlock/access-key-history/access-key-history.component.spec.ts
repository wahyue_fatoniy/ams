import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessKeyHistoryComponent } from './access-key-history.component';

describe('AccessKeyHistoryComponent', () => {
  let component: AccessKeyHistoryComponent;
  let fixture: ComponentFixture<AccessKeyHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessKeyHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessKeyHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
