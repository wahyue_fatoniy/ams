import { HttpErrorResponse } from '@angular/common/http';

export interface AccessKeyHistory {
  id?: number;
  access_key_id: number;
  date: Date;
  worker?: string;
  site?: string;
  name?: string;
  project?: string;
}

const DEFAULT: AccessKeyHistory = {id: null, access_key_id: null, date: null};

export class AccessKeyHistoryModel {
  constructor() {}
  list: AccessKeyHistory[];
  loading: boolean;
  statusError: HttpErrorResponse;

  setAll(list: AccessKeyHistory[]) {
    console.info('[access key history] ambil data access key history');
    console.debug(list);
    this.loading = false;
    this.list = list;
  }

  getById(id: number): AccessKeyHistory {
    const index = this.list.findIndex(key => key.id === id);
    return index === -1 ? DEFAULT : this.list[index];
  }

  getError(): {name: string, message: string} {
    let name: string, message: string;
    name = this.statusError.name;
    message = this.statusError.message;
    if (this.statusError.error) {
        if (this.statusError.error.error) {
          name = this.statusError.error.error.name ? this.statusError.error.error.name : name;
          message = this.statusError.error.error.message ? this.statusError.error.error.message : message;
        }
    }
    return {name: name, message: message};
  }

  setError(error: HttpErrorResponse) {
    this.statusError = error;
    this.loading = false;
    console.error('[access key history] terdeteksi error', this.getError());
  }

  add(value: AccessKeyHistory) {
    console.info('[access key history] tambah data access key history');
    console.debug(value);
    this.loading = false;
    this.list = [value, ...this.list];
  }

  edit(id: number, value: AccessKeyHistory) {
    console.info('[access key history] ubah data access key history, ID = ' + id);
    console.debug(value);
    this.loading = false;
    this.list = this.list.map(key => key.id === id ? value : key);
  }

  remove(id: number) {
    console.info('[access key history] hapus data access key history, ID = ' + id);
    this.loading = false;
    this.list = this.list.filter(key => key.id !== id);
  }

}
