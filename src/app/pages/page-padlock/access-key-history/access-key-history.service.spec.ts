import { TestBed, inject } from '@angular/core/testing';

import { AccessKeyHistoryService } from './access-key-history.service';

describe('AccessKeyHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccessKeyHistoryService]
    });
  });

  it('should be created', inject([AccessKeyHistoryService], (service: AccessKeyHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
