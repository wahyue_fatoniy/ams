import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';
import { AppRouterModule } from './app.router';
import { environment } from '../environments/environment';
import { PagesModule } from './pages/pages.module';
import { AaaModule } from '../aaa/aaa.module';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    RouterModule,
    AppRouterModule,
    PagesModule,
    AaaModule.forRoot(),
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {}
