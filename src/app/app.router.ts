import { Routes, RouterModule, Router } from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    CallbackComponent,
    LoginComponent,
    PublicComponent,
        MapViewComponent,
        SetupComponent,
            ProvinceComponent,
            CityComponent,
            DistrictComponent,
            SiteComponent,
            EquipmentTypeComponent,
            PerfObjectComponent,
            SecurityComponent,
            ChannelComponent,
    PageSiteComponent,
        AlarmComponent,
        AlarmSiteComponent,
        HistoryComponent,
        ReportComponent,
        ManagedObjectComponent,
        HomeComponent,
        ViewComponent,
        MappingSensorComponent,
        PerfCounterComponent,
        ProvisioningComponent,
        ProvisioningHistoryComponent,
        ViewV2Component
} from './pages';
import { AuthGuard, AuthService } from './shared';
import { DashboardComponent } from './pages/home/dashboard/dashboard.component';
import { PagePadlockComponent } from './pages/page-padlock/page-padlock.component';
import { DashboardPadlockComponent } from './pages/page-padlock/dashboard-padlock/dashboard-padlock.component';
import { WorkerComponent } from './pages/page-padlock/worker/worker.component';
import { WorkOrderComponent } from './pages/page-padlock/work-order/work-order.component';
import { AccessKeyHistoryComponent } from './pages/page-padlock/access-key-history/access-key-history.component';
import { WorkOrderHistoryComponent } from './pages/page-padlock/work-order-history/work-order-history.component';

const ROUTES: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'callback', component: CallbackComponent},
    {path: 'public', component: PublicComponent, canActivate: [AuthGuard], children: [
        {path: '', redirectTo: 'map', pathMatch: 'full'},
        {path: 'map', component: MapViewComponent, canActivate: [AuthGuard]},
        {path: 'setup', component: SetupComponent, canActivate: [AuthGuard], children: [
            {path: '', redirectTo: 'province', pathMatch: 'full'},
            {path: 'province', component: ProvinceComponent, canActivate: [AuthGuard]},
            {path: 'district', component: DistrictComponent, canActivate: [AuthGuard]},
            {path: 'city', component: CityComponent, canActivate: [AuthGuard]},
            {path: 'site', component: SiteComponent, canActivate: [AuthGuard]},
            {path: 'equipment-type', component: EquipmentTypeComponent, canActivate: [AuthGuard]},
            {path: 'perf-object', component: PerfObjectComponent, canActivate: [AuthGuard]},
            {path: 'security', component: SecurityComponent, canActivate: [AuthGuard]},
            {path: 'channel', component: ChannelComponent, canActivate: [AuthGuard]}
        ]}
    ]},
    {
      path: 'padlock', component: PagePadlockComponent, canActivate: [AuthGuard], children: [
        {path: '', redirectTo: 'dashboard-padlock', pathMatch: 'full'},
        {path: 'dashboard-padlock', component: DashboardPadlockComponent, canActivate: [AuthGuard]},
        {path: 'worker', component: WorkerComponent, canActivate: [AuthGuard]},
        {path: 'work-order', component: WorkOrderComponent, canActivate: [AuthGuard]},
        {path: 'work-order-history', component: WorkOrderHistoryComponent, canActivate: [AuthGuard]},
        {path: 'access-key-history', component: AccessKeyHistoryComponent, canActivate: [AuthGuard]}
      ]
    },
    { path : 'home', component: HomeComponent, canActivate : [AuthGuard], children: [
        {path: '', redirectTo: 'alarm', pathMatch: 'full'},
        {path: 'alarm', component: AlarmComponent, canActivate: [AuthGuard]},
        {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
        {path: 'site', component: PageSiteComponent, canActivate: [AuthGuard], children: [
            {path: '', redirectTo: 'view', pathMatch: 'full'},
            {path: 'view', component: ViewComponent, canActivate: [AuthGuard]},
            {path: 'mapping-sensor', component: MappingSensorComponent, canActivate: [AuthGuard]},
            {path: 'perf-counter', component: PerfCounterComponent, canActivate: [AuthGuard]},
            {path: 'alarm-site', component: AlarmSiteComponent, canActivate: [AuthGuard]},
            {path: 'history', component: HistoryComponent, canActivate: [AuthGuard]},
            {path: 'report', component: ReportComponent, canActivate: [AuthGuard]},
            {path: 'managed-object', component: ManagedObjectComponent, canActivate: [AuthGuard]},
            {path: 'provisioning', component: ProvisioningComponent, canActivate: [AuthGuard]},
            {path: 'provisioning-history', component: ProvisioningHistoryComponent, canActivate: [AuthGuard]}
        ]},
    ]}
];

@NgModule({
    imports : [
        CommonModule,
        RouterModule.forRoot(ROUTES, {useHash: true}),
    ],
    exports : [
        RouterModule
    ]
})

export class AppRouterModule {
    constructor(public router: Router, public auth: AuthService) {
        this.router.errorHandler = (error: any) => {};
        this.router.config.unshift({
          path: '', redirectTo: this.auth.getRedirect(), pathMatch: 'full'
        });
    }
}
