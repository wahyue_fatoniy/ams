import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AngularMaterialModule } from '../angular-material.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule
  ],
  exports : [
    NavbarComponent,
    SidebarComponent,
    AngularMaterialModule,
    RouterModule
  ],
  declarations: [SidebarComponent, NavbarComponent]
})
export class TemplateModule { }
