import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgmCoreModule } from '@agm/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { FileUploadModule } from 'ng2-file-upload';
import { ChartsModule } from 'ng2-charts';
import { ChartModule } from 'angular-highcharts';
import { NgxGaugeModule } from 'ngx-gauge';
import { NgxCarouselModule } from 'ngx-carousel';
import { DragulaModule } from 'ng2-dragula';


export class CustomOption extends ToastOptions {
    animate = 'flyRight';
    showCloseButton = true;
}


@NgModule({
    imports : [
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD5DTq_C3F4HLDcRYbYPJKkVY4_Z16Xm5Y'
        }),
        ToastModule.forRoot(),
        FileUploadModule
    ],
    declarations : [],
    exports : [
        PerfectScrollbarModule,
        AgmCoreModule,
        ToastModule,
        FileUploadModule,
        ChartsModule,
        ChartModule,
        AgmCoreModule,
        PerfectScrollbarModule,
        NgxGaugeModule,
        NgxCarouselModule,
        DragulaModule,
        FileUploadModule,
        ToastModule,
    ],
    providers : [
        {provide: ToastOptions, useClass: CustomOption}
    ],
})

export class ToolsModule {}
