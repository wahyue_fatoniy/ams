import { Component, OnInit } from '@angular/core';
import { AuthService} from './shared';
import { DatePipe } from '@angular/common';
export interface Filter {
  value: string;
  label: string;
}

export interface Action<T> {
  actionType: string;
  value: T;
}

export interface Location {
  lat: number;
  lng: number;
}

export class AppModel<T> {

  constructor() {}
  format: DatePipe = new DatePipe('id');

  filterPredicate(data: T, keyword: string, typeFilter: string): boolean {
    let result, value = JSON.parse(JSON.stringify(data));
    value['start'] = value['start'] ? this.format.transform(value['start'], 'longDate') : undefined;
    value['end']   = value['end'] ? this.format.transform(value['end'], 'longDate') : undefined;
    value['date']  = value['date'] ? this.format.transform(value['date'], 'dd MMMM yyyy HH:mm:ss') : undefined;

    if (typeFilter === 'all') {
      result = JSON.stringify(value);
    } else {
      result = value[ typeFilter ];
    }
    result = result.toString().trim().toLowerCase();
    return result.indexOf(keyword) !== -1;
  }

}

export const TABLE_STYLE = `
  .mat-table {
    display: table;
    > .mat-header-row, > .mat-row {
          display: table-row;
          padding: 0;
          border: none;
          > .mat-header-cell, > .mat-cell {
              display: table-cell;
              height: 48px;
              vertical-align: middle;
              border-bottom: 1px solid rgba(0, 0, 0, 0.12);
          }
      }
    }

  .profile {
    height: 40px;
    width: 40px;
    cursor: pointer;
  }
`;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(public auth: AuthService) {}

  ngOnInit() {
    this.auth.checkAuthentication();
  }
}
