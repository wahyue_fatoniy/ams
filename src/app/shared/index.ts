// global variable
export * from '../global.variable';

// aaa
export * from '../../aaa/services/auth.service';
export * from '../../aaa/guards/auth-scope.guard';
export * from '../../aaa/guards/auth.guard';


// components
export * from './components/table/table.component';
export * from './components/error-handler/error-handler.component';
export * from './components/form-v2/form-v2.component';
export * from './components/form-v2/interface';
export * from './components/map/map.component';
export * from './components/dialog/dialog.component';
export * from './components/form-map-dialog/form-map-dialog.component';
export * from './components/table-expansion/table-expansion.component';
export * from './components/bar-chart/bar-chart.component';
export * from './components/dialog/dialog.component';
export * from './components/form-dashboard-dialog/form-dashboard-dialog.component';
export * from './components/form-map-dialog/form-map-dialog.component';
export * from './components/form-v2/form-v2.component';
export * from './components/line-chart-v2/line-chart-v2.component';
export * from './components/map/map.component';
export * from './components/map-area-v2/map-area-v2.component';
export * from './components/map-building/map-building.component';
export * from './components/table-expansion/table-expansion.component';
export * from './components/widget-ac/widget-ac.component';
export * from './components/widget-atm/widget-atm.component';
export * from './components/widget-cashier/widget-cashier.component';
export * from './components/widget-chiller/widget-chiller.component';
export * from './components/widget-door/widget-door.component';
export * from './components/widget-lamp/widget-lamp.component';
export * from './components/widget-lock/widget-lock.component';
export * from './components/widget-provisioning/widget-provisioning.component';
export * from './components/widget-pump/widget-pump.component';
export * from './components/widget-rack/widget-rack.component';
export * from './components/widget-store-v2/widget-store-v2.component';
export * from './components/widget-temperature/widget-temperature.component';
export * from './components/widget-template1/widget-template1.component';
export * from './components/widget-template2/widget-template2.component';
export * from './components/form-dashboard-dialog/form-dashboard-dialog.component';
export * from './components/widget-store-v2/widget-store-v2.component';
export * from './components/form-provisioning-dialog/form-provisioning-dialog.component';
export * from './components/dialog-image/dialog-image.component';


// pipes
export * from './pipes/clear-alarm.pipe';
export * from './pipes/detect-image.pipe';
export * from './pipes/url-encode.pipe';
export * from './pipes/format-date.pipe';
export * from './pipes/filter-data.pipe';
export * from './pipes/sort-data.pipe';
export * from './pipes/convertion-value.pipe';
export * from './pipes/info-highchart.pipe';

// services
export * from './services/socket.service';
export * from './services/storage.service';


// models
export interface GeoPoint {
    lat: number;
    lng: number;
}

export interface Province {
    id ?: number;
    label: string;
    location: GeoPoint;
    zoom: number;
    cities  ?: City[];
}

export interface City {
    id ?: number;
    label: string;
    location: GeoPoint;
    zoom: number;
    province_id: number;
    province   ?: Province;
    districts  ?: District[];
}

export interface District {
    id ?: number;
    label: string;
    location: GeoPoint;
    zoom: number;
    city_id: number;
    city    ?: City;
    sites   ?: Site[];
}

export interface Site {
    id ?: number;
    label: string;
    location: GeoPoint;
    zoom: number;
    address: string;
    district_id: number;
    discobj_id: number;
    district   ?: District;
    alarm ?: boolean;
    managedObjects ?: ManagedObject[];
    items ?: Item[];
}

export interface Item {
    id ?: number;
    top: number;
    left: number;
    type: string;
    site_id: number;
}

export interface EquipmentType {
    id ?: number;
    type: string;
    brand: string;
    series: string;
}

export interface ManagedObject {
    id ?: number;
    site_id: number;
    discobj_id: number;
    site ?: Site;
    top: number;
    left: number;
}


export interface Location {
    latitude: number;
    longitude: number;
}

export interface Parent {
    id: number;
    name ?: string;
    location?: Location;
    eqType?: EquipmentType;
    parent  ?: any;
}

export interface InputEquipmentType {
    id: number;
    type?: string;
    brand?: string;
    series?: string;
}

export interface Discobject {
    id ?: number;
    name: string;
    location: Location;
    eqType: InputEquipmentType;
    parent  ?: Parent;
}

export interface PerfObject {
    id ?: number;
    name: string;
    unit: string;
    period: string;
}


export interface Alarm {
    id ?: number;
    alarmObj: Discobject;
    type: string;
    severity: string;
    description: string;
    actTime: string;
    ackTime: null | string;
    ackUser: null | string;
    clearTime ?: string;
}

export interface Security {
    id ?: number;
    name: string;
    username: string;
    email: string;
    phone: string;
    securitySites ?: SecuritySite[];
    password ?: string;
}

export interface SecuritySite {
    id ?: number;
    security_id: number;
    site_id: number;
    security ?: Security;
    site ?: Site;
}

export interface ChannelType {
    id ?: number;
    label: string;
    channels ?: Channel[];
}

export interface Channel {
    id ?: number;
    label: string;
    channel_type_id: number;
    channelType ?: ChannelType;
}

export interface Sensor {
    id ?: number;
    label: string;
    site_id: number;
    channel_type_id: number;
    channelType ?: ChannelType;
    mappingSensors ?: MappingSensor[];
}

export interface MappingSensor {
    id ?: number;
    sensor_id: number;
    channel_id: number;
    discobj_id: number;
    channel ?: Channel;
}

export interface InputDiscobject {
    id: number;
    name ?: string;
    location ?: Location;
    eqType  ?: EquipmentType;
    parent  ?: Parent;
}

export interface InputPerfObject {
    id: number;
    name ?: string;
    unit ?: string;
    period ?: string;
}

export interface PerfCounter {
    id ?: number;
    discObj: InputDiscobject;
    perfObj: InputPerfObject;
}

export interface PerfMeasurement {
    id ?: number;
    counter: PerfCounter;
    measurementTime: string;
    measurementValue: number;
}
