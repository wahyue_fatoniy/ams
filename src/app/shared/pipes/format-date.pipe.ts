import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDate'
})

export class FormatDatePipe implements PipeTransform {
  transform(date: any, format: string): any {
    const d = new Date(date);
    const now = d.toString();
    const months = {
      Jan: '01',
      Feb: '02',
      Mar: '03',
      Apr: '04',
      May: '05',
      Jun: '06',
      Jul: '07',
      Aug: '08',
      Sep: '09',
      Oct: '10',
      Nov: '11',
      Dec: '12'
    };
    const listMonths = {
      Jan: 'January',
      Feb: 'February',
      Mar: 'March',
      Apr: 'April',
      May: 'May',
      Jun: 'June',
      Jul: 'July',
      Aug: 'August',
      Sep: 'September',
      Oct: 'October',
      Nov: 'November',
      Dec: 'December'
    };

    const config = {
      YYYY: now.substr(11, 4),
      MMMM: listMonths[now.substr(4, 3)],
      MM: months[now.substr(4, 3)],
      DD: now.substr(8, 2),
      hh: now.substr(16, 2),
      mm: now.substr(19, 2),
      ss: now.substr(22, 2)
    };
    // format
    let myDate = format;
    // tslint:disable-next-line:forin
    for (const i in config) {
      myDate = myDate.split(i).join(config[i]);
    }
    return myDate;
  }
}
