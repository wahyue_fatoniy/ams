import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterData'
})
export class FilterDataPipe implements PipeTransform {

  validationDetail(list, filter) {
    const result = [];
    if (list.length != 0) {
       for (const i in list) {
          let valid = true;
          for (const j in filter) {
             if (list[i][j]  != filter[j]) {
                valid = false;
             }
          }
          if (valid) {
             result.push(list[i]);
          }
       }
    }
    return result;
  }

  transform(items: Array<any>, filter: any, type: string): any {
    let value = [];
    if (filter != undefined) {
        if (filter !== []) {
            switch (type) {
              case 'default':
                value = items.filter(item =>
                  JSON.stringify(item).toLowerCase().indexOf(filter.toLowerCase()) !== -1
                );
              break;

              case 'detail':
                    value = this.validationDetail(items, filter);
              break;
          }
        }
    }
    return value;
   }

}
