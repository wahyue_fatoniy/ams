import { Pipe, PipeTransform } from '@angular/core';
import { Alarm } from '../index';
@Pipe({
  name: 'clearAlarm'
})
export class ClearAlarmPipe implements PipeTransform {

  transform(list: Alarm[], newAlarm: Alarm): Alarm[] {
    if(newAlarm.severity === 'CLEAR'){
      list = list.filter(res=>res.id !== newAlarm.id);
    }
    return list;
  }

}
