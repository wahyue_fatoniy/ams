import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ConvertionValuePipe } from './convertion-value.pipe';

export interface InfoHighchart {
  time: string;
  value: number;
  perfType: string;
  eqType: string;
  managedObj: string;
}

@Pipe({
  name: 'infoHighchart'
})
export class InfoHighchartPipe implements PipeTransform {

  date: DatePipe = new DatePipe('id');


  transform(config: InfoHighchart): any {
    const units = {'Temperatur Ruang': '°C', 'Temperatur Coil': '°C', 'Daya': 'W', 'Kelembapan Relatif': '%'};
    let unit  = units[config.perfType];
    if (unit === 'W') {
        const convert = new ConvertionValuePipe().transform(config.value, unit);
        unit  = convert.unit;
        config.value = convert.converted;
    }
    return `
      <strong>Time</strong> : ${this.date.transform(config.time, 'medium')}</br>
      <strong>Type</strong> : ${config.eqType} </br>
      <strong>Value</strong> : ${config.value} ${unit} </br>
      <strong>Managed Object </strong> : ${config.managedObj}
    `;
  }

}
