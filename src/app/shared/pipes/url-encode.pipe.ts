import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'urlEncode'
})
export class UrlEncodePipe implements PipeTransform {

  transform(url: any): string {
    return encodeURIComponent(JSON.stringify(url));
  }

}
