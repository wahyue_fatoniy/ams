import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'detectImage'
})

export class DetectImagePipe implements PipeTransform {

  transform(value: string) {
    if (value != null && value !== undefined) {
      value = value.toString();
      const indexJpg = value.indexOf('.jpg');
      const indexPng = value.indexOf('.png');
      if (indexJpg !== -1 || indexPng !== -1) {
        const labels = value.split(' ');
        let link = '';
        let description = '';

        labels.forEach(label => {
          if (label.indexOf('.jpg') !== -1 || label.indexOf('.png') !== -1) {
            link = label;
          } else {
            description += `${label} `;
          }
        });
        return {
          link: true,
          value: `/api/containers/security/download/${link}`,
          description: description,
          image: link
        };
      } else {
        return { link: false };
      }
    } else {
      return { link: false };
    }
  }

}
