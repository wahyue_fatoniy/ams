import { Pipe, PipeTransform } from '@angular/core';

export interface Convert {
  value: number;
  converted: number;
  unit: string;
}

@Pipe({
  name: 'convertionValue'
})
export class ConvertionValuePipe implements PipeTransform {

  transform(value: any, unit: string): Convert {
    const result: any = {};
    result.value = value;
    if (value > 0 && value < 1) {
      value = parseFloat(value.toFixed(3));
      unit = unit;
    } else if ( value >= 1 && value < 1000 ) {
      value  = parseFloat(value.toFixed(1));
      unit = unit;
    } else if (value >= 1000 && value < 1000000) {
      value /= 1000;
      value  = parseFloat(value.toFixed(1));
      unit   = `K${unit}`;
    } else if (value >= 1000000 && value < 1000000000) {
      value /= 1000000;
      value  = parseFloat(value.toFixed(1));
      unit   = `M${unit}`;
    } else if (value >= 1000000000 && value < 1000000000000) {
      value /= 1000000000;
      value  = parseFloat(value.toFixed(1));
      unit   = `G${unit}`;
    } else if (value >= 1000000000000) {
      value /= 1000000000000;
      value  = parseFloat(value.toFixed(1));
      unit   = `T${unit}`;
    }
    result.converted = value;
    result.unit = unit;
    return result;
  }

}
