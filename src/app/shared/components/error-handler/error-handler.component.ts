import { Component, ViewContainerRef, OnChanges} from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-error-handler',
  templateUrl: './error-handler.component.html',
  styleUrls: ['./error-handler.component.css']
})
export class ErrorHandlerComponent {

  constructor(private toastr: ToastsManager, vRef: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vRef);
  }

  open(config: HttpErrorResponse) {
    console.log(config);
    const message: string =  config.error.error == undefined ? config.message : config.error.error.message;
    this.toastr.error(message, config.statusText, {
        toastLife: 10000,
        animate : 'flyRight',
        showCloseButton : true
    });
  }

}
