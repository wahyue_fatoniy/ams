import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-widget-door',
  templateUrl: './widget-door.component.html',
  styleUrls: ['./widget-door.component.css']
})
export class WidgetDoorComponent implements OnInit {

  constructor() { }
  @Input() type: any;
  @Input() position: any;
  @Input() label: any;

  color: string;
  set: any;

  config = {
  		'door-left1'   : { css1: 'door1-left' },
  		'door-top1'    : { css1: 'door1-top'},
  		'door-right1'  : { css1: 'door1-right'},
  		'door-bottom1' : { css1: 'door1-bottom'},
  		'door-left2'   : { css: 'door-left', css1: 'door-left1' },
  		'door-top2'    : { css: 'door-top', css1: 'door-top1' },
  		'door-right2'  : { css: 'door-right', css1: 'door-right1' },
  		'door-bottom2' : { css: 'door-bottom', css1: 'door-bottom1' },
  };

  status: any = { alarm: 'danger', open: 'open', normal: 'normal' };

  ngOnInit() {
  	this.set = this.config[this.type];
  	this.color = 'normal';
  }

}
