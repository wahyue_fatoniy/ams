import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetDoorComponent } from './widget-door.component';

describe('WidgetDoorComponent', () => {
  let component: WidgetDoorComponent;
  let fixture: ComponentFixture<WidgetDoorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetDoorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetDoorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
