import { Component, OnInit, Input } from '@angular/core';

export interface WidgetType2 {
  unit: string;
  min: number;
  max: number;
  label: string;
  icon: string;
}

@Component({
  selector: 'app-widget-template2',
  templateUrl: './widget-template2.component.html',
  styles: []
})
export class WidgetTemplate2Component implements OnInit {

  constructor() { }
  @Input() min: number;
  @Input() max: number;
  @Input() unit: string;
  @Input() icon: string;
  @Input() label: string;

  ngOnInit() {
  }

}
