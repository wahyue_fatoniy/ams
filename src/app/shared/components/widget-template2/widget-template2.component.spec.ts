import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetTemplate2Component } from './widget-template2.component';

describe('WidgetTemplate2Component', () => {
  let component: WidgetTemplate2Component;
  let fixture: ComponentFixture<WidgetTemplate2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetTemplate2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTemplate2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
