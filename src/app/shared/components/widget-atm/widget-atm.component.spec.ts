import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetAtmComponent } from './widget-atm.component';

describe('WidgetAtmComponent', () => {
  let component: WidgetAtmComponent;
  let fixture: ComponentFixture<WidgetAtmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetAtmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetAtmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
