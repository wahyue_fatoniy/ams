import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetCashierComponent } from './widget-cashier.component';

describe('WidgetCashierComponent', () => {
  let component: WidgetCashierComponent;
  let fixture: ComponentFixture<WidgetCashierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetCashierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetCashierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
