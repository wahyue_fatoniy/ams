import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-widget-cashier',
  templateUrl: './widget-cashier.component.html',
  styleUrls: ['./widget-cashier.component.css']
})
export class WidgetCashierComponent implements OnInit {

  constructor() { }
  @Input() label: any;

  ngOnInit() {}

}
