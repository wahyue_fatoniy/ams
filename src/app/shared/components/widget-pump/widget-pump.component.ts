import { Component, OnInit, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'app-widget-pump',
  templateUrl: './widget-pump.component.html',
  styleUrls: ['./widget-pump.component.css']
})
export class WidgetPumpComponent implements OnInit {

  constructor() { }

  @Input() saverity: number;
  @Input() power: boolean;
  @Input() lifeTime: any;
  @Input() label: any;

  status: any = ['clear', 'warning', 'minor', 'major', 'critical'];
  imageUrl: string;

  image = {
      critical : '../assets/img/iconArea/pump_critical.png',
      off      : '../assets/img/iconArea/pump_off.png',
      clear    : '../assets/img/iconArea/pump_normal.png',
      major    : '../assets/img/iconArea/pump_major.png'
  };

  ngOnInit() {
    if (this.power == false) {
        this.imageUrl = '../assets/img/iconArea/pump_off.png';
    } else {
      this.imageUrl = this.image[this.status[this.saverity]];
    }
  }

  ngOnChanges() {

  }

}
