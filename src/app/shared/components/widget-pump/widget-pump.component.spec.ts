import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetPumpComponent } from './widget-pump.component';

describe('WidgetPumpComponent', () => {
  let component: WidgetPumpComponent;
  let fixture: ComponentFixture<WidgetPumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetPumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetPumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
