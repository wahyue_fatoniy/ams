import { Component, Output, EventEmitter, Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-dialog-image',
  template: '',
  styleUrls: ['./dialog-image.component.css']
})
export class DialogImageComponent {

  constructor(public dialog: MatDialog) { }

  open(link: string) {
    const dialogRef = this.dialog.open(DialogImage, {
      data: link,
      width : '600px;'
    });
  }

}



@Component({
  templateUrl: './dialog-image.component.html'
})


// tslint:disable-next-line:component-class-suffix
export class DialogImage {

  constructor(public dialogRef: MatDialogRef<DialogImageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogImage) { }

}
