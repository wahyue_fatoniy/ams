import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetChillerComponent } from './widget-chiller.component';

describe('WidgetChillerComponent', () => {
  let component: WidgetChillerComponent;
  let fixture: ComponentFixture<WidgetChillerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetChillerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetChillerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
