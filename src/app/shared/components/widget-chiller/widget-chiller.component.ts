import { Component, OnInit, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'app-widget-chiller',
  templateUrl: './widget-chiller.component.html',
  styleUrls: ['./widget-chiller.component.css']
})
export class WidgetChillerComponent implements OnInit {

  constructor() { }
  @Input() saverity: number;
  @Input() power: boolean;
  @Input() temp_material: any;
  @Input() temp_setting: any;
  @Input() temp_air: any;
  @Input() label: any;

  status: any = ['clear', 'warning', 'minor', 'major', 'critical'];
  imageUrl: string;
  color: string;
  lamp: string;

  image = {
      off      : {img: '../assets/img/iconArea/chiller_mati.png', color: '#99e2ff'},
      clear    : {img: '../assets/img/iconArea/chiller_normal.png', color: '#99e2ff'},
      major    : {img: '../assets/img/iconArea/chiller_major.png', color: '#ffbe4d'},
      critical : {img: '../assets/img/iconArea/chiller_critical.png', color: '#ff6666'}
  };
  statusLamp = { on : 'orange', off : 'gray' };

  ngOnInit() {
    if (this.power == false) {
        this.imageUrl = this.image['off']['img'];
        this.color    = this.image['off']['color'];
    } else {
        this.imageUrl = this.image[this.status[this.saverity]]['img'];
        this.color    = this.image[this.status[this.saverity]]['color'];
    }
    this.lamp = this.statusLamp['on'];
  }

  ngOnChanges() {

  }

}
