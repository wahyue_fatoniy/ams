import { Component, Inject, EventEmitter, Output, ViewChild} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgxCarousel } from 'ngx-carousel';
export interface ConfigWidget {
   view: string;
   informations: Array<string>;
}

export interface FormDashboardValue {
    actionType: string;
    label: string;
    configWidget: ConfigWidget;
    id          ?: number;
}

@Component({
  selector: 'app-form-dashboard-dialog',
  template: '',
  styleUrls: ['./form-dashboard-dialog.component.css']
})
export class FormDashboardDialogComponent {
  @Output() save: EventEmitter<any> = new EventEmitter<any>();
  constructor(public dialog: MatDialog) { }

  open(value: FormDashboardValue) {
  	const dialogRef = this.dialog.open(DashboardDialogComponent, {
  		width : '300px',
  		height: '450px',
      data  :  JSON.parse(JSON.stringify(value)),
      disableClose : true
    });
    dialogRef.afterClosed().subscribe(result => {
       if (result != 'cancel') {
          this.save.emit(result);
       }
    });
  }

}





@Component({
	selector : 'app-dashboard-dialog',
	templateUrl : './form-dashboard-dialog.component.html',
  styleUrls: ['./form-dashboard-dialog.component.css']
})
export class DashboardDialogComponent {

    carousel: NgxCarousel = {
        grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
        slide: 1,
        speed: 400,
        point: {
          visible: true
        },
        load: 2,
        touch: true,
        loop: true,
        custom: 'banner'
    };
    threshold = {
        '0'  : {color: '#5cb85c'},
        '70' : {color: '#f0ad4e'},
        '90' : {color: '#f44336'}
    };
    views = [
      {label: 'Energy', unit: 'WH'},
      {label: 'Power',  unit: 'W'}
    ];
    informations = [
      {label: 'Temperature', icon: 'fa fa-thermometer-full', color: '#5bc0de'},
      {label: 'Alarm', icon: 'fa fa-bell', color: '#f44336'},
      {label: 'Humidity', icon: 'fa fa-tint', color: '#f0ad4e'}
    ];

    buttons = {
      Temperature : {label: 'Maximum Temperature', icon: 'fa fa-thermometer-full', color: '#5bc0de'},
      Alarm       : {label: 'Total Alarm', icon: 'fa fa-bell', color: '#f44336'},
      Humidity    : {label: 'Maximum Humidity', icon: 'fa fa-tint', color: '#f0ad4e'}
    };

    constructor(public dialogRef: MatDialogRef<DashboardDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.setViewsFirst();
    }

    setViewsFirst() {
        const set1  = this.views.filter(res => res.label === this.data.configWidget.view);
        const set2  = this.views.filter(res => res.label !==  this.data.configWidget.view);
        this.views = set1.concat(set2);
    }


    showLabel(value) {
      if (value.length > 28) {
         value = value.substr(0, 28) + '...';
      }
      return value;
    }



}
