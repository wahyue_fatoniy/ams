import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDashboardDialogComponent } from './form-dashboard-dialog.component';

describe('FormDashboardDialogComponent', () => {
  let component: FormDashboardDialogComponent;
  let fixture: ComponentFixture<FormDashboardDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDashboardDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDashboardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
