import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetTemperatureComponent } from './widget-temperature.component';

describe('WidgetTemperatureComponent', () => {
  let component: WidgetTemperatureComponent;
  let fixture: ComponentFixture<WidgetTemperatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetTemperatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTemperatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
