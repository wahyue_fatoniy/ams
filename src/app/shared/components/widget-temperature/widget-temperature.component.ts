import { Component, OnInit, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'app-widget-temperature',
  templateUrl: './widget-temperature.component.html',
  styleUrls: ['./widget-temperature.component.css']
})
export class WidgetTemperatureComponent implements OnInit {

  constructor() { }

  @Input() saverity: number;
  @Input() power: boolean;
  @Input() label: any;
  @Input() humidity: any;
  @Input() temperature: any;

  status: any = ['clear', 'warning', 'minor', 'major', 'critical'];
  imageUrl: string;
  color    = {critical: '#ff6666', clear: '#19a9d5', major: 'orange', off: 'gray'};

  ngOnInit() {
    if (this.power == false) {
        this.imageUrl = this.color['off'];
    } else {
      this.imageUrl = this.color[this.status[this.saverity]];
    }
  }

  ngOnChanges() {

  }
}
