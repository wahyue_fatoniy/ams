import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';

export interface eventWidget {
  id: number;
  command: string; /* Power, Temperature, Padlock */
  value: any;  /* ON/OFF, LOCK/UNLOCK, TEMPERATURE */
}

export interface Provisioning {
  id: number,
  label: string,
  type: string,   /*PL1BT, AC*/
  address: string,
  command: Object  /*{ power : ON/OFF}*/
}

@Component({
  selector: 'app-widget-provisioning',
  templateUrl: './widget-provisioning.component.html',
  styleUrls: ['./widget-provisioning.component.css']
})
export class WidgetProvisioningComponent implements OnInit {
  @Input() manageObjectId: number;
  @Input() label: string;
  @Input() type: string;
  @Input() address: string;
  @Input() command: Object;
  @Output() action: EventEmitter<Object> = new EventEmitter<Object>();
  iconPath = '../../../../assets/img/provisioningIcon/';

  widgetEquipment: Object = {
    L100w: {
      color: '#f57c00', icon: this.iconPath + 'bulb-outline.png', eventObject: [
        { name: 'power', icon: 'fa fa-power-off', color: '#ff5252' }
      ]
    },
    AC: {
      color: '#2196f3', icon: this.iconPath + 'air-conditioner.png', eventObject: [
        { name: 'power', icon: 'fa fa-power-off', color: '#ff5252' },
        { name: 'temperature', icon: 'fa fa-thermometer-full', color: '#2196f3' }
      ]
    },
    PL1BT: {
      color: '#4caf50', icon: this.iconPath + 'lock.png', eventObject: [
        { name: 'padlock', icon: 'fa fa-lock', color: '#4caf50' },
      ]
    },
    Pump: {
      color: '#ff5252', icon: this.iconPath + 'pump.png', eventObject: [
        { name: 'power', icon: 'fa fa-power-off', color: '#ff5252' },
      ]
    },
    Chiller: {
      color: '#deb200', icon: this.iconPath + 'freezer.png', eventObject: [
        { name: 'power', icon: 'fa fa-power-off', color: '#ff5252' },
        { name: 'temperature', icon: 'fa fa-thermometer-full', color: '#2196f3' }
      ]
    }
  };

  constructor() { }

  ngOnInit() { }

  setProvisioningObject(value: eventWidget): void {
    this.action.emit(value);
  }
}
