import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetProvisioningComponent } from './widget-provisioning.component';

describe('WidgetProvisioningComponent', () => {
  let component: WidgetProvisioningComponent;
  let fixture: ComponentFixture<WidgetProvisioningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetProvisioningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetProvisioningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
