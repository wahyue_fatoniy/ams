import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-widget-lock',
  templateUrl: './widget-lock.component.html',
  styleUrls: ['./widget-lock.component.css']
})
export class WidgetLockComponent implements OnInit {

  @Input() label: any;
  @Input() severity: string;

  constructor() { }



  ngOnInit() {

  }

}
