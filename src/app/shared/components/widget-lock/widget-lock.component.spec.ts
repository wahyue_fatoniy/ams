import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetLockComponent } from './widget-lock.component';

describe('WidgetLockComponent', () => {
  let component: WidgetLockComponent;
  let fixture: ComponentFixture<WidgetLockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetLockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetLockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
