import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnChanges {

  constructor() {}

  @Input()  center;
  @Input()  markers;
  @Output() action: EventEmitter<any> = new EventEmitter<any>();
  @Output() addMarker: EventEmitter<any> = new EventEmitter<any>();
  @Output() clickMarker: EventEmitter<any> = new EventEmitter<any>();
  showbox: any;
  newMarker: any = {zoom: null, latitude: null, longitude: null};

  ngOnChanges() {
    this.newMarker =  Object.assign({}, this.center);
    this.setIcons();
  }


  setIcons() {
    this.markers = this.markers.map(res => {
        return Object.assign({}, res, {
          icon: res.alarm ? 'assets/img/map/alarm.png' : 'assets/img/map/clear.png'
      });
    });
  }

  setMarker(event) {
    this.newMarker.latitude  = parseFloat(event.coords.lat.toFixed(6));
    this.newMarker.longitude = parseFloat(event.coords.lng.toFixed(6));
    this.addMarker.emit(this.newMarker);
  }

  zoomChange(event) {
    this.newMarker.zoom = event;
    this.addMarker.emit(this.newMarker);
  }

  Showinfobox(i) {
    this.showbox = i;
  }

  clickedMarker(event) {
    this.clickMarker.emit(event);
  }

  hiddeninfobox() {
    this.showbox = 'Hidden';
  }

}
