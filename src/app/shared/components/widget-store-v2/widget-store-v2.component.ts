import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { ConfigWidget } from '../form-dashboard-dialog/form-dashboard-dialog.component';
import { ConvertionValuePipe } from '../../pipes/convertion-value.pipe';
export interface Gauge {
   value: number;
   type: string;
}

export interface Information {
   type: string;
   value: number;
}

@Component({
  selector: 'app-widget-store-v2',
  templateUrl: './widget-store-v2.component.html',
  styleUrls: ['./widget-store-v2.component.css']
})
export class WidgetStoreV2Component implements OnChanges {

  constructor() { }
  @Input() label: string;
  @Input() gauge: Gauge;
  @Input() informations: Information[];
  @Input() id: number;
  @Input() loading: boolean;
  @Output() action: EventEmitter<any> = new EventEmitter<any>();
  color = {good: '#5cb85c', warning: '#f0ad4e', critical: '#f44336'};
  statusGauge: string;

  buttons = {
    Temperature : {label: 'Maximum Temperature', icon: 'fa fa-thermometer-full', color: '#5bc0de'},
    Alarm       : {label: 'Total Alarm', icon: 'fa fa-bell', color: '#f44336'},
    Humidity    : {label: 'Maximum Humidity', icon: 'fa fa-tint', color: '#f0ad4e'}
  };
  treshold;
  unit;
  ngOnChanges() {
     this.setTresholdGauge();
  }


  setTresholdGauge() {
    this.treshold = {Power : 1300, Energy: 6667};
    this.unit     = { Power : 'W', Energy: 'WH'};
    const convert   = new ConvertionValuePipe().transform(this.gauge.value, this.unit[this.gauge.type]);
    const status    = convert.unit.substr(0, 1);
    switch (status) {
      case 'K':
        this.treshold[this.gauge.type]  /= 1000;
      break;
      case 'M':
        this.treshold[this.gauge.type] /= 1000000;
      break;
    }
    const warning  = (this.treshold[this.gauge.type] / 100) * 70;
    const critical = (this.treshold[this.gauge.type] / 100) * 90;
    if (convert.converted < warning) {
      this.statusGauge = 'good';
    } else if (convert.converted >= warning && convert.converted < critical) {
      this.statusGauge = 'warning';
    } else {
      this.statusGauge = 'critical';
    }
    this.unit[this.gauge.type]  = convert.unit;
    this.gauge.value = convert.converted;
  }

  showTitle(value) {
    if (value) {
      if (value.length > 25) {
        value = value.substr(0, 25) + '...';
      }
    }
    return value;
  }


}
