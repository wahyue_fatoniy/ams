import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetStoreV2Component } from './widget-store-v2.component';

describe('WidgetStoreV2Component', () => {
  let component: WidgetStoreV2Component;
  let fixture: ComponentFixture<WidgetStoreV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetStoreV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetStoreV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
