import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatRadioButton } from '@angular/material';
import { eventWidget } from '../widget-provisioning/widget-provisioning.component';

@Component({
  selector: 'app-form-provisioning-dialog',
  template: ''
})
export class FormProvisioningDialogComponent implements OnInit {
  @Output() dialogResult: EventEmitter<Object> = new EventEmitter<Object>();

  constructor(private dialog: MatDialog) { }

  ngOnInit() { }

  openDialog(value: eventWidget): void {
    let dialogRef = this.dialog.open(ProvisioningDialog, {
      width: '300px',
      height: '250px',
      data: value
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result != 'cancel') {
        this.dialogResult.emit(result);
      }
    })
  }
}

@Component({
  selector: 'provisioning-dialog',
  templateUrl: './form-provisioning-dialog.component.html'
})
export class ProvisioningDialog {
  command: string;
  value: any;
  id: number;

  constructor(
    public dialogRef: MatDialogRef<ProvisioningDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.id = data['id']
    this.command = data['command'];
    this.value = data['value']
  }

  radioResult(event:MatRadioButton): void {
    this.value = event['value'];
  }
}
