import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProvisioningDialogComponent } from './form-provisioning-dialog.component';

describe('FormProvisioningDialogComponent', () => {
  let component: FormProvisioningDialogComponent;
  let fixture: ComponentFixture<FormProvisioningDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProvisioningDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProvisioningDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
