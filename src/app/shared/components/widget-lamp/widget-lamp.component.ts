import { Component, OnInit, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'app-widget-lamp',
  templateUrl: './widget-lamp.component.html',
  styleUrls: ['./widget-lamp.component.css']
})
export class WidgetLampComponent implements OnInit {

  constructor() { }
  @Input() power: number;
  @Input() label: any;

  ngOnInit() {

  }

}
