import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export interface TableExpansion {
   propertyParent: Array<string>;
   propertyChild: Array<string>;
   buttons: Array<string>;
   datasets: Array<any>;
   loading: boolean;
   labelCreate    ?: string;
}

export interface ActionTableExpansion {
  actionType: string;
  value      ?: any;
}

@Component({
  selector: 'app-table-expansion',
  templateUrl: './table-expansion.component.html',
  styleUrls: ['./table-expansion.component.css']
})
export class TableExpansionComponent implements OnInit {

  @Input() propertyParent: Array<string>;
  @Input() propertyChild: Array<string>;
  @Input() buttons: Array<string>;
  @Input() datasets: Array<any>;
  @Input() loading: boolean;
  @Input() labelCreate: string;
  @Output() action: EventEmitter<ActionTableExpansion> = new EventEmitter<ActionTableExpansion>();
  constructor() {}
  ngOnInit() {
  }

  setButton(type: string): boolean {
      return this.buttons.filter(res => res == type).length != 0 ? true : false;
  }

}
