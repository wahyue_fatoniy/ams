import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapBuildingComponent } from './map-building.component';

describe('MapBuildingComponent', () => {
  let component: MapBuildingComponent;
  let fixture: ComponentFixture<MapBuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapBuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapBuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
