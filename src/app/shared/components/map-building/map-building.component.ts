import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-map-building',
  templateUrl: './map-building.component.html',
  styleUrls: ['./map-building.component.css']
})
export class MapBuildingComponent implements OnInit {

  constructor() { }

  @Output() action: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit() {}

  active = [
    { label : 'Door-1' ,  active : 1},
    { label : 'Door-2' ,  active : 0},
    { label : 'Rack' ,    active : 0},
    { label : 'Atm' ,     active : 0},
    { label : 'Cashier' , active : 0}
  ];
  navContent: any = 'Door-1';

  addBuilding(type, label) {
    this.action.emit({label: label, top: 40, left: 5, type: type});
  }

  selectActive(label) {
    for (const i of this.active) {
        if (i.label == label) {
            i.active = 1;
            this.navContent = i.label;
        } else {
          i.active = 0;
        }
    }
  }

}
