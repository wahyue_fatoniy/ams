import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  ViewChild,
  SimpleChanges
} from '@angular/core';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { OPTIONS } from './options.config';

export interface BarData {
  data: any;
  label: string; // AC | Chiller | Other
  borderWidth: number; // 2
  borderStyle: string; // "dash"
}

export interface BarChart {
  chartType: string;
  labels: Array<string>;
  datasets: BarData[];
}

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnChanges {
  constructor() {}

  @Input() labels: string[];
  @Input() chartType: string;
  @Input() datasets: any[];
  @Output() type: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  barChartOptions: any = OPTIONS;
  icon = 'fa fa-bar-chart';

  chartTypeList: any[] = [
    { value: 'daily', viewValue: 'Daily' },
    { value: 'weekly', viewValue: 'Weekly' },
    { value: 'monthly', viewValue: 'Monthly' }
  ];
  barChartType = 'bar';
  barChartLegend = true;
  pageEmpty: boolean;

  changeChartType(event) {
    this.type.emit(event);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.generatePageEmpty();
    if (changes['labels']) {
      if (this.chart && this.chart.chart && this.chart.chart.config) {
        this.chart.chart.config.data.labels = this.labels;
        this.chart.chart.update();
      }
    }
  }

  generatePageEmpty() {
    let val = 0;
    this.pageEmpty = false;
    // tslint:disable-next-line:forin
    for (const i in this.datasets) {
      // tslint:disable-next-line:forin
      for (const j in this.datasets[i].data) {
        val += this.datasets[i].data[j];
      }
    }
    if (val === 0) {
      this.pageEmpty = true;
    }
  }
}
