import { Component, OnChanges, Output, Input, EventEmitter } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { ConvertionValuePipe } from '../../pipes/convertion-value.pipe';
import { InfoHighchartPipe } from '../../pipes/info-highchart.pipe';
export interface LineChart {
   chartType: string;
   perfType: string;
   series: SeriesHighchart[];
   loading: boolean;
}

export interface DataHighchart {
     x: number;
     y: number;
     info: string;
}

export interface SeriesHighchart {
   name: string;
   yAxis: number;
   data: DataHighchart[];
   cursor: string; // pointer
   marker: {symbol: string}; // marker :{symbol:`url(link.png)`}
   perfCounter ?: any;
   discobject  ?: any;
   type: string; // line || scatter
   showInLegend: boolean;
}

@Component({
  selector: 'app-line-chart-v2',
  templateUrl: './line-chart-v2.component.html',
  styleUrls: ['./line-chart-v2.component.css']
})
export class LineChartV2Component implements OnChanges {

  constructor() {}
  @Input() chartType: string;
  @Input() perfType: string;
  @Input() series: SeriesHighchart[];
  @Input() loading: boolean;
  @Output() chartTypeChanges: EventEmitter<string> = new EventEmitter<string>();
  @Output() perfTypeChanges: EventEmitter<string>  = new EventEmitter<string>();
  info: InfoHighchartPipe = new InfoHighchartPipe();
  icons = {power: 'fa fa-bolt', humidity: 'fa fa-tint', temperature: 'fa fa-thermometer-full', alarm: 'fa fa-bell'};
  units = {power: 'W', humidity: '%', temperature: '°C'};
  chartTypes = [
    {id: 'daily', label: 'Daily'},
    {id: 'weekly', label: 'Weekly'},
    {id: 'monthly', label: 'Monthly'}
  ];
  perfTypes  = [
    {id: 'power', label: 'Power'},
    {id: 'temperature', label: 'Temperature'},
    {id: 'humidity', label: 'Humidity'}
  ];
  options = {
      title   : {text: '', style: {display: 'none'}},
      credits : {enabled: false},
      yAxis   : [
        {title: {text: null}, labels: {formatter: function() {
          const convert = new ConvertionValuePipe().transform(this.value, 'W');
          return `${convert.converted} ${convert.unit}`;
        }}},
        {title: {text: null}, labels: {format: '{value}°C'}},
        {title: {text: null}, labels: {format: '{value} %'}},
        {title: {text: null}},
      ],
      tooltip : {formatter: function () {return this.point.info; }, useHTML: true},
      xAxis   : {type: 'datetime', labels: {overflow: 'justify'}},
      series  : []
  };
  private chart;

  ngOnChanges() {
    this.options.series = this.series;
    this.chart = new Chart(this.options);
  }

  addPoint(value) {
    const indexSeries = this.series.findIndex(res => res.perfCounter.id === value.counter.id);
    if (indexSeries !== -1) {
      const counter = this.series[indexSeries].perfCounter;
      this.chart.addPoint({
          x    : Date.parse(value.measurementTime),
          y    : value.measurementValue,
          info : this.info.transform({
            time  :  value.measurementTime,
            value :  value.measurementValue,
            eqType : counter.discObj.eqType.type,
            perfType : counter.perfObj.name,
            managedObj : counter.discObj.name
          })
      }, indexSeries);
    }
  }

}
