import { Component, OnInit, Input } from '@angular/core';

export interface WidgetType1 {
  unit: string;
  value: number;
  label: string;
  icon: string;
}

@Component({
  selector: 'app-widget-template1',
  templateUrl: './widget-template1.component.html',
  styles: []
})
export class WidgetTemplate1Component implements OnInit {

  constructor() {}
  @Input() unit: string;
  @Input() value: number;
  @Input() label: string;
  @Input() icon: string;

  ngOnInit() {
  }

}
