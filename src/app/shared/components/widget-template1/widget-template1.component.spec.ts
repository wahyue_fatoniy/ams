import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetTemplate1Component } from './widget-template1.component';

describe('WidgetTemplate1Component', () => {
  let component: WidgetTemplate1Component;
  let fixture: ComponentFixture<WidgetTemplate1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetTemplate1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTemplate1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
