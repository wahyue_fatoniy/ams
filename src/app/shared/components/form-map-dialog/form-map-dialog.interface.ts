export interface ListMetadata {
    Province ?: Array<any>;
    City     ?: Array<any>;
    District ?: Array<any>;
}

export interface FormMetadata {
    id: number;
    Label: string;
    Latitude: number;
    Longitude: number;
    Zoom: number;
    Province  ?: string;
    City      ?: string;
    District  ?: string;
    list      ?: {
        Province ?: number;
        City     ?: number;
        District ?: number;
    };
}


export interface CenterMetadata {
    latitude: number;
    longitude: number;
    zoom: number;
}


export interface ResponseDialogMetadata {
    actionType: string;
    value: FormMetadata;
}

export interface DataMetadata {
    center: CenterMetadata;
    value     ?: FormMetadata;
    list      ?: ListMetadata;
    actionType: string;
    title: string;
}


export interface ConfigDialogMetadata {
    type: string;
    model: string;
    list      ?: Array<any>;
    id        ?: { set: string; typeData: string; };
    listLabel ?: string;
    disable   ?: boolean;
}
