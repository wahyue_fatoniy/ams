import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMapDialogComponent } from './form-map-dialog.component';

describe('FormMapDialogComponent', () => {
  let component: FormMapDialogComponent;
  let fixture: ComponentFixture<FormMapDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormMapDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMapDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});



