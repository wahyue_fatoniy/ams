import { Component, Output, EventEmitter, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FilterDataPipe } from '../../pipes/filter-data.pipe';
import {
    FormMetadata,
    ListMetadata,
    CenterMetadata,
    DataMetadata,
    ResponseDialogMetadata,
    ConfigDialogMetadata
} from './form-map-dialog.interface';


const defaultLocation: CenterMetadata = { latitude: -0.891230, longitude: 118.498535, zoom: 5 };



@Component({
  selector: 'app-form-map-dialog',
  template: '',
  styleUrls: ['./form-map-dialog.component.css']
})


export class FormMapDialogComponent {

  @Output() save: EventEmitter<any> = new EventEmitter<any>();
  constructor(public dialog: MatDialog) {}

  data: DataMetadata ;
  backup: FormMetadata ;
  setCenter(actionType: string, value: FormMetadata): CenterMetadata {
        let center;
        if (actionType == 'create') {
            center = Object.assign({}, defaultLocation);
        } else {
            center = {
                latitude   : value.Latitude,
                longitude  : value.Longitude,
                zoom       : value.Zoom
            };
        }
        return center;
  }

  openDialog(title: string, actionType: string, value?: FormMetadata, list?: ListMetadata) {
        const sizeWidth = {create: '1000px', edit: '1000px', remove: ''};
        this.backup   = Object.assign({}, value);
        this.data     = {
            actionType  :  actionType,
            title       :  title,
            value       :  value,
            list        :  list,
            center      :  this.setCenter(actionType, value)
        };
        const dialogRef = this.dialog.open(DialogMapComponent, {
            width  : sizeWidth[ actionType ],
            data   : this.data,
            disableClose : true
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result.actionType == 'cancel') {
                for (const i in value) {
                    value[i] = this.backup[i];
                }
            } else {
                this.save.emit({actionType: actionType, value: result.value});
            }
        });
  }


}









@Component({
  templateUrl : './dialog.html'
})

export class DialogMapComponent {

  constructor(public dialogRef: MatDialogRef<DialogMapComponent>,
  @Inject(MAT_DIALOG_DATA) public data: DataMetadata) {
    if (this.data.title != 'Form Province') {
        this.setList('District');
        this.setList('City');
        this.setList('Province');
    }
    this.setValue();
    this.setValueNotModel();
    this.setModelAddress();
 }

  backupList: any  = {};
  dist: any  = {};
  config: ConfigDialogMetadata[] = [
    {type: 'text', model: 'Label'},
    {type: 'number', model: 'Latitude', disable: true},
    {type: 'number', model: 'Longitude', disable: true},
    {type: 'number', model: 'Zoom', disable: true},
  ];

  setModelAddress() {
    if (this.data.title == 'Form Site') {
        this.config.push({type: 'text', model: 'Address'});
    }
  }

  filterList(id_parent, model) {
     for (const i in this.config) {
         if (this.config[i].model == model && this.config[i].type == 'list') {
             this.config[i].list  = new FilterDataPipe().transform(this.backupList[model], {id_parent: id_parent}, 'detail');
         }
     }
  }


  setValueNotModel() {
    let zone, province, branch;
    if (this.data.actionType == 'edit') {
        switch (this.data.title) {
            case 'Form District':
                zone = new FilterDataPipe().transform(this.backupList['City'], {id: this.dist.list['City']}, 'detail');
                province = new FilterDataPipe().transform(this.backupList['Province'], {id: zone[0].id_parent}, 'detail');
                this.dist.list['Province'] = province[0].id;
                this.filterList(this.dist.list['Province'], 'City');
            break;
            case 'Form Site':
                branch = new FilterDataPipe().transform(this.backupList['District'], {id: this.dist.list['District']}, 'detail');
                zone   = new FilterDataPipe().transform(this.backupList['City'], {id: branch[0].id_parent}, 'detail');
                province = new FilterDataPipe().transform(this.backupList['Province'], {id: zone[0].id_parent}, 'detail');
                this.dist.list['City'] = zone[0].id;
                this.dist.list['Province'] = province[0].id;
                this.filterList(this.dist.list['City'], 'District');
                this.filterList(this.dist.list['Province'], 'City');
            break;
        }
    }
  }


  setList(type) {
     if (this.data.list[ type ] != undefined ) {
        this.config.unshift({
              type      : 'list',
              model     : type,
              list      : this.data.list[ type ],
              id        : { set : 'id', typeData : 'number' },
              listLabel : 'label'
        });
        this.backupList[ type ] = Object.assign([], this.data.list[ type ]);
      }
  }

  addMarker(event) {
    this.data.center = {
        latitude   :   event.latitude,
        longitude  :   event.longitude,
        zoom       :   event.zoom
    };
    this.dist.Latitude  = event.latitude;
    this.dist.Longitude = event.longitude;
    this.dist.Zoom      = event.zoom;
  }

  setListValue() {
    for (const i in this.config) {
        if (this.config[i].type == 'list') {
            const tile = this.config[i];
            const list = tile.list;
            for (const j in list) {
                if (list[j][ tile.listLabel ] == this.dist[ tile.model ]) {
                    this.dist.list[ tile.model ] = list[j][ tile.id.set ];
                }
            }
        }
    }
  }


  setValue() {
    if (this.data.actionType != 'create') {
        this.dist = this.data.value;
        this.dist.list = {};
        this.setListValue();
    } else {
        this.dist = {
            Latitude  : defaultLocation.latitude,
            Longitude : defaultLocation.longitude,
            Zoom      : defaultLocation.zoom,
            list      : {}
        };
    }
  }

  setTypeData(type, model) {
    switch (type) {
        case 'text':
            this.dist[ model ] = this.dist[ model ].toString();
        break;
        case 'number':
            this.dist[ model ] = parseFloat(this.dist[ model ]);
        break;
    }
    if(model === 'Latitude'){
        this.data.center.latitude = this.dist[model];
    }
    else if(model === 'Longitude'){
        this.data.center.longitude = this.dist[model];
    }
    else if(model === 'Zoom'){
        this.data.center.zoom = this.dist[model];
    }
  }


  setManual(event) {
    for (const i in this.config) {
        if (this.config[i].disable != undefined) {
            this.config[i].disable = !event.checked;
        }
    }
  }

  changeList(tile, index) {
      const list = tile.list;
      for (const i in list) {
          if (list[i][ tile.id.set] == this.dist.list[ tile.model ]) {
             this.dist[ tile.model ] = list[i][ tile.listLabel ];
             this.data.center = {
                 latitude :  list[i].location.lat,
                 longitude : list[i].location.lng,
                 zoom      : list[i].zoom
             };
             this.dist.Latitude  = list[i].location.lat;
             this.dist.Longitude = list[i].location.lng;
             this.dist.Zoom      = list[i].zoom;
             this.dataListAssignment(index, list[i][ tile.id.set]);
           }
      }
  }

  dataListAssignment(index: number, id_parent: number) {
    const child  = this.config[ index + 1 ];
    if (child.type == 'list') {
        child.list = new FilterDataPipe().transform(this.backupList[ child.model ], {id_parent : id_parent}, 'detail');
    }
  }


  showPlaceholder(tile) {
    let result;
    if (tile.list.length == 0) {
        result = `${tile.model} not available`;
        if (tile.model == 'City') {
            this.config[2].list = [];
        }
    } else {
        result = tile.model;
    }
    return result;
  }


}
