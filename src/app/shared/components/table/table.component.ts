import { Component, Input, ViewChild, OnChanges, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

import { FormControl } from '@angular/forms';
import { DetectImagePipe } from '../../index';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';



export interface Table {
  title: string;
  loading: boolean;
  buttons?: Array<string>;
  columns: Array<string>;
  datasets: Array<any>;
  images?: Array<string>;
  width?: string;
  datepicker?: boolean;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    {provide: MAT_DATE_LOCALE, useValue: 'id'},

    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ]
})
export class TableComponent implements OnChanges {

  constructor() {

  }
  link: DetectImagePipe = new DetectImagePipe();
  date = new FormControl();
  maxDate = new Date();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() loading = false;
  @Input() buttons: Array<string> = [];
  @Input() columns: Array<string>;
  @Input() datasets: Array<any>;
  @Input() images: Array<string>;
  @Input() title: string;
  @Input() width: string;
  @Input() datepicker: boolean;
  @Output() action: EventEmitter<any> = new EventEmitter<any>();
  typeColumn: any = { id: 'number', action: 'buttons' };
  create = false;
  configButtons: any = {
    edit: { type: 'edit', label: 'Edit', icon: 'edit', color: 'btn-success' },
    remove: { type: 'remove', label: 'Remove', icon: 'close', color: 'btn-danger' },
    detail: { type: 'detail', label: 'Show detail', icon: 'recent_actors', color: 'btn-info' },
    acknowledge: { type: 'acknowledge', label: 'Acknowledge', icon: 'assignment_ind', color: 'btn-info' }
  };
  pageSize = 10;
  pageIndex = 0;
  // datasets
  dataSource: any;


  ngOnChanges() {
    this.width = this.width === undefined ? '1000px' : this.width;
    this.dataSource = new MatTableDataSource(this.datasets);
    this.setButtons();
    this.setImages();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.date.valueChanges.subscribe(data => {
      this.action.emit({ actionType: 'datepicker', value: data });
    });
    this.datasets.forEach(element => {
      element.active = '#fff';
    });
  }

  setActive(id) {
    this.datasets.forEach(element => {
      element.active = element.id === id ? '#eee' : '#fff';
    });
  }

  setButtons() {
    if (this.buttons !== undefined) {
      if (this.buttons !== []) {
        const buttons = [];
        this.buttons.forEach(button => {
          button === 'create' ? this.create = true : buttons.push(button);
        });
        if (buttons.length !== 0) {
          const index = this.columns.findIndex(column => column === 'action');
          if (index === -1) {
            this.columns.push('action');
          }
        }
        this.buttons = buttons;
      }
    }
  }

  setImages() {
    if (this.images !== undefined) {
      this.images.forEach(image => {
        this.typeColumn[image] = 'image';
      });
    }
  }



  applyFilter(text) {
    text = text.trim(); // Remove whitespace
    text = text.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = text;
  }

  setAction(actionType, value) {
    this.action.emit({ actionType: actionType, value: value });
  }

  pageSet(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
  }


}
