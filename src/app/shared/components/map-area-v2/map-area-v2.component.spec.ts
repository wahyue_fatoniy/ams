import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapAreaV2Component } from './map-area-v2.component';

describe('MapAreaV2Component', () => {
  let component: MapAreaV2Component;
  let fixture: ComponentFixture<MapAreaV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapAreaV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapAreaV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
