export interface AC {
  id: number;
  label: string;
  top: number;
  left: number;
  power: number;
  temperature: number;
  severity: string;
  id_discobject ?: number;
}

export interface Chiller {
   id: number;
   label: string;
   top: number;
   left: number;
   power: number;
   temp_material: number;
   temp_air: number;
}

export interface Pump {
  id: number;
  label: string;
  top: number;
  left: number;
  power: number;
  lifetime: number;
}

export interface Temperature {
  id: number;
  label: string;
  top: number;
  left: number;
  power: number;
  humidity: number;
  temperature: number;
}

export interface Lamp {
  id: number;
  label: string;
  top: number;
  left: number;
  power: number;
  id_discobject ?: number;
}

export interface PadLock {
  id: number;
  label: string;
  top: number;
  left: number;
  severity: string;
  id_discobject ?: number;
}

export interface Building {
  id ?: number;
  top: number;
  left: number;
  type: string;
}

export interface Position {
  id: number;
  top: number;
  left: number;
  type ?: string;
  label ?: string;
}

export interface MapArea {
  ac: AC[];
  chiller: Chiller[];
  pump: Pump[];
  lamp: Lamp[];
  temperature: Temperature[];
  lock: PadLock[];
  rack: Building[];
  atm: Building[];
  cashier: Building[];
  door: Building[];
  loading: boolean;
  buildingView: boolean;
  updatePosition: {
      equipment: Position[],
      building: Position[]
  };
  removeBuilding: Position[];
}

import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-map-area-v2',
  templateUrl: './map-area-v2.component.html',
  styleUrls: ['./map-area-v2.component.css']
})
export class MapAreaV2Component implements OnChanges {

  constructor() { }
  @Input() loading: boolean;
  @Input() ac: AC[];
  @Input() chiller: Chiller[];
  @Input() temperature: Temperature[];
  @Input() pump: Pump[];
  @Input() lamp: Lamp[];
  @Input() lock: PadLock[];
  @Input() rack: Building[];
  @Input() cashier: Building[];
  @Input() atm: Building[];
  @Input() door: Building[];
  @Output() action: EventEmitter<any> = new EventEmitter<any>();
  viewType = 'default';

  ngOnChanges() {
  }

  convert(value) {
    const width = document.getElementById('container').offsetWidth;
    value = (value * width) / 100;
    return value;
  }

  remove(type: string, id: any, buildingType?: string) {
    if (this.viewType == 'building') {
       this.sendAction(type, id, buildingType);
    }
  }

  sendAction(type: string, id: any, buildingType?: string) {
    let result;
    if (id != undefined) {
      let left  = document.getElementById(id).offsetLeft;
      const top   = document.getElementById(id).offsetTop;
      const width = document.getElementById('container').offsetWidth;
      left      = (left * 100) / width;
      left      = parseFloat(left.toFixed(2));
      result = {
          id   : id,
          top  : top,
          left : left
      };
      if (buildingType != undefined) {
        result.buildingType = buildingType;
      }
    }
    this.action.emit({type: type, value: result});
  }
}
