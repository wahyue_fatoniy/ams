import { Component, Output, EventEmitter, Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface DialogV2 {
  id: number;
  message: string;
  acknowledge ?: boolean;
}

@Component({
  selector: 'app-dialog',
  template: ''
})

export class DialogRemoveComponent {

  @Output() save: EventEmitter<number> = new EventEmitter<number>();
  constructor(public dialog: MatDialog) { }

  open(config: DialogV2) {
    config.message += ' ?';
    const dialogRef = this.dialog.open(Dialog, {
      data  :  config,
      disableClose : true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.type == 'save') {
        this.save.emit(result.value);
      }
    });
  }
}

@Component({
	templateUrl : 'dialog.component.html'
})

export class Dialog {
  constructor(public dialogRef: MatDialogRef<Dialog>, @Inject(MAT_DIALOG_DATA) public data: any) {}
}
