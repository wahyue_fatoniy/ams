import { Component, Output, EventEmitter } from '@angular/core';
import {  MatDialog } from '@angular/material';
import {  FormV2 } from './interface';
import {  FormDialogV2 } from './form-template';


export interface BindData {
  id: number;
  model: string;
}

@Component({
  selector: 'app-form-v2',
  template : ''
})
export class FormV2Component {
  @Output() save: EventEmitter<any> = new EventEmitter<any>();
  constructor(public dialog: MatDialog) {}

  open(config: FormV2) {
    const dialogRef = this.dialog.open(FormDialogV2, {
        width : '800px',
        data  : config,
        disableClose : true
    });
    dialogRef.afterClosed().subscribe(result => {
        if (result.type == 'save') {
            const value = result.value;
            if (config.actionType == 'edit') {
              value.id = config.value.id;
            }
            for (const i in config.models) {
              const tile = config.models[i];
              if (tile.type == 'image') {
                 value[ tile.model ] = `/api/containers/${tile.container}/download/${value[tile.model]}`;
              }
              if (config.actionType == 'edit') {
                for (const j in config.hiddens) {
                  value[ config.hiddens[j] ] = config.value[ config.hiddens[j] ];
                }
              }
            }
            this.save.emit({actionType: config.actionType, value: value});
        }
    });
  }
}

