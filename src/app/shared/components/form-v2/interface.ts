export interface Model {
    type: string;
    model: string;
    // khusus type input select option
    list         ?: Array<any>; // list data di select option
    listLabel    ?: string;    // property yang digunakan di select option
    idSet        ?: string;    // property yang akan di set di select option
    multiple     ?: boolean;
    //khusus type data date
    formatInput  ?: string;
    minValidate  ?: string;
    //khusus file type
    container    ?: string;
    loading      ?: boolean;
    disabled     ?: boolean;
    required     ?: boolean; //setting jika ingin validasi required di matikan
    event        ?: string; //setting jika model change memiliki event tertentu kepada model yang lain
 }

export interface FormV2 {
    title: string;
    actionType: string;
    models: Model[];
    value: any;
    //khusus jika ingin ada properti yang di set tapi tidak di tampilkaan di form
    hiddens     ?: Array<string>;
}
