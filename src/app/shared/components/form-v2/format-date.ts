import { NativeDateAdapter } from '@angular/material';

export class AppDateAdapter extends NativeDateAdapter {
    format(date: Date, displayFormat: Object): string {
        if (displayFormat === 'input') {
            const day = date.getDate();
            const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
                          'November', 'December'];
            const month = date.getMonth();
            const year = date.getFullYear();
            return `${day} ${months[ month ]} ${year}`;
        } else {
            return date.toDateString();
        }
    }
  }

export const MY_FORMATS = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'input',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    }
};
