import { Component, Input, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators, FormBuilder} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FileUploader } from 'ng2-file-upload';
import { FormatDatePipe } from '../../pipes/format-date.pipe';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { FilterDataPipe } from '../../pipes/filter-data.pipe';
import { SortDataPipe } from '../../pipes/sort-data.pipe';


//date picker
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
const moment = _rollupMoment || _moment;

//import internal class
import {  FormV2, Model } from './interface';
import {  MY_FORMATS, AppDateAdapter } from './format-date';
import {  CreateStateMatcher, EditStateMatcher } from './validation';
import { ValidationErrors } from '@angular/forms/src/directives/validators';

export interface ImageConfig {
  label: string;
  container: string;
}

@Component({
    templateUrl: 'dialog.html',
    styles : [`
      input[type="file"] {
        display: none;
      }
      .button-row { display: flex; align-items: center; justify-content: space-around; }
    `],
    providers: [
      {provide: DateAdapter, useClass: AppDateAdapter},
      {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ]
  })


  export class FormDialogV2 {
    constructor(public dialogRef: MatDialogRef<FormDialogV2>, @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder, private http: HttpClient) {
        this.setForm();
        this.setMinimumValidate();
        this.setFiles();
    }
    form: FormGroup;
    matcher = {create: new CreateStateMatcher(), edit: new EditStateMatcher()};
    files   = {};
    removeImages: Array<ImageConfig> = [];
    @ViewChild('parent') parent;

    eventChange(event, config) {
      if (config.model == 'Location' && this.data.title == 'Form Work Order') {
          const index = this.data.models.findIndex(res => res.model == 'Padlock List');
          const indexLocation = config.list.findIndex(res => res.id == event.value);
          const latitude  = config.list[indexLocation].latitude;
          const longitude = config.list[indexLocation].longitude;
          this.data.models[index].list = [];
          this.data.models[index].loading = true;
          this.form.controls['Padlock List'].setValue(undefined);
          this.http.get(`/api/apps/discobject?latitude=${latitude}&longitude=${longitude}`)
          .subscribe((result: any) => {
            this.data.models[index].loading = false;
            this.data.models[index].list = result.filter(res => res.eqType.type == 'PL1BT');
          });
      } else if (config.event == 'Verify parent') {
          const index = config.list.findIndex(res => res.id == event.value);
          if (config.list[index].type == 'Zone') {
            this.form.controls['Parent'].reset({ value: undefined, disabled: true });
          } else {
            this.form.controls['Parent'].reset({ value: undefined, disabled: false });
          }
      }
    }

    //khusus type file
    setFiles() {
      const models = this.data.models;
      for (const i in models) {
         if (models[i].type == 'image') {
             this.files[ models[i].container ] = new FileUploader({url: `/api/containers/${models[i].container}/upload`});
         }
      }
    }

    getPictureName(container) {
      const index = this.files[container].queue.length - 1;
      const results  = this.files[container].queue[ index ].file.name.split('.');
      const type     = results[ results.length - 1  ];
      let text = '';
      const possible = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`;
      const date  = new FormatDatePipe().transform(new Date(), 'DDMMYYYYhhmmss');
      for (let i = 0; i < 10; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      const value = `${date}${text}.${type}`;
      this.files[container].queue[index].file.name = value;
      return value;
    }

    setPicture(tile) {
      const name = this.getPictureName(tile.container);
      const set  = this.form.getRawValue();
      if (set[tile.model] != '') {
         this.removeImages.push({container: tile.container, label: set[tile.model]});
      }
      set[ tile.model ] = name;
      this.form.setValue(set);
    }

    saveImages() {
      for (const i in this.files) {
        this.files[i].uploadAll();
      }
      const set = [];
      const list = this.removeImages;
      for (const i in list) {
         set.push(this.http.delete(`/api/containers/${list[i].container}/files/${list[i].label}`));
      }
      Observable.forkJoin(set).subscribe(res => {});
    }

    setForm() {
      const models = this.data.models;
      const set = {};
      for (const i in models) {
        const result: any = this.setEditValue(models[i]);
        let disabled = false;
        if (models[i].disabled != undefined) {
          disabled = models[i].disabled;
        }
        switch (models[i].type) {
          case 'email':
            set[ models[i].model ] = new FormControl(result, [Validators.required, Validators.email]);
          break;
          case 'phone':
          set[ models[i].model ] = new FormControl(result, [Validators.required, Validators.maxLength(15),
            Validators.minLength(10), Validators.pattern(/(?:\+62)?0?8\d{2}(\d{8})/)]);
          break;
          case 'date':
            set[ models[i].model ] = new FormControl(result, [this.validatorDate]);
          break;
          case 'image':
            set[ models[i].model ] = new FormControl(result, [Validators.required,
              Validators.pattern(/\.(gif|jpg|jpeg|tiff|png)$/i)]);
          break;
          default:
            const validator = models[i].required == false ? [] : [Validators.required];
            set[ models[i].model ] = new FormControl({value: result, disabled: disabled}, validator);
          break;
        }
      }
      this.form = this.formBuilder.group(set);
    }



    placeholder(value: Model) {
      let result;
      if (value.loading == undefined || value.loading == false) {
          if (value.list.length == 0) {
              result = `${value.model} not available`;
          } else {
              result = value.model;
          }
      } else {
         result = 'Loading...';
      }
      return result;
    }

    setEditValue(tile) {
      let result: any = '';
      if (this.data.actionType === 'create' && tile.type === 'date') {
         result = moment(null);
      }
      if (this.data.actionType === 'edit') {
          switch (tile.type) {
            case 'date':
                if (tile.formatInput != undefined) {
                  const date     = this.data.value[ tile.model ];
                  const position = {
                      YYYY : tile.formatInput.indexOf('YYYY'),
                      MM   : tile.formatInput.indexOf('MM'),
                      DD   : tile.formatInput.indexOf('DD')
                  };
                  const set = {
                      YYYY : parseInt(date.substr(position['YYYY'], 4)),
                      MM   : parseInt(date.substr(position['MM'], 2)) - 1,
                      DD   : parseInt(date.substr(position['DD'], 2))
                  };
                  result = moment([set['YYYY'], set['MM'], set['DD']]);
                  result = result._d;
                } else {
                   console.log(`please set format input date in model ${tile.model}`);
                }
            break;
            case 'list':
              if (tile.multiple == undefined) {
                for (const i in tile.list) {
                    if ( tile.list[i][ tile.listLabel ] == this.data.value[ tile.model ] ) {
                      result = tile.list[i][ tile.idSet ];
                    }
                }
              } else {
                result = this.data.value[ tile.model ];
              }
            break;
            case 'image':
                const links = this.data.value[ tile.model ].split('/');
                result = links[ links.length - 1  ];
            break;
            default:
              result = this.data.value[ tile.model ];
            break;
          }
      }
      return result;
    }


    //khusus input datepicker
    setMinimumValidate() {
      const models = this.data.models;
      for (const i in models) {
         if (models[i].type == 'date') {
            if (models[i].minValidate != undefined) {
               this.data.models[i].min = new Date();
            }
         }
      }
    }

    changeDate(value, model: string) {
      const models = this.data.models;
      for (const i in models) {
        if (models[i].type == 'date') {
          if (models[i].minValidate == model) {
             this.data.models[i].min = value;
          }
        }
      }
    }

    validatorDate(tile: FormGroup) {
      let valid = null;
      if (isNaN(tile.value)) {
        valid = {matDatepickerParse: true};
      }
      if (tile.value == null) {
        valid = {required: true};
      }
      return valid;
    }

  }
