import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-widget-rack',
  templateUrl: './widget-rack.component.html',
  styleUrls: ['./widget-rack.component.css']
})
export class WidgetRackComponent implements OnInit {

  constructor() { }
  @Input() type: any;
  @Input() label: any;

  ngOnInit() { }

}
