import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetRackComponent } from './widget-rack.component';

describe('WidgetRackComponent', () => {
  let component: WidgetRackComponent;
  let fixture: ComponentFixture<WidgetRackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetRackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetRackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
