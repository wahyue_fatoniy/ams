import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetAcComponent } from './widget-ac.component';

describe('WidgetAcComponent', () => {
  let component: WidgetAcComponent;
  let fixture: ComponentFixture<WidgetAcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetAcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetAcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
