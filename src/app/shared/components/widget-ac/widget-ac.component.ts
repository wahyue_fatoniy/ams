import { Component, OnInit, Input} from '@angular/core';


@Component({
  selector: 'app-widget-ac',
  templateUrl: './widget-ac.component.html',
  styleUrls: ['./widget-ac.component.css']
})
export class WidgetAcComponent implements OnInit {

  constructor() { }

  @Input() severity: number;
  @Input() power: number;
  @Input() temperature: number;
  @Input() label: string;
  color = { clear : '#5bc0de', warning: '#d9534f', critical: '#d9534f', major: '#f0ad4e', minor: '#f0ad4e'};
  setColor: string;
  ngOnInit() {
    this.setColor = this.power == 0 ? '#ababab' : this.color[this.severity];
  }


}


