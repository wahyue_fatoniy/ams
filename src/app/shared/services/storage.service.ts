import { Injectable } from '@angular/core';
import { LocalStorage } from 'ng2-webstorage';
import { Site } from '../index';
import { WidgetSettingView, Widget } from "../../pages";

@Injectable()
export class StorageService {

  constructor() { }

  @LocalStorage('site') site: Site;
  @LocalStorage('defaultWidget') defaultWidget:WidgetSettingView;
  @LocalStorage('widgets') widgets:Widget[];
}
