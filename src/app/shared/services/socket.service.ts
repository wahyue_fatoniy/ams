import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
import { HOST } from '../index';

@Injectable()
export class SocketService {

  constructor() {}
  socket = io(`${HOST}:3002`);


  // channel alarm.data or perfmeasurement.data
  streaming(channel: string) {
    const observable = new Observable(observer => {
      this.socket.on(channel, (data) => {
        if (this.IsJsonString(data)) {
          data = JSON.parse(data);
          observer.next(data);
        }
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  IsJsonString(message: string) {
    try {
        JSON.parse(message);
    } catch (e) {
        console.log(`not valid json data on kafka '${message}'`);
        return false;
    }
    return true;
  }

}
