import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { MAT_DATE_LOCALE } from '@angular/material';
import localesId from '@angular/common/locales/id';
registerLocaleData(localesId);

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToolsModule } from '../tools.module';
import { AngularMaterialModule } from '../angular-material.module';

// components
import { TableComponent } from './components/table/table.component';
import { ErrorHandlerComponent } from './components/error-handler/error-handler.component';
import {
  DialogRemoveComponent,
  Dialog
} from './components/dialog/dialog.component';
import { MapComponent } from './components/map/map.component';
import {
  FormMapDialogComponent,
  DialogMapComponent
} from './components/form-map-dialog/form-map-dialog.component';
import { FormV2Component } from './components/form-v2/form-v2.component';
import { FormDialogV2 } from './components/form-v2/form-template';
import { TableExpansionComponent } from './components/table-expansion/table-expansion.component';

import { LineChartV2Component } from './components/line-chart-v2/line-chart-v2.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { MapAreaV2Component } from './components/map-area-v2/map-area-v2.component';
import { WidgetTemplate1Component } from './components/widget-template1/widget-template1.component';
import { WidgetTemplate2Component } from './components/widget-template2/widget-template2.component';
import { WidgetAcComponent } from './components/widget-ac/widget-ac.component';
import { WidgetChillerComponent } from './components/widget-chiller/widget-chiller.component';
import { WidgetPumpComponent } from './components/widget-pump/widget-pump.component';
import { WidgetLockComponent } from './components/widget-lock/widget-lock.component';
import { WidgetLampComponent } from './components/widget-lamp/widget-lamp.component';
import { WidgetDoorComponent } from './components/widget-door/widget-door.component';
import { WidgetAtmComponent } from './components/widget-atm/widget-atm.component';
import { WidgetRackComponent } from './components/widget-rack/widget-rack.component';
import { WidgetProvisioningComponent } from './components/widget-provisioning/widget-provisioning.component';
import { WidgetTemperatureComponent } from './components/widget-temperature/widget-temperature.component';
import { WidgetCashierComponent } from './components/widget-cashier/widget-cashier.component';
import { MapBuildingComponent } from './components/map-building/map-building.component';
import {
  FormDashboardDialogComponent,
  DashboardDialogComponent
} from './components/form-dashboard-dialog/form-dashboard-dialog.component';
import { WidgetStoreV2Component } from './components/widget-store-v2/widget-store-v2.component';
import {
  FormProvisioningDialogComponent,
  ProvisioningDialog
} from './components/form-provisioning-dialog/form-provisioning-dialog.component';

// pipes
import { ClearAlarmPipe } from './pipes/clear-alarm.pipe';
import { DetectImagePipe } from './pipes/detect-image.pipe';
import { UrlEncodePipe } from './pipes/url-encode.pipe';
import { FilterDataPipe } from './pipes/filter-data.pipe';
import { SortDataPipe } from './pipes/sort-data.pipe';
import { FormatDatePipe } from './pipes/format-date.pipe';
import { ConvertionValuePipe } from './pipes/convertion-value.pipe';
import { InfoHighchartPipe } from './pipes/info-highchart.pipe';

// directives
import { DraggableDirective } from './directives/draggable.directive';
import { ImageDirective } from './directives/image.directive';

// services
import { SocketService } from './services/socket.service';
import { StorageService } from './services/storage.service';
import {
  DialogImageComponent,
  DialogImage
} from './components/dialog-image/dialog-image.component';
import { AppService } from './services/app.service';

@NgModule({
  imports: [
    CommonModule,
    AngularMaterialModule,
    ToolsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  exports: [
    TableComponent,
    ErrorHandlerComponent,
    DialogRemoveComponent,
    Dialog,
    MapComponent,
    FormMapDialogComponent,
    DialogMapComponent,
    FormV2Component,
    FormDialogV2,
    DialogRemoveComponent,
    TableExpansionComponent,
    LineChartV2Component,
    BarChartComponent,
    MapAreaV2Component,
    WidgetTemplate1Component,
    WidgetTemplate2Component,
    WidgetAcComponent,
    WidgetChillerComponent,
    WidgetPumpComponent,
    WidgetLockComponent,
    WidgetLampComponent,
    WidgetDoorComponent,
    WidgetAtmComponent,
    WidgetRackComponent,
    WidgetProvisioningComponent,
    WidgetTemperatureComponent,
    WidgetCashierComponent,
    MapBuildingComponent,
    FormDashboardDialogComponent,
    DashboardDialogComponent,
    WidgetStoreV2Component,
    FormProvisioningDialogComponent,
    ProvisioningDialog,
    DialogImageComponent,
    DialogImage,

    ClearAlarmPipe,
    UrlEncodePipe,
    DetectImagePipe,
    FormatDatePipe,
    FilterDataPipe,
    SortDataPipe,
    InfoHighchartPipe,
    ConvertionValuePipe
  ],
  declarations: [
    TableComponent,
    ErrorHandlerComponent,
    DialogRemoveComponent,
    Dialog,
    MapComponent,
    FormMapDialogComponent,
    DialogMapComponent,
    FormV2Component,
    FormDialogV2,
    DialogRemoveComponent,
    TableExpansionComponent,
    LineChartV2Component,
    BarChartComponent,
    MapAreaV2Component,
    WidgetTemplate1Component,
    WidgetTemplate2Component,
    WidgetAcComponent,
    WidgetChillerComponent,
    WidgetPumpComponent,
    WidgetLockComponent,
    WidgetLampComponent,
    WidgetDoorComponent,
    WidgetAtmComponent,
    WidgetRackComponent,
    WidgetProvisioningComponent,
    WidgetTemperatureComponent,
    WidgetCashierComponent,
    MapBuildingComponent,
    FormDashboardDialogComponent,
    DashboardDialogComponent,
    WidgetStoreV2Component,
    FormProvisioningDialogComponent,
    ProvisioningDialog,
    DialogImageComponent,
    DialogImage,

    ImageDirective,
    DraggableDirective,

    ClearAlarmPipe,
    UrlEncodePipe,
    DetectImagePipe,
    FormatDatePipe,
    FilterDataPipe,
    SortDataPipe,
    InfoHighchartPipe,
    ConvertionValuePipe
  ],
  providers: [
    StorageService,
    SocketService,
    AppService,
    { provide: LOCALE_ID, useValue: 'id' },
    {provide: MAT_DATE_LOCALE, useValue: 'id'}
  ],
  entryComponents: [
    Dialog,
    DialogMapComponent,
    FormDialogV2,
    DashboardDialogComponent,
    ProvisioningDialog,
    DialogImage
  ]
})
export class SharedModule {}
