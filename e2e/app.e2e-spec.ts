import { AppPage } from './app.po';
import { browser } from 'protractor';

describe('ams App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    browser.pause();
  });
});
